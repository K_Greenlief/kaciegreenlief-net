﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace HTTP_Web_Services_POST_PUT_DELETE_lecture
{
    public class APIService
    {
        private readonly string API_URL = "";
        private readonly string API_KEY = "000000";
        private readonly RestClient client = new RestClient();

        public APIService(string api_url)
        {
            API_URL = api_url;
        }

        public List<Hotel> GetHotels()
        {
            RestRequest request = new RestRequest(API_URL + "hotels?APIKey=" + API_KEY);
            IRestResponse<List<Hotel>> response = client.Get<List<Hotel>>(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new HttpRequestException("Error occurred - unable to reach server.");
            }
            else if (!response.IsSuccessful)
            {
                throw new HttpRequestException("Error occurred - received non-success response: " + (int)response.StatusCode);
            }
            else
            {
                return response.Data;
            }
        }

        public List<Reservation> GetReservations(int hotelId = 0)
        {
            string url = API_URL;
            if (hotelId != 0)
            {
                url += $"hotels/{hotelId}/reservations";
            }
            else
            {
                url += "reservations";
            }

            RestRequest request = new RestRequest(url + "?APIKey=" + API_KEY);
            IRestResponse<List<Reservation>> response = client.Get<List<Reservation>>(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new HttpRequestException("Error occurred - unable to reach server.");
            }
            else if (!response.IsSuccessful)
            {
                throw new HttpRequestException("Error occurred - received non-success response: " + (int)response.StatusCode);
            }
            else
            {
                return response.Data;
            }
        }

        public Reservation GetReservation(int reservationId)
        {
            RestRequest request = new RestRequest(API_URL + "reservations/" + reservationId + "?APIKey=" + API_KEY);
            IRestResponse<Reservation> response = client.Get<Reservation>(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new HttpRequestException("Error occurred - unable to reach server.");
            }
            else if (!response.IsSuccessful)
            {
                throw new HttpRequestException("Error occurred - received non-success response: " + (int)response.StatusCode);
            }
            else
            {
                return response.Data;
            }
        }

        public Reservation AddReservation(Reservation newReservation)
        {
            RestRequest request = new RestRequest(API_URL + "reservations?APIKey=" + API_KEY);
            request.AddJsonBody(newReservation);
            IRestResponse<Reservation> response = client.Post<Reservation>(request);
            // TODO Typically I would refactor the other methods, I'm not today so you have the progression
            // I don't care about the return, just if an error is thrown.
            ProcessResponse(response);
            return response.Data;
        }

        public Reservation UpdateReservation(Reservation reservationToUpdate)
        {
            RestRequest request = new RestRequest(API_URL + "reservations/" + reservationToUpdate.Id + "?APIKey=" + API_KEY);
            request.AddJsonBody(reservationToUpdate);
            IRestResponse<Reservation> response = client.Put<Reservation>(request);
            ProcessResponse(response);
            return response.Data;
        }

        public void DeleteReservation(int reservationId)
        {
            RestRequest req = new RestRequest(API_URL + "reservations/" + reservationId + "?APIKey=" + API_KEY);
            IRestResponse res = client.Delete(req);
            if (res.ResponseStatus != ResponseStatus.Completed)
            {
                throw new HttpRequestException("Error occurred - unable to reach server.");
            }
            else if (!res.IsSuccessful)
            {
                throw new HttpRequestException("Error occurred - received non-success response: " + res.StatusCode + " -- " + (int)res.StatusCode);
            }

        }

        private bool ProcessResponse(IRestResponse responseIn)
        {
            if (responseIn.ResponseStatus != ResponseStatus.Completed)
            {
                throw new HttpRequestException("Error occurred - unable to reach server.");
            }
            else if (!responseIn.IsSuccessful)
            {
                throw new HttpRequestException("Error occurred - received non-success response: " + (int)responseIn.StatusCode);
            }
            return true;
        }
    }
}

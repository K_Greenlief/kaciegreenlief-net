﻿using System;
using System.Collections.Generic;

namespace HTTP_Web_Services_POST_PUT_DELETE_lecture
{
    class Program
    {

        static void Main(string[] args)
        {
            ConsoleService console = new ConsoleService(new APIService("https://te-pgh-api.azurewebsites.net/api/"));
            console.Run();
        }
    }
}

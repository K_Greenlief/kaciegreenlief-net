# Consuming APIs: POST, PUT, and DELETE (C#)

In this exercise, you'll continue working on the command-line application that displays online auction info. The functionality that you wrote in the previous lesson is provided.

Your task is to add web API calls using RestSharp to create new auctions (`POST`), update existing auctions (`PUT`), and delete auctions (`DELETE`).

## Step One: Identify your API Key
Before you can access the web API, you need a special Key. This Key is called an `API Key`. An `API key` or application programming interface key is a code that gets passed to the web API by the client computer application. In this case, the API key is passed by the command-line application. The `API Key` is used to identify its user, developer or calling program to a website.

In your case, the API Key is the Tech Elevator Inventory Number located on the tag on your laptop. In some cases, that tag is located on the bottom or your laptop.

Add your API Key to the `APIService.cs` class.
## Step Two: Explore the API

Before moving on to the next step, explore the web API using Postman. You can access the following endpoints:

- GET: https://te-pgh-api.azurewebsites.net/api/auctions?APIKey={your_api_key}
- GET: https://te-pgh-api.azurewebsites.net/api/auctions/{id}?APIKey={your_api_key} (use a number between 1 and 7 in place of {id})

These are the endpoints you'll work on for this exercise:

- POST: https://te-pgh-api.azurewebsites.net/api/auctions?APIKey={your_api_key}
- PUT: https://te-pgh-api.azurewebsites.net/api/auctions/{id}?APIKey={your_api_key}
- DELETE: https://te-pgh-api.azurewebsites.net/api/auctions/{id}?APIKey={your_api_key}

## Step Three: Evaluation criteria and functional requirements

* All unit tests pass in `AuctionApp.Tests`.
* Code is clean, concise, and readable.

To complete this exercise, you need to complete the `APIService` class by implementing the `AddAuction()`, `UpdateAuction()`, and `DeleteAuction()` methods.

### Tips and tricks

* The `Auction` class has a constructor which takes a CSV string containing either four or five elements: Title, Description, User, Current Bid, and optionally, Auction ID.
* The URL for the API is declared in `APIService.cs`. You may need to append a slash depending on the API method you're using.
* The `AddAuction()` method takes an `Auction` object as a parameter that's passed from `Program.cs`. Have the `AddAuction()` method return the `Auction` object returned from the API when it's successful. If unsuccessful, throw an `HttpRequestException`.
* 
* The `UpdateAuction()` method takes an `Auction` object as a parameter that's passed from `Program.cs`. Have the `UpdateAuction()` method return the `Auction` object returned from the API when it's successful. If unsuccessful, throw an `HttpRequestException`.
* 
* The `DeleteAuction()` method takes an `int` as a parameter that's passed from the console. It's the `Id` of the auction to delete. Have the `DeleteAuction()` method return `true` if successful, throw an `HttpRequestException` if not successful.
* 
* Consider that the server may return an error, or that the server might not be reached. Implement the necessary error handling.

## Step Four: Add a new auction

The `AddAuction()` method creates a new auction. Make sure to handle any exceptions that might be thrown:

```csharp
public Auction AddAuction(Auction newAuction) {
    // place code here
    throw new NotImplementedException();
}
```

When you've completed the `AddAuction()` method, run the unit tests, and verify that the `AddAuction_ExpectSuccess`, `AddAuction_ExpectFailureResponse`, and `AddAuction_ExpectNoResponse` tests pass.

## Step Five: Update an existing auction

The `UpdateAuction()` method overwrites the existing auction with an updated one for a given ID. Make sure to handle any exceptions that might be thrown:

```csharp
public Auction UpdateAuction(Auction auctionToUpdate) {
    // place code here
    throw new NotImplementedException();
}
```

When you've completed the `UpdateAuction()` method, run the unit tests, and verify that the `UpdateAuction_ExpectSuccess`, `UpdateAuction_ExpectFailureResponse`, and `UpdateAuction_ExpectNoResponse` tests pass.

## Step Six: Delete an auction

The `DeleteAuction()` method removes an auction from the system. Make sure to handle any exceptions that might come up. What happens if you enter an ID for an auction that doesn't exist?

```csharp
public bool DeleteAuction(int auctionId) {
    // place code here
    throw new NotImplementedException();
}
```

When you've completed the `DeleteAuction()` method, run the unit tests, and verify that the `DeleteAuction_ExpectSuccess`, `DeleteAuction_ExpectFailureResponse`, and `DeleteAuction_ExpectNoResponse` tests pass.

Once all unit tests pass, you've completed this exercise.

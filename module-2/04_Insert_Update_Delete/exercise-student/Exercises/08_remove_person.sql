-- 8. Remove "Penn Jillette" from the person table 
-- You'll have to remove data from another table before you can make him "disappear" (Get it? Because he's a magician...) (1 row each)



delete from movie_actor 
where actor_id = (select distinct actor_id from movie_actor
join person on movie_actor.actor_id = person.person_id
where person_name = 'Penn Jillette')

delete from person
where person_name = 'Penn Jillette'


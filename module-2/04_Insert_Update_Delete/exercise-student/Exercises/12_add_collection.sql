-- 12. Create a "Bill Murray Collection" in the collection table. 
--For the movies that have Bill Murray in them, set their collection ID to the "Bill Murray Collection". (1 row, 6 rows)
BEGIN TRANSACTION

insert into collection(collection_name)
values('Bill Murray Collection')

update movie
set collection_id = (

select collection_id from collection 
where collection_name = 'Bill Murray Collection') 

where (movie.movie_id = (select distinct movie_id from movie_actor
join person on person_id = movie_actor.actor_id
where person_name = 'Bill Murray')
) 




--join movie_actor on movie.movie_id = movie_actor.movie_id
--join person on movie_actor.actor_id = person.person_id
ROLLBACK;

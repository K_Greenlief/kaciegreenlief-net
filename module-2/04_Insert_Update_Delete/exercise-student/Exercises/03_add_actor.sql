-- 3. Did you know Eric Stoltz was originally cast as Marty McFly in "Back to the Future"? 
--Add Eric Stoltz to the list of actors for "Back to the Future" (1 row)


insert into movie_actor(movie_id,actor_id)
values((select distinct movie_id from movie where title = 'Back to the Future'), (select distinct actor_id from movie_actor 
join person on movie_actor.actor_id = person.person_id
where person_name = 'Eric Stoltz'))


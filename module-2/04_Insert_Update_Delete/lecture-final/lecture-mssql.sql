-- INSERT

-- Add Disneyland to the park table. 
-- (It was established on 7/17/1955, has an area of 0.78 square miles and does not offer camping.)
INSERT INTO park(park_name,date_established,area,has_camping)
VALUES('Disneyland','7/17/1955',0.78,0)

-- Add Hawkins, IN (with a population of 30,000 and an area of 14.71 square miles) 
-- and Cicely, AK (with a popuation of 839 and an area of 4.39 square miles) to the city table.
insert into city(city_name,area,population,state_abbreviation)
values('Hawkins',14.71,30000,'IN')

insert into city
values('Cicely','AK',839,4.39)

insert into city
values('Hunts Point','WA',2500,4.39),('Yarrow Point','WA',2000,2.5)
-- Since Disneyland is in California (CA), add a record representing that to the park_state table.
insert into park_state(park_id,state_abbreviation)
select park_id,'CA' from park where park_name='Disneyland'

-- UPDATE

-- Change the state nickname of California to "The Happiest Place on Earth."
UPDATE state set state_nickname='The Happiest Place on Earth'
where state_abbreviation='CA'

-- Increase the population of California by 1,000,000.
update state set population = population + 1000000
where state_abbreviation = 'CA'
-- Change the capital of California to Anaheim.
update state set capital = (select top 1 city_id from city where city_name='Anaheim' and state_abbreviation='CA')
where state_abbreviation ='CA'

-- Change California's nickname back to "The Golden State", reduce the population by 1,000,000, and change the capital back to Sacramento.
update state
set state_nickname = 'The Golden State',
population = population - 1000000,
capital = (select top 1 city_id from city where city_name='Sacramento' and state_abbreviation='CA')
where state_abbreviation='CA'

-- DELETE

-- Delete Hawkins, IN from the city table.
delete from city where city_name='Hawkins' and state_abbreviation = 'IN'

-- Delete all cities with a population of less than 1,000 people from the city table.
delete from city where population < 1000


-- REFERENTIAL INTEGRITY

-- Try adding a city to the city table with "XX" as the state abbreviation.
insert into city
values('Nowhereland','XX',8,10)

-- Try deleting California from the state table.
delete from state where state_abbreviation='CA'

-- Try deleting Disneyland from the park table. Try again after deleting its record from the park_state table.
delete from park
where park_name='Disneyland'

delete from park_state
where park_id=(select park_id from park where park_name='Disneyland')

-- CONSTRAINTS

-- NOT NULL constraint
-- Try adding Smallville, KS to the city table without specifying its population or area.
insert into city (city_name,state_abbreviation)
values('Smallville','KS')

-- DEFAULT constraint
-- Try adding Smallville, KS again, specifying an area but not a population.
insert into city (city_name,state_abbreviation,area)
values('Smallville','KS',0.75)


-- Retrieve the new record to confirm it has been given a default, non-null value for population.

SELECT *
FROM city
WHERE city_name = 'Smallville';

-- UNIQUE constraint
-- Try changing California's nickname to "Vacationland" (which is already the nickname of Maine).
update state set state_nickname='VacationLand' where state_abbreviation='CA'

-- CHECK constraint
-- Try changing the census region of Florida (FL) to "Southeast" (which is not a valid census region).
update state set census_region='SouthEast' where state_abbreviation='Fl'


-- TRANSACTIONS

-- Delete the record for Smallville, KS within a transaction.
BEGIN TRANSACTION
select * from city where city_name='Smallville'
delete from city where city_name='Smallville'
select * from city where city_name='Smallville'
commit
select * from city where city_name='Smallville'

-- Delete all of the records from the park_state table, but then "undo" the deletion by rolling back the transaction.
BEGIN TRANSACTION
delete from park_state
select * from park_state
ROLLBACK;
select * from park_state

-- Update all of the cities to be in the state of Texas (TX), but then roll back the transaction.
begin transaction
update city set state_abbreviation='TX';
select top 10 * from city;
rollback;
select top 10 * from city;

-- Demonstrate two different SQL connections trying to access the same table where one is inside of a transaction but hasn't committed yet.

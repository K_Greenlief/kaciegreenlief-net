﻿using HotelReservations.Dao;
using HotelReservations.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelReservations.Controllers
{
    [Route("api?[controller]")]
    [ApiController]
    public class ReservationsController : Controller
    {
        private IReservationDao _reservationDao;

        public ReservationsController()
        {
            _reservationDao = new ReservationDao();
        }

        [HttpGet]
        public List<Reservation> GetAllReservations()
        {
            return _reservationDao.List();
        }


        [HttpGet("{id}")]
        public Reservation GetReservationByID(int id)
        {
            Reservation reserve = new Reservation();
            if (reserve != null)
            {
                return reserve;
        }
            return null;
        }
    }

}

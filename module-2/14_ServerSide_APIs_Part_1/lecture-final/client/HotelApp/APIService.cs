﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace HTTP_Web_Services_GET_lecture
{
    public class APIService
    {
        private readonly string API_URL;
        private readonly RestClient client = new RestClient();
        public APIService(string URL)
        {
            API_URL = URL;
        }
        public List<Hotel> GetHotels()
        {
            RestRequest request = new RestRequest(API_URL + "hotels");
            IRestResponse<List<Hotel>> response = client.Get<List<Hotel>>(request);
            return response.Data;
        }

        public List<Review> GetReviews()
        {
            RestRequest request = new RestRequest(API_URL + "reviews");
            IRestResponse<List<Review>> response = client.Get<List<Review>>(request);
            return response.Data;
        }

        public Hotel GetDetailsForHotel(int hotelId)
        {
            RestRequest request = new RestRequest(API_URL + "hotels/" + hotelId);
            IRestResponse<Hotel> response = client.Get<Hotel>(request);
            return response.Data;
        }

        public List<Review> GetReviewsForHotel(int hotelId)
        {
            RestRequest request = new RestRequest(API_URL + "hotels/" + hotelId + "/reviews");
            IRestResponse<List<Review>> response = client.Get<List<Review>>(request);
            return response.Data;
        }

        public List<Hotel> GetHotelsWithStarRating(int starRating)
        {
            RestRequest request = new RestRequest(API_URL + "hotels?stars=" + starRating);
            IRestResponse<List<Hotel>> response = client.Get<List<Hotel>>(request);
            return response.Data;
        }

        public City GetPublicAPIQuery()
        {
            RestRequest request = new RestRequest("https://api.teleport.org/api/cities/geonameid:5128581/");
            IRestResponse<City> response = client.Get<City>(request);
            return response.Data;
        }

    }
}

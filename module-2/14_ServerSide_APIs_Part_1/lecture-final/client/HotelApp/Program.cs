﻿using RestSharp;
using System;
using System.Collections.Generic;

namespace HTTP_Web_Services_GET_lecture
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleService cli = new ConsoleService(new APIService("https://localhost:44322/api/"));
            cli.Run();
        }


    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyFirstWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        private static List<Locations> locations = new List<Locations>();
        public LocationsController()
        {
            if (locations.Count == 0)
            {
                locations.Add(new Locations(1,
                    "Tech Elevator Cleveland",
                    "7100 Euclid Ave #14",
                    "Cleveland",
                    "OH",
                    "44103"));
                locations.Add(new Locations(2,
                        "Tech Elevator Columbus",
                        "1275 Kinnear Rd #121",
                        "Columbus",
                        "OH",
                        "43212"));
                locations.Add(new Locations(3,
                        "Tech Elevator Cincinnati",
                        "1776 Mentor Ave Suite 355",
                        "Cincinnati",
                        "OH",
                        "45212"));
                locations.Add(new Locations(4,
                        "Tech Elevator Pittsburgh",
                        "901 Pennsylvania Ave #3",
                        "Pittsburgh",
                        "PA",
                        "15233"));
                locations.Add(new Locations(5,
                        "Tech Elevator Detroit",
                        "440 Burroughs St #316",
                        "Detroit",
                        "MI",
                        "48202"));
                locations.Add(new Locations(6,
                        "Tech Elevator Philadelphia",
                        "30 S 17th St",
                        "Philadelphia",
                        "PA",
                        "19103"));
            }
        }

        [HttpGet]
        public List<Locations> GetLocations()
        {
            return locations;
        }
        [HttpPost]
        public Locations AddingALocation(Locations newLocation)
        {
            if(newLocation != null)
            {
                locations.Add(newLocation);
                return newLocation;
            }
            return null;
        }
    }
}

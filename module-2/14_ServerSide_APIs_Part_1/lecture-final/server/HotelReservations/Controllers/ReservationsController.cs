﻿using HotelReservations.Dao;
using HotelReservations.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelReservations.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        private IReservationDao _reservationDao;

        public ReservationsController()
        {
            _reservationDao = new ReservationDao();
        }

        [HttpGet]
        public List<Reservation> GetAllReservations()
        {
            return _reservationDao.List();
        }
        //[HttpGet("{id}",Name ="GetReservation")]
        //public Reservation GetAllReservationByID(int id)
        //{
        //    List<Reservation> all = _reservationDao.List();
        //    foreach (Reservation item in all)
        //    {
        //        if(item.Id == id)
        //        {
        //            return item;
        //        }
        //    }
        //    return null;
        //}
        [HttpGet("{hotelid}")]
        public Reservation GetReservationByID(int hotelid)
        {
            Reservation reservation = _reservationDao.Get(hotelid);
            if(reservation != null)
            {
                return reservation;
            }
            return null;
        }
        [HttpPost]
        public ActionResult AddNewReservation(Reservation newReservation)
        {
            Reservation addedReservation = _reservationDao.Create(newReservation);
            return CreatedAtRoute("GetReservation", new { id = addedReservation.Id }, addedReservation);
        }

        // You can override the base route, but it's generally considered bad practice.
        // Keep all the specific base routes in one controller.
        //[HttpGet("/api/hotels/{id}/reservations")]
        //public List<Reservation> GetReservationsByHotel(int id)
        //{
        //    return _reservationDao.FindByHotel(id);
        //}

    }
}

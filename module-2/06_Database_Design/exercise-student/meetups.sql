USE master
GO

DROP DATABASE IF EXISTS MeetupsDB
GO

CREATE DATABASE MeetupsDB;
GO

USE MeetupsDB;
GO

BEGIN TRANSACTION

CREATE TABLE Member
(
MemberId  int	identity(1,1),
LastName nvarchar(20) null,
FirstName nvarchar(15) not null,
Email nvarchar(50) not null,
PhoneNumber nvarchar(15) null,
Birthday date null,
wants_emails bit not null

constraint pk_singlemember primary key(MemberId)
)

CREATE TABLE InterestGroup
(
GroupId   int	identity(1,1),
GroupName nvarchar(MAX) not null,

constraint pk_interestgroup primary key(GroupId)
)

CREATE TABLE Event
(
EventId   int	identity(1,1),
EventName nvarchar(50) not null,
EventDescription nvarchar(MAX) not null,
EventDate date not null,
EventTime time not null,
EventDuration int check (EventDuration >= 30),
GroupId int not null,

constraint pk_event primary key(EventId),
constraint fk_event_InterestGroup foreign key(GroupId) references InterestGroup(GroupId)
)

CREATE TABLE Group_Member
(
GroupId	int	not null,
MemberId int not null

constraint pk_idofmember primary key(MemberId, GroupId)
)

CREATE TABLE Event_Member
(
EventId int not null,
MemberId int not null

constraint pk_member primary key(MemberId, EventId)
)

COMMIT



INSERT into InterestGroup (GroupName)
VALUES('The Lonely Hearts Club'),
('Rock and Roll Party Queens'),
('Dance Dance Revolutionizers');


INSERT into Member (LastName, FirstName, Email, Birthday, wants_emails)
VALUES ('Greenlief','Kacie','kaciecraig.21@gmail.com','1994-09-07',1),
('Greenlief','Zach','zachgreenlief@gmail.com','1993-02-24',0),
('Smith','John','johnsmith@fakeemail.com','1999-01-01',1),
('AndTheSunshineBand','Casey','sunshine@yahoo.com','2021-06-14',0),
('Abraham','Father','manysons@hotmail.com','1003-02-02',1),
('Stinespring','Darrel','darrelwayne@nunya.com','1972-01-01',0),
('Stinespring','Marilee','emilyrose@hotmail.com','1983-01-19',0),
('Rose','Emily','demonspawn28@gmail.com','2000-03-28',1)

INSERT into Event (EventName, EventDescription, EventDate, EventTime, EventDuration, GroupId)
VALUES ('EventOne','Super happy fun partytime','2021-06-06', '15:00:00', 30, 1),
('EventTwo','It was the best of times, it was the worst of times', '2021-06-12', '14:00:00', 40, 2),
('EventThree','Get it on like Donkey Kong', '2021-06-13', '13:00:00', 50, 3),
('EventFour','Smoother than a fresh jar of Skippy', '2021-06-12', '12:00:00', 60, 2)

insert into Event_Member (EventId,MemberId)
VALUES(1,1),(2,2),(3,4),(4,8)

insert into Group_Member(GroupId,MemberId)
values(1,6),(1,8),(2,1),(2,7),(3,1),(3,2)
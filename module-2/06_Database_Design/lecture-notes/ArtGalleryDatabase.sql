-- makes sure we're on the "root" of the database server
USE master
GO

-- Delete our database if it exists
DROP DATABASE IF EXISTS ArtGallery

-- Create a new ArtGallery DB
CREATE DATABASE ArtGallery;
GO

-- Switch to ArtGallery before building tables.
USE ArtGallery
GO

BEGIN TRANSACTION
CREATE TABLE Customer_Address
(
	CustomerID		int		not null,
	AddressID		int		not null,

	constraint pk_customer_address primary key (CustomerID, AddressID)
);
CREATE TABLE Customers
(
	CustomerID		int				identity(1,1),
	FirstName		nvarchar(20)	not null,
	LastName		nvarchar(50)	null,
	PhoneNumber		nvarchar(15)	not null,
	Extension		nvarchar(5)		null,
	Email			nvarchar(MAX)	not null,
	CustomerSince	datetime		not null,

	constraint pk_customers primary key(CustomerID)
);
CREATE TABLE State
(
	StateID	int	identity(1,1),
	Name	varchar(50)	not null,
	Abbreviation	varchar(2)	not null,

	constraint pk_state primary key(StateID)
);
CREATE TABLE Addresses
(
	AddressID	int			identity(1,1),
	Address1	varchar(50)	not null,
	Address2	varchar(50)	null,
	StateID		int			not null,
	Zip			nvarchar(15)	not null,
	City		nvarchar(50)	not null,
	Country		nvarchar(25)	not null,
	isBilling	bit				not null,

	constraint pk_addresses primary key(AddressID),
	constraint fk_addresses_state foreign key(StateID) references State(StateID)
);



-- After all the tables are create, now add foreign keys/constraints
ALTER TABLE Customer_Address ADD constraint fk_customer_address_customer foreign key(CustomerID) references Customers(CustomerID)
ALTER TABLE Customer_Address ADD constraint fk_customer_address_address foreign key(AddressID) references Addresses(AddressID)
ALTER TABLE Customers ADD constraint sinceDate DEFAULT GETDATE() FOR CustomerSince
COMMIT
﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionApp
{
    public class APIService
    {
        private string API_URL { get; }
        private string API_KEY { get; }
        private readonly RestClient client = new RestClient();
        public APIService(string URL, string key)
        {
            API_URL = URL;
            API_KEY = key;
        }

        public List<Auction> GetAllAuctions()
        {
            RestRequest request = new RestRequest($"{API_URL}?APIKey={API_KEY}");
            IRestResponse<List<Auction>> response = client.Get<List<Auction>>(request);
            return response.Data;

        }

        public Auction GetDetailsForAuction(int auctionId)
        {
            RestRequest request = new RestRequest($"{API_URL}{auctionId}?APIKey={API_KEY}");
            IRestResponse<Auction> response = client.Get<Auction>(request);
            return response.Data;

        }

        public List<Auction> GetAuctionsSearchTitle(string searchTitle)
        {
            RestRequest request = new RestRequest(API_URL + "?APIKey=" + API_KEY);
            IRestResponse<List<Auction>> response = client.Get<List<Auction>>(request);
            List<Auction> newAuctionList = new List<Auction>();
            foreach (Auction item in response.Data)
            {
                if (item.Title.ToLower().Contains(searchTitle.ToLower()))
                {
                    newAuctionList.Add(item);
                }

            }

            return newAuctionList;
        }

        public List<Auction> GetAuctionsSearchPrice(double searchPrice)
        {
            RestRequest request = new RestRequest($"{API_URL}?APIKey={API_KEY}");
            IRestResponse<List<Auction>> response = client.Get<List<Auction>>(request);
            List<Auction> itemByPrice = new List<Auction>();
            foreach (Auction item in response.Data)
            {
                if (item.CurrentBid <= searchPrice)
                {
                    itemByPrice.Add(item);
                }
            }
            return itemByPrice;

        }
    }
}

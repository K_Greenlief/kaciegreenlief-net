﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace HTTP_Web_Services_GET_lecture
{
    public class APIService
    {
        private readonly string API_URL = "https://te-pgh-api.azurewebsites.net/api/";
        private readonly string API_KEY = "000000";
        private readonly RestClient client = new RestClient();

        public List<Hotel> GetHotels()
        {
            RestRequest request = new RestRequest(API_URL + "hotels?APIKey=" + API_KEY);
            IRestResponse<List<Hotel>> response = client.Get<List<Hotel>>(request);
            return response.Data;
        }
        public List<Review> GetReviews()
        {
            RestRequest request = new RestRequest(API_URL + "reviews?APIKey=" + API_KEY);
            IRestResponse<List<Review>> response = client.Get<List<Review>>(request);
            return response.Data;
        }

        public Hotel GetDetailsForHotelByID(int hotelID)
        {
            RestRequest request = new RestRequest(API_URL + "hotels/" + hotelID + "?APIKey=" + API_KEY);
            IRestResponse<Hotel> response = client.Get<Hotel>(request);
            return response.Data;
        }

        public List<Review> GetReviewsForHotel(int hotelID)
        {
            RestRequest request = new RestRequest(API_URL + "hotels/" + hotelID + "/reviews?APIKey=" + API_KEY);
            IRestResponse<List<Review>> response = client.Get<List<Review>>(request);
            return response.Data;
        }

        public List<Hotel> GetHotelsByRating(int starRating)
        {
            RestRequest request = new RestRequest(API_URL + "hotels?APIKey=" + API_KEY + "&stars=" + starRating);
            IRestResponse<List<Hotel>> response = client.Get<List<Hotel>>(request);
            return response.Data;
        }

        public City GetPublicAPIQuery()
        {
            RestRequest request = new RestRequest("https://api.teleport.org/api/cities/geonameid%3A5128581/");
            IRestResponse<City> response = client.Get<City>(request);
            return response.Data;
        }
    }
}

﻿using RestSharp;
using System;
using System.Collections.Generic;

namespace AuctionApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            const string BASE_URL = "https://te-pgh-api.azurewebsites.net/api/auctions/";
            // add your API key here
            const string API_KEY = "student-final";

            ConsoleService cli = new ConsoleService(new APIService(BASE_URL,API_KEY));
            cli.Run();
        }
    }
}

﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionApp
{
    public class APIService
    {
        readonly RestClient client = new RestClient();
        private string API_URL { get; }
        private string API_KEY { get; }
        public APIService(string URL, string key)
        {
            API_URL = URL;
            API_KEY = key;
        }

        public List<Auction> GetAllAuctions()
        {
            RestRequest request = new RestRequest(API_URL + "?APIKey=" + API_KEY);
            IRestResponse<List<Auction>> response = client.Get<List<Auction>>(request);
            return response.Data;
        }

        public Auction GetDetailsForAuction(int auctionId)
        {
            RestRequest requestOne = new RestRequest(API_URL + "/" + auctionId + "?APIKey=" + API_KEY);
            IRestResponse <Auction> response = client.Get<Auction>(requestOne);
            return response.Data;
        }

        public List<Auction> GetAuctionsSearchTitle(string searchTitle)
        {
            RestRequest request = new RestRequest(API_URL + "?APIKey=" + API_KEY + "&title_like=" + searchTitle);
            IRestResponse<List<Auction>> response = client.Get<List<Auction>>(request);
            return response.Data;
        }

        public List<Auction> GetAuctionsSearchPrice(double searchPrice)
        {
            RestRequest request = new RestRequest(API_URL + "?APIKey=" + API_KEY + "&currentBid_lte=" + searchPrice);
            IRestResponse<List<Auction>> response = client.Get<List<Auction>>(request);
            return response.Data;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using VendingMachine.Classes;

namespace VendingMachine.DAO
{
    public class DBDAO : IDAO
    {
        private readonly string connectionString;
        private Guid vm_ID;

        public DBDAO(string connectionString,Guid VM_ID)
        {
            this.connectionString = connectionString;
            vm_ID = VM_ID;
        }
        public Dictionary<string, Queue<VendingMachineItem>> LoadMachine(string filePath)
        {
            Dictionary<string, Queue<VendingMachineItem>> inventory = new Dictionary<string, Queue<VendingMachineItem>>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand("select * from VM_Items order by SlotID;", conn);
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        string slot = Convert.ToString(sqlDataReader["slotid"]);
                        inventory[slot] = CreateVMItemQueueFromReader(sqlDataReader);
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            return inventory;
        }

        public void LogTransaction(string action, decimal txAmount, decimal currentBalance)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand("insert into VM_Log(Action,StartValue,EndValue,VM_ID)" +
                        " VALUES(@action,@startValue,@endValue,@VM_ID)", conn);
                    sqlCommand.Parameters.AddWithValue("@action", action);
                    sqlCommand.Parameters.AddWithValue("@startValue", txAmount);
                    sqlCommand.Parameters.AddWithValue("@endValue", currentBalance);
                    sqlCommand.Parameters.AddWithValue("@VM_ID", vm_ID);
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public void ResetLog()
        {
            // Non-money making way
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand("delete from VM_LOG where VM_ID=@vm_id;", conn);
                    sqlCommand.Parameters.AddWithValue("@vm_id", vm_ID);
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private Queue<VendingMachineItem> CreateVMItemQueueFromReader(SqlDataReader reader)
        {
            // Set up a local queue that represents the items in a slot
            Queue<VendingMachineItem> queue = new Queue<VendingMachineItem>();
            // set up data
            string itemDesc = Convert.ToString(reader["Description"]);
            decimal price = Convert.ToDecimal(reader["price"]);
            string snackType = Convert.ToString(reader["snacktype"]);

            // Check the type of item in a slot based on the input files
            switch (snackType)
            {
                case "Chip":
                    //instantiate the Queue
                    queue = new Queue<VendingMachineItem>();
                    // Add 5 objects to the queue
                    for (int i = 0; i < 5; i++)
                    {
                        // declare an instantiate the product item
                        ChipItem chipItem = new ChipItem(itemDesc, price);
                        // Add it to the queue
                        queue.Enqueue(chipItem);
                    }
                    break;
                case "Drink":
                    //instantiate the Queue
                    queue = new Queue<VendingMachineItem>();
                    // Add 5 objects to the queue
                    for (int i = 0; i < 5; i++)
                    {
                        // declare an instantiate the product item
                        BeverageItem chipItem = new BeverageItem(itemDesc, price);
                        // Add it to the queue
                        queue.Enqueue(chipItem);
                    }
                    break;
                case "Candy":
                    queue = new Queue<VendingMachineItem>();
                    for (int i = 0; i < 5; i++)
                    {
                        CandyItem chipItem = new CandyItem(itemDesc, price);
                        queue.Enqueue(chipItem);
                    }
                    break;
                case "Gum":
                    queue = new Queue<VendingMachineItem>();
                    for (int i = 0; i < 5; i++)
                    {
                        GumItem chipItem = new GumItem(itemDesc, price);
                        queue.Enqueue(chipItem);
                    }
                    break;
                default:
                    break;
            }
            return queue;
        }
    }
}

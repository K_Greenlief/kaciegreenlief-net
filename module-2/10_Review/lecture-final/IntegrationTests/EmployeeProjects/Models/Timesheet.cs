﻿using System;

namespace EmployeeProjects.Models
{
    public class Timesheet
    {
        public int TimesheetId { get; set; }

        public int EmployeeId { get; set; }

        public int ProjectId { get; set; }

        public DateTime DateWorked { get; set; }

        public decimal HoursWorked { get; set; }

        public bool IsBillable { get; set; }

        public string Description { get; set; }

        public Timesheet() { }

        public Timesheet(int timesheetId, int employeeId, int projectId, DateTime dateWorked, decimal hoursWorked, bool isBillable, string description)
        {
            TimesheetId = timesheetId;
            EmployeeId = employeeId;
            ProjectId = projectId;
            DateWorked = dateWorked;
            HoursWorked = hoursWorked;
            IsBillable = isBillable;
            Description = description;
        }

        public override bool Equals(object obj)
        {
            // Did I get an object
            if(obj == null)
            {
                return false;
            }
            // Did I get the exact same object?
            if(obj == this)
            {
                return true;
            }
            // Is the obj the same type
            if(GetType() != obj.GetType())
            {
                return false;
            }
            // Compare the fields
            bool theyMatch = true;
            Timesheet timesheetIn = (Timesheet)obj;
            theyMatch = TimesheetId == timesheetIn.TimesheetId && theyMatch;
            theyMatch = EmployeeId == timesheetIn.EmployeeId && theyMatch;
            theyMatch = ProjectId == timesheetIn.ProjectId && theyMatch;
            theyMatch = DateWorked == timesheetIn.DateWorked && theyMatch;
            theyMatch = HoursWorked == timesheetIn.HoursWorked && theyMatch;
            theyMatch = IsBillable == timesheetIn.IsBillable && theyMatch;
            theyMatch = Description == timesheetIn.Description && theyMatch;

            return theyMatch;
        }
    }
}

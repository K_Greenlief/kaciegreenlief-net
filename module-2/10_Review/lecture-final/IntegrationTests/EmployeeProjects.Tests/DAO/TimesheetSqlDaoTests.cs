﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using EmployeeProjects.DAO;
using EmployeeProjects.Models;

namespace EmployeeProjects.Tests.DAO
{
    [TestClass]
    public class TimesheetSqlDaoTests : EmployeeProjectsDaoTests
    {
        private static readonly Timesheet TIMESHEET_1 = new Timesheet(1, 1, 1, DateTime.Parse("2021-01-01"), 1.8M, true, "Timesheet 1");
        private static readonly Timesheet TIMESHEET_2 = new Timesheet(2, 1, 1, DateTime.Parse("2021-01-02"), 1.5M, true, "Timesheet 2");
        private static readonly Timesheet TIMESHEET_3 = new Timesheet(3, 2, 1, DateTime.Parse("2021-01-01"), 0.25M, true, "Timesheet 3");
        private static readonly Timesheet TIMESHEET_4 = new Timesheet(4, 2, 2, DateTime.Parse("2021-02-01"), 2.0M, false, "Timesheet 4");

        private Timesheet testTimesheet;

        private TimesheetSqlDao dao;


        [TestInitialize]
        public override void Setup()
        {
            dao = new TimesheetSqlDao(ConnectionString);
            testTimesheet = new Timesheet(999, 2, 1, DateTime.UtcNow, 40.0M, true, "Our test timesheet");
            base.Setup();
        }

        [TestMethod]
        public void GetTimesheet_ReturnsCorrectTimesheetForId()
        {
            // Arrange
            // use TIMESHEET_1 as an expected
            // ACT
            Timesheet actual = dao.GetTimesheet(1);
            // Assert
            // Did I get back a timesheet?
            Assert.IsNotNull(actual);
            // Can't do it, it's comparing values on the stack
            // Assert.AreEqual(TIMESHEET_1, actual);
            AssertTimesheetsMatch(TIMESHEET_1, actual);

            // another timesheet
            actual = dao.GetTimesheet(2);
            AssertTimesheetsMatch(TIMESHEET_2, actual);
            actual = dao.GetTimesheet(3);
            AssertTimesheetsMatch(TIMESHEET_3, actual);
            actual = dao.GetTimesheet(4);
            AssertTimesheetsMatch(TIMESHEET_4, actual);

        }

        [TestMethod]
        public void GetTimesheet_ReturnsNullWhenIdNotFound()
        {
            Assert.Fail();
        }

        [TestMethod]
        public void GetTimesheetsByEmployeeId_ReturnsListOfAllTimesheetsForEmployee()
        {
            // Arrange
            // Act
            IList<Timesheet> actual = dao.GetTimesheetsByEmployeeId(1);
            // Assert
            // Did I get anything back
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(TIMESHEET_1, actual[0]);
            AssertTimesheetsMatch(TIMESHEET_1, actual[0]);
            AssertTimesheetsMatch(TIMESHEET_2, actual[1]);

            actual = dao.GetTimesheetsByEmployeeId(2);
            Assert.AreEqual(2, actual.Count);
            AssertTimesheetsMatch(TIMESHEET_3, actual[0]);
            AssertTimesheetsMatch(TIMESHEET_4, actual[1]);

        }

        [TestMethod]
        public void GetTimesheetsByProjectId_ReturnsListOfAllTimesheetsForProject()
        {
            Assert.Fail();
        }

        [TestMethod]
        public void CreateTimesheet_ReturnsTimesheetWithIdAndExpectedValues()
        {
            // Arrange 
            // Added a timesheet object to TestInitialize
            // Act
            Timesheet actual = dao.CreateTimesheet(testTimesheet);
            // Did I get something inserted
            Assert.IsTrue(actual.TimesheetId > 0);
            testTimesheet.TimesheetId = actual.TimesheetId;
            AssertTimesheetsMatch(testTimesheet, actual);
        }

        [TestMethod]
        public void CreatedTimesheetHasExpectedValuesWhenRetrieved()
        {
            Assert.Fail();
        }

        [TestMethod]
        public void UpdatedTimesheetHasExpectedValuesWhenRetrieved()
        {
            Assert.Fail();
        }

        [TestMethod]
        public void DeletedTimesheetCantBeRetrieved()
        {
            Assert.Fail();
        }

        [TestMethod]
        public void GetBillableHours_ReturnsCorrectTotal()
        {
            Assert.Fail();
        }

        private void AssertTimesheetsMatch(Timesheet expected, Timesheet actual)
        {
            Assert.AreEqual(expected.TimesheetId, actual.TimesheetId);
            Assert.AreEqual(expected.EmployeeId, actual.EmployeeId);
            Assert.AreEqual(expected.ProjectId, actual.ProjectId);
            Assert.AreEqual(expected.DateWorked, actual.DateWorked);
            Assert.AreEqual(expected.HoursWorked, actual.HoursWorked);
            Assert.AreEqual(expected.IsBillable, actual.IsBillable);
            Assert.AreEqual(expected.Description, actual.Description);
        }
    }
}

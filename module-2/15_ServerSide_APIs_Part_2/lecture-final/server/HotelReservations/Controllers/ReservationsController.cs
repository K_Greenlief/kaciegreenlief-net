﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelReservations.Dao;
using HotelReservations.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HotelReservations.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        private static IReservationDao reservationDao;

        public ReservationsController(IReservationDao _reservationDao)
        {
            reservationDao = _reservationDao;
        }

        [HttpGet]
        public List<Reservation> ListReservations()
        {
            return reservationDao.List();
        }

        [HttpGet("{id}")]
        public ActionResult<Reservation> GetReservation(int id)
        {
            Reservation reservation = reservationDao.Get(id);

            if (reservation != null)
            {
                return Accepted(reservation);
            }
            else
            {
                return NotFound();
            }
        }
        [HttpPost]
        public ActionResult<Reservation> AddReservation(Reservation reservation)
        {
            Reservation added = reservationDao.Create(reservation);
            return Created($"/reservations/{added.Id}", added);
        }

        [HttpPut("{id}")]
        public ActionResult<Reservation> UpdateReservation(int id, Reservation reservationIn)
        {
            Reservation existingReservation = reservationDao.Get(id);
            if (existingReservation == null)
            {
                return NotFound("Reservation doesn't exist.");
            }
            if (id != reservationIn.Id)
            {
                return BadRequest();
            }
            Reservation reservation = reservationDao.Update(id,reservationIn);
            return Ok(reservation);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteReservation(int id)
        {
            Reservation existingReservation = reservationDao.Get(id);
            if (existingReservation == null)
            {
                return NotFound("Reservation doesn't exist.");
            }
            bool result = reservationDao.Delete(id);
            if (result)
            {
                return NoContent();
            }
            return StatusCode(418);
        }

    }
}

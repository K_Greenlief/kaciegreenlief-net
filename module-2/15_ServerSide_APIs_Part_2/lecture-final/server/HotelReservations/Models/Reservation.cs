﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace HotelReservations.Models
{
    public class Reservation
    {
        public int? Id { get; set; }

        public int HotelID { get; set; }
        [Required]
        [StringLength(15)]
        [JsonProperty("GuestName")]
        public string FullName { get; set; }
        [Required(ErrorMessage = "We need to know when you are arriving.")]
        public string CheckinDate { get; set; }
        [Required]
        public string CheckoutDate { get; set; }
        [Range(1,5)]
        public int Guests { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public Reservation() { }
        public Reservation(int? id, int hotelId, string fullName, string checkinDate, string checkoutDate, int guests)
        {
            Id = id ?? new Random().Next(100, int.MaxValue);
            HotelID = hotelId;
            FullName = fullName;
            CheckinDate = checkinDate;
            CheckoutDate = checkoutDate;
            Guests = guests;
        }
    }
}

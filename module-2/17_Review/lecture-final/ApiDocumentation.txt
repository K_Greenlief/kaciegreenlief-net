GET api/movies
List of Type Movie
{
    movie_id (int)
    title (string)
    overview (string)
    tagline (string)
    poster_path (string)
    home_page (string)
    release_date (datetime)
    collection_name (string)
    length_minutes (int)
}
GET api/movies/{id}
Object of Type Movie (see api/movies)
GET api/movies/{movie_id}/director
object type Person
{
    person_id (int)
    person_name (string)
    birthday (datetime)
    deathday (datetime)
    biography (string)
    profile_path (string)
    home_page (string)
}
GET api/movies/{movie_id}/actors
List of object type Person (see above)
GET api/actors
List of object type person with movies
{
    person_id (int)
    person_name (string)
    birthday (datetime)
    deathday (datetime)
    biography (string)
    profile_path (string)
    home_page (string)
    StarredIn (array of movies)
}
POST api/movies
Input
{
    title (string)
    overview (string)
    tagline (string)
    poster_path (string)
    home_page (string)
    release_date (datetime)
    length_minutes (int)
}
output
{
    movie_id (int)
    title (string)
    overview (string)
    tagline (string)
    poster_path (string)
    home_page (string)
    release_date (datetime)
    length_minutes (int)
}

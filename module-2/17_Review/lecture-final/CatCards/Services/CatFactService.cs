﻿using CatCards.Models;
using RestSharp;
using System.Net.Http;

namespace CatCards.Services
{
    public class CatFactService : ICatFactService
    {
        private RestClient client = new RestClient();
        private string API_URL = "https://catfact.ninja/fact";

        public CatFact GetFact()
        {
            RestRequest request = new RestRequest(API_URL);
            IRestResponse<CatFact> response = client.Get<CatFact>(request);
            if(response.ResponseStatus == ResponseStatus.Completed && response.IsSuccessful)
            {
                return response.Data;
            } else
            {
                throw new HttpRequestException("Something went wrong");
            }
        }
    }
}

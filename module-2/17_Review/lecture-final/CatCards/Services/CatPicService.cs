﻿using CatCards.Models;
using RestSharp;
using System.Net.Http;

namespace CatCards.Services
{
    public class CatPicService : ICatPicService
    {
        private RestClient client = new RestClient();
        private string API_URL = "https://random-cat-api.netlify.app/.netlify/functions/api";
        public CatPic GetPic()
        {
            // TODO is make a request to https://random-cat-api.netlify.app/.netlify/functions/api
            RestRequest request = new RestRequest(API_URL);
            // TODO deserialize the JSON into a CatPic object
            IRestResponse<CatPic> response = client.Get<CatPic>(request);
            // TODO return that CatPic object
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new HttpRequestException("Can't reach the server");
            } else if(!response.IsSuccessful)
            {
                throw new HttpRequestException(response.StatusCode.ToString());
            }
            return response.Data;
        }
    }
}

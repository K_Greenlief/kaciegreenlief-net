﻿using System.Collections.Generic;
using CatCards.DAO;
using CatCards.Models;
using CatCards.Services;
using Microsoft.AspNetCore.Mvc;

namespace CatCards.Controllers
{
    [Route("api/cards")]
    [ApiController]
    public class CatsController : ControllerBase
    {
        private readonly ICatCardDao cardDao;
        private readonly ICatFactService catFactService;
        private readonly ICatPicService catPicService;

        public CatsController(ICatCardDao _cardDao, ICatFactService _catFact, ICatPicService _catPic)
        {
            catFactService = _catFact;
            catPicService = _catPic;
            cardDao = _cardDao;
        }

        [HttpGet]
        public ActionResult<List<CatCard>> GetAllCards()
        {
            return Ok(cardDao.GetAllCards());
        }
        [HttpGet("{ccID}")]
        public ActionResult<CatCard> GetACardByID(int ccID)
        {
            CatCard catCard = cardDao.GetCard(ccID);
            if (catCard == null)
            {
                return NotFound();
            }
            return Ok(catCard);
        }
        [HttpGet("random")]
        public ActionResult<CatCard> GetRandomCard()
        {
            CatCard card = null;
            try
            {
                CatPic pic = catPicService.GetPic();
                CatFact catFact = catFactService.GetFact();

                card = new CatCard
                {
                    ImgUrl = pic.File,
                    CatFact = catFact.Fact
                };
            }
            catch (System.Exception e)
            {

                return BadRequest();
            }
            return card;
        }

        [HttpPost]
        public ActionResult<CatCard> AddCard(CatCard incomingCard)
        {
            try
            {
                CatCard addedCard = cardDao.SaveCard(incomingCard);
                return Created("api/cards/" + addedCard.CatCardId, addedCard);
            }
            catch (System.Exception e)
            {

                return BadRequest();
            }
        }
        [HttpPut("{id}")]
        public ActionResult<CatCard> UpdateCard(int id,CatCard changedCard)
        {
            if (cardDao.GetCard(id) != null)
            {
                if(id == changedCard.CatCardId)
                {
                    cardDao.UpdateCard(changedCard);
                    return Ok(changedCard);
                }
            }
            return NotFound();
        }
        [HttpDelete("{id}")]
        public ActionResult DeleteCard(int id)
        {
            if (cardDao.GetCard(id) == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    if (cardDao.RemoveCard(id))
                    {
                        return NoContent();
                    }
                }
                catch (System.Exception)
                {

                    return BadRequest();
                }
            }
            return BadRequest();
        }
    }
}

Bug 1
-----
Test that demonstrates problem: GetBillableHours_ReturnsCorrectTotal

Expected output: 0

Actual output: 2

How did you fix this bug? : IBillable was not ignoring 'false' as it was supposed to. Added is_billable = 1 
to SQL Command statement to ensure that it wsa true.


Bug 2
-----
Test that demonstrates problem: UpdatedTimesheetHasExpectedValuesWhenRetrieved

Expected output: isBillable = false

Actual output: true

How did you fix this bug? added is_billable = @is_billable to SQL Command so all parameters could be updated


Bug 3
-----
Test that demonstrates problem: GetTimesheetsByProjectId 

Expected output: List<Timesheet> expected = new List<Timesheet>() { TIMESHEET_1, TIMESHEET_2, TIMESHEET_3 };

Actual output:  Test method EmployeeProjects.Tests.DAO.TimesheetSqlDaoTests.GetTimesheetsByProjectId_ReturnsListOfAllTimesheetsForProject threw exception: 
    System.ArgumentOutOfRangeException: Index was out of range. Must be non-negative and less than the size of the collection. (Parameter 'index')
 

How did you fix this bug? changed SQL command from employee_id = @project_id to project_id = @project_id


Bug 4
-----
Test that demonstrates problem: GetTimesheetByEmployeeId

Expected output: 2

Actual output: 1

How did you fix this bug? changed 'if' statement to 'while' loop to ensure reading of all values


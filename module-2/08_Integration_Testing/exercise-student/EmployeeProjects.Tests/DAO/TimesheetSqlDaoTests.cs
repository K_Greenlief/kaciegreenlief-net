﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using EmployeeProjects.DAO;
using EmployeeProjects.Models;

namespace EmployeeProjects.Tests.DAO
{
    [TestClass]
    public class TimesheetSqlDaoTests : EmployeeProjectsDaoTests
    {
        private static readonly Timesheet TIMESHEET_1 = new Timesheet(1, 1, 1, DateTime.Parse("2021-01-01"), 1.0M, true, "Timesheet 1");
        private static readonly Timesheet TIMESHEET_2 = new Timesheet(2, 1, 1, DateTime.Parse("2021-01-02"), 1.5M, true, "Timesheet 2");
        private static readonly Timesheet TIMESHEET_3 = new Timesheet(3, 2, 1, DateTime.Parse("2021-01-01"), 0.25M, true, "Timesheet 3");
        private static readonly Timesheet TIMESHEET_4 = new Timesheet(4, 2, 2, DateTime.Parse("2021-02-01"), 2.0M, false, "Timesheet 4");

        private TimesheetSqlDao dao;


        [TestInitialize]
        public override void Setup()
        {
            dao = new TimesheetSqlDao(ConnectionString);
            base.Setup();
        }

        [TestMethod]
        public void GetTimesheet_ReturnsCorrectTimesheetForId()
        {
            //arrange
            int testInput = 2;
            Timesheet expected = TIMESHEET_2;

            //act
            Timesheet testSheet = dao.GetTimesheet(testInput);
            //assert
            AssertTimesheetsMatch(expected,testSheet);
        }

        [TestMethod]
        public void GetTimesheet_ReturnsNullWhenIdNotFound()
        {
            //arrange
            int testInput = 28;
            //act
            Timesheet testSheet = dao.GetTimesheet(testInput);
            //assert
            Assert.IsNull(testSheet);
        }

        [TestMethod]
        public void GetTimesheetsByEmployeeId_ReturnsListOfAllTimesheetsForEmployee()
        {
            //arrange
            int testInput = 2;
            List<Timesheet> expected = new List<Timesheet>() { TIMESHEET_3, TIMESHEET_4 };

            //act
            List<Timesheet> testList = new List<Timesheet>(dao.GetTimesheetsByEmployeeId(testInput));
            //assert

            for (int i = 0; i < expected.Count; i++)
            {
                AssertTimesheetsMatch(expected[i], testList[i]);
            }
            
        }

        [TestMethod]
        public void GetTimesheetsByProjectId_ReturnsListOfAllTimesheetsForProject()
        {
            //arrange
            int testInput = 1;
            List<Timesheet> expected = new List<Timesheet>() { TIMESHEET_1, TIMESHEET_2, TIMESHEET_3 };

            //act
            List<Timesheet> testList = new List<Timesheet>(dao.GetTimesheetsByProjectId(testInput));
            //assert

            for (int i = 0; i < expected.Count; i++)
            {
                AssertTimesheetsMatch(expected[i], testList[i]);
            }
        }

        [TestMethod]
        public void CreateTimesheet_ReturnsTimesheetWithIdAndExpectedValues()
        {
            //arrange
            Timesheet testTIMESHEET = new Timesheet(11, 2, 2, DateTime.Parse("2021-02-01"), 2.0M, false, "Timesheet 11");

            //act
            Timesheet expected = dao.CreateTimesheet(testTIMESHEET);
            //assert
            AssertTimesheetsMatch(expected, testTIMESHEET);
        }

        [TestMethod]
        public void CreatedTimesheetHasExpectedValuesWhenRetrieved()
        {
            //arrange
            Timesheet testTIMESHEET = new Timesheet(11, 2, 2, DateTime.Parse("2021-02-01"), 2.0M, false, "Timesheet 11");
            Timesheet expected = dao.CreateTimesheet(testTIMESHEET);
            //act
            Timesheet testSheet = dao.GetTimesheet(11);
            //assert
            AssertTimesheetsMatch(expected, testSheet);
        }

        [TestMethod]
        public void UpdatedTimesheetHasExpectedValuesWhenRetrieved()
        {
            Timesheet TIMESHEET_3Updated = new Timesheet(3, 2, 1, DateTime.Parse("2021-07-07"), 0.5M, false, "Timesheet 3");
           
            //act
            dao.UpdateTimesheet(TIMESHEET_3Updated);
            Timesheet expected = dao.GetTimesheet(TIMESHEET_3.TimesheetId);
            //assert
            AssertTimesheetsMatch(TIMESHEET_3Updated, expected);
        }

        [TestMethod]
        public void DeletedTimesheetCantBeRetrieved()
        {
            //act
            dao.DeleteTimesheet(3);
            Timesheet expected = dao.GetTimesheet(3);
            //assert
            Assert.IsNull(expected);
        }

        [TestMethod]
        public void GetBillableHours_ReturnsCorrectTotal()
        {
            decimal expected = 0M;
            decimal hours = dao.GetBillableHours(2,2);

            Assert.AreEqual(expected, hours);

        }

        private void AssertTimesheetsMatch(Timesheet expected, Timesheet actual)
        {
            Assert.AreEqual(expected.TimesheetId, actual.TimesheetId);
            Assert.AreEqual(expected.EmployeeId, actual.EmployeeId);
            Assert.AreEqual(expected.ProjectId, actual.ProjectId);
            Assert.AreEqual(expected.DateWorked, actual.DateWorked);
            Assert.AreEqual(expected.HoursWorked, actual.HoursWorked);
            Assert.AreEqual(expected.IsBillable, actual.IsBillable);
            Assert.AreEqual(expected.Description, actual.Description);
        }
    }
}

﻿using System;

namespace EmployeeProjects.Models
{
    public class Timesheet
    {
        public int TimesheetId { get; set; }

        public int EmployeeId { get; set; }

        public int ProjectId { get; set; }

        public DateTime DateWorked { get; set; }

        public decimal HoursWorked { get; set; }

        public bool IsBillable { get; set; }

        public string Description { get; set; }

        public Timesheet() { }

        public Timesheet(int timesheetId, int employeeId, int projectId, DateTime dateWorked, decimal hoursWorked, bool isBillable, string description)
        {
            TimesheetId = timesheetId;
            EmployeeId = employeeId;
            ProjectId = projectId;
            DateWorked = dateWorked;
            HoursWorked = hoursWorked;
            IsBillable = isBillable;
            Description = description;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (this == obj)
            { 
                return true; 
            }
            if(GetType() != obj.GetType())
            {
                return false;
            }
            // compare the fields
            bool theyMatch = true;
            theyMatch = TimesheetId == ((Timesheet)obj).TimesheetId && theyMatch;
            theyMatch = EmployeeId == ((Timesheet)obj).EmployeeId && theyMatch;
            theyMatch = ProjectId == ((Timesheet)obj).ProjectId && theyMatch;
            theyMatch = DateWorked == ((Timesheet)obj).DateWorked && theyMatch;
            theyMatch = HoursWorked == ((Timesheet)obj).HoursWorked && theyMatch;
            theyMatch = IsBillable == ((Timesheet)obj).IsBillable && theyMatch;
            theyMatch = Description == ((Timesheet)obj).Description && theyMatch;

            return theyMatch;
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Transactions;
using USCitiesAndParks.Models;

namespace USCitiesAndParksTests
{
    public class DaoBaseTests
    {
        protected City testCity { get; set; } = new City();
        protected State testState { get; set; } = new State();
        protected string connectionString = @"Server=.\SQLEXPRESS;Database=UnitedStates;Trusted_Connection=True;";
        protected TransactionScope transaction;
        [TestInitialize]
        public void Initialize()
        {
            // begin our transaction
            transaction = new TransactionScope();

            // check for a specific city
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    // look for city id 1
                    SqlCommand cmd = new SqlCommand("select * from city where city_id=@cityID;", connection);
                    cmd.Parameters.AddWithValue("@cityID", 1);
                    SqlDataReader sqlDataReader = cmd.ExecuteReader();
                    if (sqlDataReader.Read())
                    {
                        // got the city
                        testCity.CityId = 1;
                        testCity.CityName = Convert.ToString(sqlDataReader["city_name"]);
                        testCity.StateAbbreviation = Convert.ToString(sqlDataReader["state_abbreviation"]);
                        testCity.Population = Convert.ToInt32(sqlDataReader["population"]);
                        testCity.Area = Convert.ToDecimal(sqlDataReader["area"]);

                    }
                    else
                    {
                        // it isn't there, so add it.
                        // first, stop reading from the database
                        sqlDataReader.Close();
                        GetTestState(connection);
                        cmd = new SqlCommand($"insert into city(city_name,state_abbreviation,population,area) output INSERTED.city_id VALUES('Gotham','{testState.StateAbbreviation}',42,8);", connection);
                        testCity.CityId = Convert.ToInt32(cmd.ExecuteScalar());
                        testCity.CityName = "Gotham";
                        testCity.StateAbbreviation = "PA";
                        testCity.Population = 42;
                        testCity.Area = 8;

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            // create it if it doesn't exist
            // store for future use

        }
        [TestCleanup]
        public void CleanUp()
        {
            transaction.Dispose();
        }
        private State GetTestState(SqlConnection conn)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand("select * from state where state_abbreviation=@stateABB", conn);
                sqlCommand.Parameters.AddWithValue("@stateABB", "PA");
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.Read())
                {
                    testState.StateAbbreviation = "PA";
                    testState.StateName = Convert.ToString(sqlDataReader["state_name"]);
                }
                else
                {
                    // add the state
                    sqlDataReader.Close();
                    sqlCommand = new SqlCommand("ALTER TABLE state ALTER COLUMN capital INT NULL;insert into state" +
                        "(state_abbreviation,state_name,population,area,capital,sales_tax,state_nickname,census_region)" +
                        " VALUES('PA','Pennsylvania',80,5,NULL,7,'XXXXXXX','West';", conn);
                    sqlCommand.ExecuteNonQuery();
                    testState.StateAbbreviation = "PA";
                    testState.StateName = "Pennsylvania";

                }
                sqlDataReader.Close();
            }
            catch (Exception)
            {

                throw;
            }
            return testState;
        }

    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Transactions;
using USCitiesAndParks.DAO;
using USCitiesAndParks.Models;

namespace USCitiesAndParksTests
{
    [TestClass]
    public class CitySQLDaoTests : DaoBaseTests
    {

        [TestMethod]
        public void GetCityByID()
        {
            // Arrange
            CitySqlDao cityDao = new CitySqlDao(connectionString);

            // Act
            City city = cityDao.GetCity(testCity.CityId);

            // Assert
            AssertCitiesMatch(testCity, city);
        }

        private void AssertCitiesMatch(City expected, City actual)
        {
            Assert.AreEqual(expected.CityId, actual.CityId);
            Assert.AreEqual(expected.CityName, actual.CityName);
            Assert.AreEqual(expected.StateAbbreviation, actual.StateAbbreviation);
            Assert.AreEqual(expected.Population, actual.Population);
            Assert.AreEqual(expected.Area, actual.Area);
        }
    }
}

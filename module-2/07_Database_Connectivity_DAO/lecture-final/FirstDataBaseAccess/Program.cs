﻿using System;
using System.Data.SqlClient;

namespace FirstDataBaseAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SqlConnection connection = new SqlConnection("Server=.\\SQLEXPRESS;Database=UnitedStates;Trusted_Connection=True;");
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = "select * from state;";
                sqlCommand.Connection = connection;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                // read the data
                while (sqlDataReader.Read())
                {
                    string stateAbb = Convert.ToString(sqlDataReader["state_abbreviation"]);
                    int stateArea = Convert.ToInt32(sqlDataReader["area"]);
                }
                connection.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using USCitiesAndParks.Models;

namespace USCitiesAndParks.DAO
{
    public class ParkSqlDao : IParkDao
    {
        private readonly string connectionString;

        public ParkSqlDao(string connString)
        {
            connectionString = connString;
        }

        public Park GetPark(int parkId)
        {
            Park parkOutput = null;
            try
            {
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = "select * from park where park_id = @parkID";
                sqlCommand.Parameters.AddWithValue("@parkID", parkId);
                sqlCommand.Connection = connection;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                parkOutput = new Park();
                while (reader.Read())
                {
                    parkOutput = CreateParkFromReader(reader);
                }
                connection.Close();
            }
            catch (Exception e)
            {
                throw e;
            }
            return parkOutput;
        }

        public IList<Park> GetParksByState(string stateAbbreviation)
        {
            List<Park> output = new List<Park>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("select * from park join  park_state on park.park_id = park_state.park_id where state_abbreviation = @stateAbbreviation;", connection);
                    sqlCommand.Parameters.AddWithValue("@stateAbbreviation", stateAbbreviation);
                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        Park tempPark = CreateParkFromReader(reader);
                        output.Add(tempPark);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return output;
        }

        public Park CreatePark(Park park)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("insert into park (park_name,date_established,area,has_camping) " +
                        "OUTPUT INSERTED.park_id " +
                        "VALUES(@parkName, @parkDate, @area, @has_camping);", connection);
                    sqlCommand.Parameters.AddWithValue("@parkName", park.ParkName);
                    sqlCommand.Parameters.AddWithValue("@parkDate", park.DateEstablished);
                    sqlCommand.Parameters.AddWithValue("@area", park.Area);
                    sqlCommand.Parameters.AddWithValue("@has_camping", park.HasCamping);

                    park.ParkId = Convert.ToInt32(sqlCommand.ExecuteScalar());
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return park;
        }

        public void UpdatePark(Park park)
        {
            throw new NotImplementedException();
        }

        public void DeletePark(int parkId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    // must delete records in park_state first
                    SqlCommand cmd = new SqlCommand("DELETE FROM park_state WHERE park_id = @park_id; DELETE FROM park WHERE park_id = @park_id;", conn);
                    cmd.Parameters.AddWithValue("@park_id", parkId);

                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception)
            {

                throw;
            }
        }
        public void  AddParkToState(int parkId, string state_abbreviation)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand sqlCommand = new SqlCommand("insert into park_state(park_id,state_abbreviation) VALUES (@parkID,@stateA)", conn);
                    sqlCommand.Parameters.AddWithValue("@parkID", parkId);
                    sqlCommand.Parameters.AddWithValue("@stateA", state_abbreviation);
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void RemoveParkFromState(int parkId, string state_abbreviation)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("DELETE FROM park_state WHERE park_id = @park_id AND state_abbreviation = @state_abbreviation;", conn);
                    cmd.Parameters.AddWithValue("@park_id", parkId);
                    cmd.Parameters.AddWithValue("@state_abbreviation", state_abbreviation);

                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private Park CreateParkFromReader(SqlDataReader reader)
        {
            Park parkOut = new Park();
            parkOut.ParkId = Convert.ToInt32(reader["park_id"]);
            parkOut.ParkName = Convert.ToString(reader["park_name"]);
            parkOut.DateEstablished = Convert.ToDateTime(reader["date_established"]);
            parkOut.Area = Convert.ToDecimal(reader["area"]);
            parkOut.HasCamping = Convert.ToBoolean(reader["has_camping"]);
            return parkOut;
        }

        public int CountParksForState(string state_abbreviation)
        {
            int numberParks = 0;
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("select state_abbreviation,count(*) as parkCount from park_state where state_abbreviation=@stateA group by state_abbreviation", sqlConnection);
                    sqlCommand.Parameters.AddWithValue("@stateA", state_abbreviation);
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        numberParks = Convert.ToInt32(sqlDataReader["parkCount"]);
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            return numberParks;
        }
    }
}

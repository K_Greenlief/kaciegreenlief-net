﻿using EmployeeProjects.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeProjects.DAO
{
    public class EmployeeSqlDao : IEmployeeDao
    {
        private readonly string connectionString;

        public EmployeeSqlDao(string connString)
        {
            connectionString = connString;
        }

        public IList<Employee> GetAllEmployees()
        {
            List<Employee> employeeList = new List<Employee>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("select * from employee;", connection);
                    SqlDataReader reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        Employee output = new Employee();
                        output.EmployeeId = Convert.ToInt32(reader["employee_id"]);
                        output.DepartmentId = Convert.ToInt32(reader["department_id"]);
                        output.FirstName = Convert.ToString(reader["first_name"]);
                        output.LastName = Convert.ToString(reader["last_name"]);
                        output.BirthDate = Convert.ToDateTime(reader["birth_date"]);
                        output.HireDate = Convert.ToDateTime(reader["hire_date"]);
                        employeeList.Add(output);
                    }
                }
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
            return employeeList;
        }

        public IList<Employee> SearchEmployeesByName(string firstNameSearch, string lastNameSearch)
        {
            List<Employee> employeeList2 = new List<Employee>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("select * from employee where last_name like @last_name and first_name like @first_name", connection);

                    sqlCommand.Parameters.AddWithValue("@last_name", "%"+lastNameSearch+"%");
                    sqlCommand.Parameters.AddWithValue("@first_name", "%"+firstNameSearch+"%");

                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        Employee output = new Employee();
                        output.EmployeeId = Convert.ToInt32(reader["employee_id"]);
                        output.DepartmentId = Convert.ToInt32(reader["department_id"]);
                        output.FirstName = Convert.ToString(reader["first_name"]);
                        output.LastName = Convert.ToString(reader["last_name"]);
                        output.BirthDate = Convert.ToDateTime(reader["birth_date"]);
                        output.HireDate = Convert.ToDateTime(reader["hire_date"]);
                        employeeList2.Add(output);
                    }
                }
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
            return employeeList2;
        }

        public IList<Employee> GetEmployeesByProjectId(int projectId)
        {
            List<Employee> employeeList3 = new List<Employee>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("select * from employee " +
                        "join project_employee on employee.employee_id = project_employee.employee_id " +
                        "join project on project.project_id = project_employee.project_id " +
                        "where project.project_id = @project_id", connection);

                    sqlCommand.Parameters.AddWithValue("@project_id",projectId);


                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        Employee output = new Employee();
                        output.EmployeeId = Convert.ToInt32(reader["employee_id"]);
                        output.DepartmentId = Convert.ToInt32(reader["department_id"]);
                        output.FirstName = Convert.ToString(reader["first_name"]);
                        output.LastName = Convert.ToString(reader["last_name"]);
                        output.BirthDate = Convert.ToDateTime(reader["birth_date"]);
                        output.HireDate = Convert.ToDateTime(reader["hire_date"]);
                        employeeList3.Add(output);
                    }
                }
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
            return employeeList3;
        }

        public void AddEmployeeToProject(int projectId, int employeeId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("INSERT into project_employee(project_id, employee_id)" +
                        "VALUES(@project_id,@employee_id);", connection);

                    sqlCommand.Parameters.AddWithValue("@project_id", projectId);
                    sqlCommand.Parameters.AddWithValue("@employee_id", employeeId);

                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
        }

        public void RemoveEmployeeFromProject(int projectId, int employeeId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("DELETE from project_employee " +
                        "where project_id = @project_id and employee_id = @employee_id;", connection);

                    sqlCommand.Parameters.AddWithValue("@project_id", projectId);
                    sqlCommand.Parameters.AddWithValue("@employee_id", employeeId);

                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
        }

        public IList<Employee> GetEmployeesWithoutProjects()
        {
            List<Employee> employeeList4 = new List<Employee>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("select * from employee " +
                        "left join project_employee on employee.employee_id = project_employee.employee_id " +
                        "where project_id is null", connection);

                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        Employee output = new Employee();
                        output.EmployeeId = Convert.ToInt32(reader["employee_id"]);
                        output.DepartmentId = Convert.ToInt32(reader["department_id"]);
                        output.FirstName = Convert.ToString(reader["first_name"]);
                        output.LastName = Convert.ToString(reader["last_name"]);
                        output.BirthDate = Convert.ToDateTime(reader["birth_date"]);
                        output.HireDate = Convert.ToDateTime(reader["hire_date"]);
                        employeeList4.Add(output);
                    }
                }
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
            return employeeList4;
        }


    }
}

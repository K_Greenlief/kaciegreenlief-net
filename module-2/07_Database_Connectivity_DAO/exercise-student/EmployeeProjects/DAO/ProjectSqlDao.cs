﻿using EmployeeProjects.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeProjects.DAO
{
    public class ProjectSqlDao : IProjectDao
    {
        private readonly string connectionString;

        public ProjectSqlDao(string connString)
        {
            connectionString = connString;
        }

        public Project GetProject(int projectId)
        {

            Project output = null;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandText = "select * from project where project_id = @projectID;";
                    sqlCommand.Parameters.AddWithValue("@projectID", projectId);
                    sqlCommand.Connection = connection;
                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    output = new Project();
                    if (reader.Read())
                    {
                        output.ProjectId = Convert.ToInt32(reader["project_id"]);
                        output.Name = Convert.ToString(reader["name"]);
                        output.FromDate = Convert.ToDateTime(reader["from_date"]);
                        output.ToDate = Convert.ToDateTime(reader["to_date"]);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return output;



            //return new Project(0, "Not Implemented Yet", DateTime.Now, DateTime.Now.AddDays(1));
        }

        public IList<Project> GetAllProjects()
        {
            List<Project> projectList = new List<Project>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("select * from project;", connection);
                    SqlDataReader reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        Project output = new Project();
                        output.ProjectId = Convert.ToInt32(reader["project_id"]);
                        output.Name = Convert.ToString(reader["name"]);
                        output.FromDate = Convert.ToDateTime(reader["from_date"]);
                        output.ToDate = Convert.ToDateTime(reader["to_date"]);
                        projectList.Add(output);
                    }
                }
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
            return projectList;
        }

        public Project CreateProject(Project newProject)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand("insert into project(name, from_date, to_date)" +
                        "OUTPUT INSERTED.project_id " +
                        "VALUES(@name, @fromDate, @toDate);", connection);
                sqlCommand.Parameters.AddWithValue("@name", newProject.Name);
                sqlCommand.Parameters.AddWithValue("@fromDate", newProject.FromDate);
                sqlCommand.Parameters.AddWithValue("@toDate", newProject.ToDate);

                newProject.ProjectId = Convert.ToInt32(sqlCommand.ExecuteScalar());
            }
            return newProject;
        }


        public void DeleteProject(int projectId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("DELETE from project_employee WHERE project_id = @projectId;" +
                        "DELETE from project WHERE project_id = @projectId;", connection);

                    sqlCommand.Parameters.AddWithValue("@projectId", projectId);

                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
        }
    }

}

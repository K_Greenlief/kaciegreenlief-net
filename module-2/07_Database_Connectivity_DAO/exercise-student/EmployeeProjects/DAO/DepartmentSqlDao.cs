﻿using EmployeeProjects.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeProjects.DAO
{
    public class DepartmentSqlDao : IDepartmentDao
    {
        private readonly string connectionString;

        public DepartmentSqlDao(string connString)
        {
            connectionString = connString;
        }

        public Department GetDepartment(int departmentId)
        {
            Department output = null;
            try
            {
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = "select * from department where department_id = @departmentID;";
                sqlCommand.Parameters.AddWithValue("@departmentID", departmentId);
                sqlCommand.Connection = connection;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                output = new Department();
                if (reader.Read())
                {
                    output.DepartmentId = Convert.ToInt32(reader["department_id"]);
                    output.Name = Convert.ToString(reader["name"]);
                }
                else 
                {
                    return null;
                }
                connection.Close();
            }
            catch (Exception e)
            {
               throw;
            }
            return output;
        }

        public IList<Department> GetAllDepartments()
        {
            List<Department> departmentList = new List<Department>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("select * from department;",connection);
                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    
                    while (reader.Read())
                    {
                        Department output = new Department();
                        output.DepartmentId = Convert.ToInt32(reader["department_id"]);
                        output.Name = Convert.ToString(reader["name"]);
                        departmentList.Add(output);
                    }
                }
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
            return departmentList;

        }

        public void UpdateDepartment(Department updatedDepartment)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCommand = new SqlCommand("UPDATE department SET name = @name WHERE department_id = @department_id;", connection);

                    sqlCommand.Parameters.AddWithValue("@name", updatedDepartment.Name);
                    sqlCommand.Parameters.AddWithValue("@department_id", updatedDepartment.DepartmentId);

                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using USCitiesAndParks.Models;

namespace USCitiesAndParks.DAO
{
    public class ParkSqlDao : IParkDao
    {
        private readonly string connectionString;

        public ParkSqlDao(string connString)
        {
            connectionString = connString;
        }

        public Park GetPark(int parkId)
        {
            Park output = null;
            try
            {
                SqlConnection connect = new SqlConnection(connectionString);
                connect.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = "select from park where park_id = " + parkId;
                sqlCommand.Connection = connect;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    output.ParkId = Convert.ToInt32(reader["parkId"]);
                    output.ParkName = Convert.ToString(reader["park_name"]);
                }
            }
            catch (Exception e) 
            {
                throw e;
            }
            return output;
        }

        public IList<Park> GetParksByState(string stateAbbreviation)
        {
            throw new NotImplementedException();
        }

        public Park CreatePark(Park park)
        {
            throw new NotImplementedException();
        }

        public void UpdatePark(Park park)
        {
            throw new NotImplementedException();
        }

        public void DeletePark(int parkId)
        {
            throw new NotImplementedException();
        }

        public void AddParkToState(int parkId, string state_abbreviation)
        {
            throw new NotImplementedException();
        }

        public void RemoveParkFromState(int parkId, string state_abbreviation)
        {
            throw new NotImplementedException();
        }

        private Park CreateParkFromReader(SqlDataReader reader)
        {
            throw new NotImplementedException();
        }
    }
}

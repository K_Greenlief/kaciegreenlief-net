-- 18. The count of the number of cities (name column 'num_cities') and 
--the state abbreviation for each state and territory 
-- (55 rows) 
select count(city_name) as 'num_cities', state_abbreviation from city where state_abbreviation != 'dc'
group by state_abbreviation
order by state_abbreviation
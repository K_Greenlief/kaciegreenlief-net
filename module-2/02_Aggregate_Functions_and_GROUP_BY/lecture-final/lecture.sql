-- ORDERING RESULTS

-- Populations of all states from largest to smallest.
select state_name,population from state
order by population desc
-- States sorted alphabetically (A-Z) within their census region. The census regions are sorted in reverse alphabetical (Z-A) order.
select state_name,census_region
from state
order by census_region desc,state_name
-- The biggest park by area
-- just because I order by, doesn't mean I have to select it
select park_name from park order by area desc
select park_name,area from park order by area desc

-- LIMITING RESULTS

-- The 10 largest cities by populations
select top 10 population,city_name from city
order by population desc
-- The 20 oldest parks from oldest to youngest in years, sorted alphabetically by name.
select top 20 park_name,YEAR(GETDATE()) - YEAR(date_established) as age from park order by age desc,park_name

-- CONCATENATING OUTPUTS

-- All city names and their state abbreviation.
select city_name + ' (' + state_abbreviation + ')' from city

-- All park names and area
select ('Name: ' + park_name + ' Area: ' + CAST(area as VARCHAR)) as park_area from park 
order by park_name

-- The census region and state name of all states in the West & Midwest sorted in ascending order.
select census_region,state_name from state
where census_region='West' or census_region='Midwest'
order by state_name

select census_region,state_name from state
where census_region like '%west'
order by state_name

select census_region,state_name from state
where census_region in ('West','MidWest')
order by state_name

-- AGGREGATE FUNCTIONS

-- Average population across all the states. Note the use of alias, common with aggregated values.
select avg(population) as average_population from state

-- Total population in the West and South census regions
select sum(population) as Total_West_And_South from state
where census_region = 'West' or census_region = 'South'
-- The number of cities with populations greater than 1 million
select count(city_name) from city
where population > 1000000
-- The number of state nicknames.
select count(state_nickname) 'States with cool official nicknames',count(*) as row_count from state

-- The area of the smallest and largest parks.
select min(area) as smallest,max(area) as largest from park


-- GROUP BY

-- Count the number of cities in each state, ordered from most cities to least.
select count(city_name) as cities,state_abbreviation from city
group by state_abbreviation
-- Determine the average park area depending upon whether parks allow camping or not.
select avg(area),count(*),has_camping from park
group by has_camping
-- Sum of the population of cities in each state ordered by state abbreviation.
select sum(population) as population,state_abbreviation from city
group by state_abbreviation
order by state_abbreviation
-- The smallest city population in each state ordered by city population.
select min(population) as min_population,state_abbreviation from city
group by state_abbreviation
order by min_population
-- SUBQUERIES (optional)

-- Include state name rather than the state abbreviation while counting the number of cities in each state,
select count(city_name) as cities,(select state_name from state where state.state_abbreviation = c.state_abbreviation) from city as c
group by state_abbreviation
-- Include the names of the smallest and largest parks
select park_name,area
from park as p,
(select min(area) as smallest,max(area) as largest from park) as sl
where p.area = sl.smallest or p.area = sl.largest
-- List the capital cities for the states in the Northeast census region.
select state_abbreviation, city_name from city
where city_id in 
(select capital from state where census_region = 'Northeast')

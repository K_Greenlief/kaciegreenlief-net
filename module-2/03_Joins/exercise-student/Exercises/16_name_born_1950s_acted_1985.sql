-- 16. The names and birthdays of actors born in the 1950s who acted in movies that were released in 1985 (20 rows)

select distinct person_name, birthday from person
join movie_actor on person.person_id = movie_actor.actor_id
join movie on movie_actor.movie_id = movie.movie_id
where (YEAR(birthday) between 1950 and 1959) and YEAR(movie.release_date) = 1985

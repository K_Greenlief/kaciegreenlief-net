-- 20. The titles, lengths, and release dates of the 5 longest movies in the "Action" genre.
--Order the movies by length (highest first), then by release date (latest first).
-- (5 rows, expected lengths around 180 - 200)

select top 5 length_minutes, release_date, title from movie


join movie_genre on movie.movie_id = movie_genre.movie_id
join genre on movie_genre.genre_id = genre.genre_id

where genre_name = 'Action'
order by length_minutes desc,release_date desc

-- 15. The title of the movie and the name of director for movies 
-- where the director was also an actor in the same movie (73 rows)

select distinct title, person_name from movie

join person on movie.director_id = person.person_id
join movie_actor on person_id = movie_actor.actor_id

where director_id = actor_id
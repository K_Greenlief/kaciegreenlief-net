﻿using System;

namespace FindAndReplace
{
    public class Program
    {
		public static void Main(string[] args)
		{
            FindAndReplaceWriter farw = new FindAndReplaceWriter();
            farw.Run();
        }
    }
}

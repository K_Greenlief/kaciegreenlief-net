﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace FindAndReplace
{
    public class FindAndReplaceWriter
    {
        public void Run()
        {
            try
            {
                Console.WriteLine("Hello, please type a ord to search for :");
                string originalWord = Console.ReadLine();
                Console.WriteLine($"Okay, now let's pick a word to swap {originalWord} with :");
                string newWord = Console.ReadLine();
                Console.WriteLine("Cool, now give me a file to search! : ");
                string sourceFile = Console.ReadLine();
                Console.WriteLine("And finally, tell me in what file you'd like to hold the finished product! :");
                string destinationFile = Console.ReadLine();

                // grab original word from each line of sourceFile, replace with newWord using writer, dump into destinationFile


                using (StreamReader osr = new StreamReader(sourceFile)) //open old file
                {

                    using (StreamWriter nsr = new StreamWriter(destinationFile)) //open new file so we can write stuff to it
                    {
                        while (!osr.EndOfStream)
                        {
                            string line = osr.ReadLine();
                            string newLine = line.Replace(originalWord, newWord);
                            nsr.WriteLine(newLine);
                        }

                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}

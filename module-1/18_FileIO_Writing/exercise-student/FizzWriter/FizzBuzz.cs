﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace FizzWriter
{
    public class FizzBuzz
    {
        public void Run()
        {
            try
            {
                Console.WriteLine("Hello, please enter the destination file :");
                string destinationFile = Console.ReadLine();


                using (StreamWriter sw = new StreamWriter(destinationFile))
                {
                    for (int i = 1; i <= 300; i++)
                    {
                        if (i % 3 == 0 && i % 5 == 0)
                        {
                            sw.WriteLine("FizzBuzz");

                        }
                        else if (i.ToString().Contains("5") || i % 5 == 0)
                        {
                            sw.WriteLine("Buzz");

                        }
                        else if (i.ToString().Contains("3") || i % 3 == 0)
                        {
                            sw.WriteLine("Fizz");

                        }
                        else
                        {
                            sw.WriteLine(i);

                        }

                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine("Error, try again");
                Console.WriteLine(e.Message) ;
            }
        }
    }
}

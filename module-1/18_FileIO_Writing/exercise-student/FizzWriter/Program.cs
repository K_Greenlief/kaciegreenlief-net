﻿using System;

namespace FizzWriter
{
    public class Program
    {
        public static void Main(string[] args)
        {
            FizzBuzz fizzBuzz = new FizzBuzz();
            fizzBuzz.Run();
        }
    }
}

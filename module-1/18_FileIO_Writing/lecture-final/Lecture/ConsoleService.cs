﻿using Lecture.Aids;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture
{
    public class ConsoleService
    {
        public void Run()
        {
            WritingTextFiles wtf = new WritingTextFiles();
            LoopingCollectionToWriteFile lctwf = new LoopingCollectionToWriteFile();
            ReadingAndWritingFiles rawf = new ReadingAndWritingFiles();
            PerformanceDemo pd = new PerformanceDemo();
            try
            {
                wtf.WritingAFile();
                wtf.MoreWritingAFile();
                lctwf.LoopingADictionaryToWriteAFile();
                rawf.OpenAndWrite();
                pd.SlowPerformance();
                pd.FastPerformance();
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}

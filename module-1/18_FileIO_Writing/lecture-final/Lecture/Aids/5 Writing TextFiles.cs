﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture.Aids
{
    public class WritingTextFiles
    {
        /*
        * This method below provides sample code for printing out a message to a text file.
        */
        public void WritingAFile()
        {
            // oops forgot try catch
            DirectoryInfo[] cDirs = new DirectoryInfo("C:\\").GetDirectories();
            using (StreamWriter sw = new StreamWriter("CDriveDirs.txt"))
            {
                foreach (DirectoryInfo dir in cDirs)
                {
                    sw.WriteLine(dir.Name);
                }

            }
            // no using block
            StreamWriter sw2 = new StreamWriter("CDriveDirs2.txt");

            foreach (DirectoryInfo dir in cDirs)
            {
                sw2.WriteLine(dir.Name);
            }
            // don't forget to clean up
            sw2.Flush();
            sw2.Close();


        }

        public void MoreWritingAFile()
        {
            string directory = Directory.GetCurrentDirectory();
            string fileName = "output.txt";
            string fullPath = Path.Combine(directory, fileName);

            try
            {
                // create the stream writing
                using(StreamWriter sw = new StreamWriter(fullPath))
                {
                    // write the date and time
                    sw.WriteLine(DateTime.UtcNow);

                    // write the local time
                    sw.WriteLine(DateTime.Now);

                    // Write hello world
                    sw.Write("Hello ");

                    // Write the other half
                    sw.WriteLine("World!!");

                    // Write
                    sw.WriteLine("Thaat's alll fooolks!");
                }
            } catch(Exception ex)
            {
                throw ex;
            }
        }


    }
}

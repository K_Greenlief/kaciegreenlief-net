﻿using System;
using Lecture.Aids;

namespace Lecture
{
    class Program
    {
        static void Main(string[] args)
        {

            ConsoleService consoleService = new ConsoleService();
            consoleService.Run();
        }
    }
}

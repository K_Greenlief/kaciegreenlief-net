﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileSplitter.Classes
{
    public class UserInterface
    {
        private int maxLineCount { get; set; }
        private FileFunctions workerClass { get; set; }
        public void Start()
        {
            bool gotFile = false;
            do
            {
                try
                {
                    string fileToUse = AskForFileMenu();
                    workerClass = new FileFunctions(fileToUse);
                    gotFile = true;
                }
                catch (Exception error)
                {
                    Console.Clear();
                    Console.WriteLine("Sorry, seems like I'm having difficulty, please try again.");
                    Console.WriteLine(error.Message);
                }

            } while (!gotFile);

            do
            {
                Console.Write("How many lines of text (max) should there be in the split files? ");
                string userInputMaxLineCount = Console.ReadLine();
                try
                {
                    maxLineCount = int.Parse(userInputMaxLineCount);
                }
                catch
                {
                    maxLineCount = 0;
                }

            } while (maxLineCount <= 0);
            Console.WriteLine($"\nFor a {workerClass.LinesInFile()} line input file, this produces {NumberOfOutPutFiles()} output files.\n");

            Console.WriteLine("**GENERATING OUTPUT**\n");
            List<string> fileNames = workerClass.CreateFiles(maxLineCount);
            foreach (string item in fileNames)
            {
                Console.WriteLine(item);
            }
        }

        public string AskForFileMenu()
        {
            Console.WriteLine("Please enter the path to your input file: ");
            string pathToFile = Console.ReadLine();
            Console.Write("Please enter the name of your input file: ");
            string fileName = Console.ReadLine();
            return Path.Combine(pathToFile, fileName);
        }

        public int NumberOfOutPutFiles()
        {
            return (int)Math.Ceiling((double)workerClass.LinesInFile() / maxLineCount);
        }
    }
}

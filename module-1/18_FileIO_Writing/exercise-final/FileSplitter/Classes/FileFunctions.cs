﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileSplitter.Classes
{
    public class FileFunctions
    {
        private string OriginalFile { get; }

        public FileFunctions(string fileName)
        {
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("Hey, sorry, can't find the file");
            }
            OriginalFile = fileName;
        }

        public int LinesInFile()
        {
            int numberOfLines = 0;
            try
            {
                using(StreamReader sr = new StreamReader(OriginalFile))
                {
                    while(!sr.EndOfStream)
                    {
                        sr.ReadLine();
                        numberOfLines++;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return numberOfLines;
        }
        public List<string> CreateFiles(int numberOfLines)
        {
            int fileSuffix = 1;
            List<String> result = new List<string>();
            try
            {
                using (StreamReader sr = new StreamReader(OriginalFile))
                {
                    while (!sr.EndOfStream)
                    {
                        using (StreamWriter sw = new StreamWriter("input-" + fileSuffix + ".txt"))
                        {
                            for (int i = 0; i < numberOfLines && !sr.EndOfStream; i++)
                            {
                                sw.WriteLine(sr.ReadLine());
                            }
                            result.Add("Generating input-" + fileSuffix + ".txt");
                        }
                        fileSuffix++;
                    }
                }

            }
            catch (Exception e)
            {
                return new List<string>();
            }
            return result;
        }
    }
}

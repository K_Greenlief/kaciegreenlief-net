﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TestableClasses.Classes;

namespace TestableClassesTests.Classes
{
    [TestClass]
    public class StringExercisesTests
    {
        StringExercises stringExercises = new StringExercises();

        [DataTestMethod]
        [DataRow("Hi","Bye","HiByeByeHi")]
        [DataRow("Yo","Alice","YoAliceAliceYo")]
        [DataRow("Hey","Friday","HeyFridayFridayHey")]
        [DataRow("Its","A3DayWeekend","ItsA3DayWeekendA3DayWeekendIts")]
        public void MakeAbbaHappyPathTest(string input1, string input2, string expected)
        {
            // makeAbba("Hi", "Bye") → "HiByeByeHi"

            // Arrange -- my ingredients
            stringExercises = new StringExercises();

            // Act
            string actual = stringExercises.MakeAbba(input1, input2);

            // Assert
            Assert.AreEqual(expected, actual, "Hmmm, looks like we aren't doing abba");

        }

        [TestMethod]
        public void MakeAbbaEmptyStringTest()
        {
            // Arrange
            stringExercises = new StringExercises();
            string thing1 = "";
            string thing2 = "";
            string actual = "";

            // Act
            actual = stringExercises.MakeAbba(thing1, thing2);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(actual));
        }

        [TestMethod]
        public void MakeAbbaNullTest()
        {
            // Arrange
            stringExercises = new StringExercises();
            string actual = "";

            // Act
            actual = stringExercises.MakeAbba(null, null);
            // Assert
            Assert.AreEqual("", actual);
            // Act
            actual = stringExercises.MakeAbba("Hi", null);
            // Assert
            Assert.AreEqual("", actual);
        }

        [TestMethod]
        public void FirstTwoHappyPath()
        {
            // Arrange
            stringExercises = new StringExercises();
            string input = "Hello";
            string expected = "He";
            string actual = "";
            // Act
            actual = stringExercises.FirstTwo(input);
            // Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void FirstTwoNull()
        {
            // Arrange
            stringExercises = new StringExercises();
            string input = null;
            string expected = "";
            string actual = "";
            // Act
            actual = stringExercises.FirstTwo(input);
            // Assert
            Assert.IsNull(actual);
        }
    }
}
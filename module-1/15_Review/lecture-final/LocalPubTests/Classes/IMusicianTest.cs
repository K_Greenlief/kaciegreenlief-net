﻿using LocalPub.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace LocalPubTests.Classes
{
    [TestClass]
    public class IMusicianTest
    {
        [TestMethod]
        public void PlayYourInstrumentHappyPath()
        {
            // Arrange
            Artist art = new Artist("Madonna");
            string expected = "Strum guitar";
            // Act
            string actual = art.Play();
            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}

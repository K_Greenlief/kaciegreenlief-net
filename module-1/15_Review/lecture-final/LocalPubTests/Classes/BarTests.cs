﻿using LocalPub;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace LocalPubTests.Classes
{
    [TestClass]
    public class BarTests
    {
        Bar RosPub = new Bar("Ro's Garden", 6);

        [TestMethod]
        public void AddAGroupUnderCapacity()
        {
            // Arrange
            List<Person> testGroup = MakeAListOfPeople(RosPub.Capacity - 1);
            // Act
            int peopleTotal = RosPub.AddAGroup(testGroup);
            // Assert
            Assert.AreEqual(testGroup.Count, peopleTotal);
        }
        [TestMethod]
        public void OpenDoorsUnderCapacity()
        {
            Person patron = new Person("Fred");
            int peopleTotal = RosPub.OpenTheDoors(patron);
            Assert.AreEqual(1, peopleTotal);
        }
        [TestMethod]
        public void AddAGroupOverCapacity()
        {
            // Arrange
            List<Person> testGroup =MakeAListOfPeople( RosPub.Capacity + 1);
            // Act
            int peopleTotal = RosPub.AddAGroup(testGroup);
            // Assert
            Assert.AreEqual(0, peopleTotal);
        }
        [TestMethod]
        public void AddAGroupAtCapacity()
        {
            // Arrange
            List<Person> testGroup = MakeAListOfPeople(RosPub.Capacity);
            // Act
            int peopleTotal = RosPub.AddAGroup(testGroup);
            // Assert
            Assert.AreEqual(0, peopleTotal);
        }

        // Helper method
        private List<Person> MakeAListOfPeople(int howMany)
        {
            List<Person> output = new List<Person>();
            for (int i = 0; i <= howMany; i++)
            {
                Person person = new Person(i.ToString());
                output.Add(person);
            }
            return output;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalPub.Classes
{
    public class Artist : Person, IMusician
    {
        public Artist(string FirstName) : base(FirstName)
        {
        }

        public string Play()
        {
            return "Strum guitar";
        }
    }
}

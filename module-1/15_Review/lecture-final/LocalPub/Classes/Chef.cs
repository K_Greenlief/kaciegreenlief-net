﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalPub
{
    public class Chef : Person
    {
        public string Cuisine { get; set; }
        public List<Recipe> Recipes { get; set; }
        public Restaurant PlaceOfEmployment { get; set; }
        public Chef(string firstName) : base(firstName) { }
    }
}

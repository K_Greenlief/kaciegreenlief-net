﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalPub.Classes
{
    public interface IMusician
    {
        string Play();
    }
}

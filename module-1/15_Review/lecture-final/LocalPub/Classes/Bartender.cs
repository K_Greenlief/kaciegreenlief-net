﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalPub.Classes
{
    public class Bartender : Person, IMusician
    {
        public Bartender(string FirstName) : base(FirstName)
        {
        }
        public List<int> WorkDays { get; set; }

        public string Play()
        {
            return "Bang glasses";
        }
    }
}

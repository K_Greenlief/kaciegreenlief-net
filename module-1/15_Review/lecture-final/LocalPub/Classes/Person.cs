﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalPub
{
    public class Person
    {
        // can only be changed by methods in this class because private set
        public string FirstName { get; private set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public string FormalName
        {
            get
            {
                return LastName + ", " + FirstName;
            }
        }
        public int Age
        {
            get
            {
                return DateTime.UtcNow.Year - DateOfBirth.Year;
            }
        }
        public Person(string FirstName)
        {
            this.FirstName = FirstName;
        }
        /// <summary>
        /// Changes the First Name and optionally the Last Name of the person
        /// </summary>
        /// <param name="FirstName">First Name of Person</param>
        /// <param name="LastName">Last Name of Person</param>
        /// <returns></returns>
        public string ChangeName(string FirstName,string LastName = "")
        {
            this.FirstName = FirstName;
            if(LastName != "")
            {
                this.LastName = LastName;
            }
            return FullName;
        }
    }
}

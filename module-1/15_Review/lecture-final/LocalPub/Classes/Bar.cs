﻿using LocalPub.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace LocalPub
{
    public class Bar
    {
        public string Name { get; }
        public int Capacity { get; private set; }
        List<Person> Patrons { get; set; } = new List<Person>();
        List<IMusician> TheBand { get; set; } = new List<IMusician>();
        public Bartender Sam { get; set; } 
        public Bar(string name, int capacity)
        {
            Name = name;
            Capacity = capacity;
        }
        public bool BartenderStartShift(Bartender bartender)
        {
            Sam = bartender;
            return true;
        }
        public int OpenTheDoors(Person person)
        {
            if(Patrons.Count + 1 <= Capacity)
            {
                Patrons.Add(person);
            }
            return Patrons.Count;
        }
        /// <summary>
        /// Add the people to the Patrons if there is enough room
        /// </summary>
        /// <param name="people"></param>
        /// <returns></returns>
        public int AddAGroup(List<Person> people)
        {
            if(Patrons.Count + people.Count <= Capacity)
            {
                Patrons.AddRange(people);
            }
            return Patrons.Count;
        }
        public bool LetTheBandIn(List<IMusician> band)
        {
            TheBand = band;
            return true;
        }
        public bool LetTheBandIn(IMusician bucky)
        {
            TheBand.Add(bucky);
            return true;
        }
     }
}

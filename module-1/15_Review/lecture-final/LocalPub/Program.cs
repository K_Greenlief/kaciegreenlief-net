﻿using LocalPub.Classes;
using System;
using System.Collections.Generic;

namespace LocalPub
{
    class Program
    {
        static void Main(string[] args)
        {
            Chef emeril = new Chef("Emeril");
            emeril.LastName = "Lagasse";
            emeril.ChangeName("Giada");

            Bar MedvitzKaraoke = new Bar("Medvitz's Live Karaoke Bar", 50);
            Bartender Isaac = new Bartender("Isaac");
            Bartender Andrew = new Bartender("Andrew");
            MedvitzKaraoke.LetTheBandIn(Andrew);
            List<IMusician> artists = new List<IMusician>();
            artists.Add(new Artist("Cher"));
            artists.Add(new Artist("Beyonce"));
            MedvitzKaraoke.LetTheBandIn(artists);
        }
    }
}

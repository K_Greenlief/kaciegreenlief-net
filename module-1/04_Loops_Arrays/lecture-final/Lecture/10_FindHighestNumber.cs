﻿namespace Lecture
{
    public partial class LectureProblem
    {
        /*
        10. What code do we need to write so that we can find the highest
             number in the array randomNumbers?
             TOPIC: Looping Through Arrays
        */
        public int FindTheHighestNumber(int[] randomNumbers)
        {
            // Variable to hold the highest number
            int max = randomNumbers[0];
            // Loop through all the number
            for (int i = 1; i < randomNumbers.Length; i++)
            {
                //Compare the current number to the highest number
                if(randomNumbers[i] > max)
                {
                    // if it is
                    // Current number put into highest number
                    max = randomNumbers[i];
                }
                // if it isn't
                // move on

            }

            // the highest number
            return max;
        }
    }
}

﻿namespace Exercises
{
    public partial class Exercises
    {
        /* 
          Given an array of ints length 3, return an array with the elements "rotated left" so

          RotateLeft3([1, 2, 3]) → [2, 3, 1] ////////

          RotateLeft3([5, 11, 9]) → [11, 9, 5]
          RotateLeft3([7, 0, 0]) → [0, 0, 7]

            delete 0 and add to length-1
            or- reassign 0 to 3 and 3 to 0
          */
           
        public int[] RotateLeft3(int[]arr)
        {
            int[] newArray = new int[3];

            newArray[0] = arr[1];
            newArray[1] = arr[2];
            newArray[2] = arr[0];

            return newArray;
        }
       
    }
}
         
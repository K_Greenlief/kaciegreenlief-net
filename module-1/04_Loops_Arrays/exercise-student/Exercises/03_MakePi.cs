﻿namespace Exercises
{
    public partial class Exercises
    {
        /*
         Return an int array length 3 containing the first 3 digits of pi, {3, 1, 4}.
         MakePi() → [3, 1, 4]
         */
      
        public int[] MakePi()
        {
            int[] piArr = new int[3];
            piArr[0] = 3;
            piArr[1] = 1;
            piArr[2] = 4;
            return piArr;
        }
    }
}

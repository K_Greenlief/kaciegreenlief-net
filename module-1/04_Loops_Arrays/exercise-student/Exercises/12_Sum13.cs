﻿namespace Exercises
{
    public partial class Exercises
    {
        /*
         Return the sum of the numbers in the array, 

        returning 0 for an empty array. 

         13 does not count and numbers that come immediately after a 13 also do
         not count.
 
         */
        public int Sum13(int[] nums)
        {
            int sum = 0;
            bool skip = false;

            for (int i = 0; i < nums.Length; i++)
            {
                if (!skip)
                {
                    if (nums[i] == 13)
                    {
                        skip = true;
                    }
                    else
                    {
                        sum += nums[i];
                    }
                }
                else
                {
                    if (nums[i] != 13)
                    {
                        skip = false;
                    }
                }

                //if (nums[i] == 13)
                //{
                //    i += 1;
                //}
                //else sum += nums[i];

            }

            return sum;
        }
    }
}

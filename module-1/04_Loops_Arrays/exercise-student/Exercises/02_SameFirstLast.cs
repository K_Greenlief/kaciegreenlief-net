﻿namespace Exercises
{
    public partial class Exercises
    {
        /*
         Given an array of ints, return true if the array is length 1 or more, and the first element and
         the last element are equal.
         SameFirstLast([1, 2, 3]) → false
         SameFirstLast([1, 2, 3, 1]) → true
         SameFirstLast([1, 2, 1]) → true
         */

        //check if arr.length >= 1 , and if (arr[0] == arr[arr.Length - 1])

        public bool SameFirstLast(int[] arr)
        {
            if (arr.Length >= 1 && (arr[0] == arr[arr.Length - 1]))
            {
                return true;
            }
            return false;
        }
    }
}

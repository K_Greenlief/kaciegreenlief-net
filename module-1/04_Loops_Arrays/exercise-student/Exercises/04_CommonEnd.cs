﻿namespace Exercises
{
    public partial class Exercises
    {
        /*
         Given 2 arrays of ints, a and b, return true if they have the same first element or they have
         the same last element. Both arrays will be length 1 or more.
         CommonEnd([1, 2, 3], [7, 3]) → true
         CommonEnd([1, 2, 3], [7, 3, 2]) → false
         CommonEnd([1, 2, 3], [1, 3]) → true
         */

        public bool CommonEnd(int[] arr1, int[] arr2)
        {
            if (arr1.Length > 1 || arr2.Length  > 1)
            {
                if (arr1[0] == arr2[0] || arr1[arr1.Length - 1] == arr2[arr2.Length - 1])
                {
                    return true;
                }
            }
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture.Farming
{
    public class FarmAnimal : ISingable
    {
        public string Name { get; set; }
        public string Sound { get; set; } 
        public string MakeSoundOnce()
        {
            return Sound;
        }
        public string MakeSoundTwice()
        {
            return Sound + " " + Sound;
        }

    }
}

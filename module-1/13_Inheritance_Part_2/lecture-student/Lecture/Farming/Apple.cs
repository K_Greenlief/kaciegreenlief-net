﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture.Farming
{
    public class Apple : ISellable
    {
        public string AppleType { get; }
        public decimal Price { get; private set; } = .5M;
        public Apple(string appleType)
        {
            AppleType = appleType;
        }
        public void SetPrice(decimal price)
        {
            if(price > .5M)
            {
                Price = price;
            } else
            {
                Price = .5M;
            }
        }

        public decimal GetSalesPrice()
        {
            return Price;
        }

        public string PriceListing()
        {
            return $"The {AppleType} costs {GetSalesPrice():C2}";
        }
    }
}

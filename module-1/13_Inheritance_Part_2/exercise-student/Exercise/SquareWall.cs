﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise
{
    public class SquareWall : RectangleWall
    {
        public int SideLength { get; }
        public SquareWall(string name, string color, int sideLength) : base(name, color, sideLength, sideLength)
        {
            SideLength = sideLength;
        }

        public override int GetArea()
        {
            int area = SideLength * SideLength;
            return area;
        }

        public override string ToString()
        {
            return ($"{Name} ({SideLength}x{SideLength}) square");
        }
    }
}

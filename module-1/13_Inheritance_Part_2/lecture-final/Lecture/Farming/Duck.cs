﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture.Farming
{
    public class Duck : FarmAnimal,ISellable
    {
        public Duck(string name, string sound) : base(name,sound)
        {
        }

        public override string ToString()
        {
            return "Wabbit season!";
        }

        public decimal GetSalesPrice()
        {
            return 15.95M;
        }

        public string PriceListing()
        {
            return "Ducks cost " + GetSalesPrice().ToString("C2");
        }
        public override string Eat()
        {
            return "Slurp slurp";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture.Farming
{
    public class Pig : FarmAnimal
    {
        public Pig(string name, string sound) : base(name,sound)
        {
        }
        public string Talk()
        {
            return "Hello Charlotte.";
        }
        public override string Eat()
        {
            return "Slop slop";
        }

    }
}

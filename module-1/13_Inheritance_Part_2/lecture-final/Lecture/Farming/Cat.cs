﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture.Farming
{
    public sealed class Cat : FarmAnimal
    {
        public string Sound { get; set; } 
        public Cat(string goofyName) : base(goofyName, "Meow")
        {
            Sound = "Hiss";
        }
        public override string Eat()
        {
            return "Tosses food around the room";
        }
    }
}

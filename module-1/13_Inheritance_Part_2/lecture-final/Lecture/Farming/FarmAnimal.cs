﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture.Farming
{
    public abstract class FarmAnimal : ISingable
    {
        public string Name { get; set; }
        private string _sound { get; set; }
        public string Sound
        {
            get
            {
                if (isAsleep)
                {
                    return "Zzzzzz";
                }
                else
                {
                    return _sound;
                }
            }
            set
            {
                _sound = value;
            }
        }
        private bool isAsleep { get; set; }
        public FarmAnimal(string name, string sound)
        {
            Name = name;
            _sound = sound;
        }
        public string MakeSoundOnce()
        {
            return Sound;
        }
        public string MakeSoundTwice()
        {
            return Sound + " " + Sound;
        }

        public bool AllowSleep()
        {
            isAsleep = true;
            return isAsleep;
        }

        public abstract string Eat();

    }
}

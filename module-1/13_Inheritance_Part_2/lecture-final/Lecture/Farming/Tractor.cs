﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture.Farming
{
    public class Tractor : ISingable,ISellable
    {
        public string Name { get; set; }
        public string Sound { get; }
        public Tractor(string sound)
        {
            Sound = sound;
        }
        public string MakeSoundOnce()
        {
            return Sound;
        }
        public string MakeSoundOnce(string soundToMake)
        {
            return soundToMake;
        }
        public string MakeSoundTwice()
        {
            return Sound + " " + Sound;
        }
        public decimal GetSalesPrice()
        {
            return 125000M;
        }

        public string PriceListing()
        {
            return $"Your tractor {Name} is on sale for {GetSalesPrice().ToString("C2")}";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture.Farming
{
    public class Cow : FarmAnimal
    {
        public Cow() : base("Cow","Moo")
        {
        }
        public override string Eat()
        {
            return "Munch munch";
        }
    }
}

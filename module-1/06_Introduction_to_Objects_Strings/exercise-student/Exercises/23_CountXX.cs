﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Count the number of "xx" in the given string. Overlapping is allowed, so "xxx" contains 2 "xx".
        CountXX("abcxx") → 1
        CountXX("xxx") → 2
        CountXX("xxxx") → 3
        */
        public int CountXX(string str)
        {
            int countXs = 0;
            string doubleXs = "xx";
            for (int i = 0; i<str.Length-1; i++)
            {
                if (doubleXs == str.Substring(i, 2))
                {
                    countXs++;
                }

            }
            return countXs;
        }
    }
}

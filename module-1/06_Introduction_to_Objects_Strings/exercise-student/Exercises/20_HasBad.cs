﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Given a string, return true if "bad" appears starting at index 0 or 1 in the string, 
        such as 
        with
        "badxxx" or "xbadxx" but not "xxbadxx". 
        The string may be any length, including 0. Note: OR StrartsWith* use .equals()
        to compare 2 strings.
        HasBad("badxx") → true
        HasBad("xbadxx") → true length ... has to be at least 4 && comapre mayhaps
        HasBad("xxbadxx") → false
        */
        public bool HasBad(string str)
        {
           if (str.StartsWith("bad") || ((str.Length >=4)  && str.Substring(1, 3).Contains("bad"))){
                return true;

            }

            return false;
        }
    }
}

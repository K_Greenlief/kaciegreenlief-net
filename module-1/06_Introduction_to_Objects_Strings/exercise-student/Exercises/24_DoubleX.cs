﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Given a string, return true if the first instance of "x" in the string
        is immediately followed by another "x".
        
        //find first insteance of an x
        //find index/location of that x
        // index +1 is also an x
        
        DoubleX("axxbb") → true
        DoubleX("axaxax") → false
        DoubleX("xxxxx") → true

        for loop iterate thru the string and look for x's
        then figure out if  i-1 or i+1 is also an x
        return true if two x's arenext to each other

        .compare



        */
        public bool DoubleX(string str)
        {
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str.Substring(i, 1).Equals("x") && str.Substring(i, 2).Equals("xx"))
                {
                    if (!(str.Substring(0, i)).Contains("x")){ 
                    return true;
                    }
                }
            }
            return false;
        }

    }
}


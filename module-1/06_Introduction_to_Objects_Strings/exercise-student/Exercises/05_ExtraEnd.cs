﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Given a string, return a new string made of 3 copies of the last 2 chars of the original string. 
        The string length will be at least 2.

        /get last two chars
        /return 3X 
        
         */
        public string ExtraEnd(string str)
        {

            if (str.Length >= 2)
            {
                string lastTwo = str.Substring(str.Length - 2);

                return (lastTwo + lastTwo + lastTwo);
            }
            else { return str; }

        }
    }
}

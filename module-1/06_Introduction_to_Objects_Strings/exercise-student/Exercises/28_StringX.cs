﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Given a string, return a version where all the "x" have been removed. Except an "x" at the very start or end
        should not be removed.
        StringX("xxHxix") → "xHix"
        StringX("abxxxcd") → "abcd"
        StringX("xabxxxcdx") → "xabcdx"

        check for what it is NOT
        */
        public string StringX(string str)
        {
            if (str.Length > 1)
            {

            string middleString = str.Substring(1, str.Length - 2).Replace("x","");
       

               str = str.Substring(0, 1) + middleString + str.Substring(str.Length - 1);
            }
            return str;
        }
    }
}

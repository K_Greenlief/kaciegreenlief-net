﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Given 2 strings, a and b, return a string of the form short+long+short, with the shorter string
        on the outside and the longer string on the inside. The strings will not be the same length, but
        they may be empty (length 0).
        ComboString("Hello", "hi") → "hiHellohi"
        ComboString("hi", "Hello") → "hiHellohi"
        ComboString("aaa", "b") → "baaab"

        so determine which string is longer
        split it like you did for the other problem
        */
        public string ComboString(string a, string b)
        {
            if (a.Length > b.Length)
            {
                string newAString = (b + a + b);
                return newAString;
            }
            else
            {
                string newBString = (a + b + a);
                return newBString;
            }
           
            
        }
    }
}

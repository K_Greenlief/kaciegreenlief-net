﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Given a string, return a new string made of every other char starting with the first, so "Hello" yields "Hlo".
        StringBits("Hello") → "Hlo"
        StringBits("Hi") → "H"
        StringBits("Heeololeo") → "Hello"

        target every other letter
        */
        public string StringBits(string str)
        {
            string newString = "";
            for (int i = 0; i < str.Length; i+=2)
            {
                newString = newString + str[i];
            }
            return newString;
        }
    }
}

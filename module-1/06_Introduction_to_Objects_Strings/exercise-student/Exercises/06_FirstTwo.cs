﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Given a string, return the string made of its first two chars, so the string "Hello" yields "He". If the
        string is shorter than length 2, return whatever there is, so "X" yields "X", and the empty string ""
        yields the empty string "". 
        */
        public string FirstTwo(string str)
        {  
            if (str.Length >= 2)
            {
                string firstInts = str.Substring(0, 2);
                return firstInts;
            }
            else
            {
                return str;
            }
        }
    }
}

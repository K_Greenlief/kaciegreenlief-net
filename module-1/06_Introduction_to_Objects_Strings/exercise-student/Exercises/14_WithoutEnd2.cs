﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Given a string, . The string
        may be any length, including 0.


        WithoutEnd2("Hello") → "ell"
        WithoutEnd2("abc") → "b"
        WithoutEnd2("ab") → ""
        */

        public string WithoutEnd2(string str)
        {
            if (str.Length < 3)
            {
                string zerostring = "";
                return zerostring;

            }
            else
            {
                string endlessString = str.Substring(1, str.Length - 2);
                return endlessString;
            }
        }
    }
}

﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Given a string, return the count of the number of times 
        that a substring length 2 appears in the string  (we won't count the end substring).
        Last2("hixxhi") → 1
        Last2("xaxxaxaxx") → 1
        Last2("axxxaaxx") → 2
        */
        public int Last2(string str)  
        {
           
            int count = 0;
            
            if (str.Length < 3)
            {
                return count;
            }
            else
            {
                string subString = str.Substring(str.Length - 2);
                for (int i = 0; i < str.Length - 2; i++)
                {
                    if (str.Substring(i, 2) == subString)
                    {
                        count += 1;
                    }
                   
                }
                
            } 
            return count;

        }
    }
}

﻿namespace Exercises
{
    public partial class StringExercises
    {
        /*
        Given a string, return a string made of the chars at indexes 0,1, 4,5, 8,9 ... so "kittens" yields "kien".
        AltPairs("kitten") → "kien"
        AltPairs("Chocolate") → "Chole"
        AltPairs("CodingHorror") → "Co[di]ng[ho]rr[or]"


          if (str.Length > 1)
            {

            string middleString = str.Substring(1, str.Length - 2).Replace("x","");
       

               str = str.Substring(0, 1) + middleString + str.Substring(str.Length - 1);
            }
            return str;
        */
        public string AltPairs(string str)
        {
            string firstIndex = str.Substring(0, 2);
            string secondIndex = str.Substring(4, 2);
            string thirdIndex = str.Substring(8, 2);

            if (str.Length < 5)
            {
                return firstIndex;
            }
            else if (str.Length >= 5 && str.Length <=9)
            {
                return firstIndex + secondIndex;
            }
            else
            {
                return firstIndex + secondIndex + thirdIndex;
            }

        
        }
    }
}

﻿using System;

namespace StringsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "Ada Lovelace";

            // Strings are actually arrays of characters (char).
            // Those characters can be accessed using [] notation.

            // 1. Write code that prints out the first and last characters
            //      of name.
            Console.Write(name[0]);
            Console.Write(name[name.Length - 1]);
            Console.WriteLine("First and Last Character. ");

            // 2. How do we write code that prints out the first three characters
            name.Substring(0, 3);
            Console.WriteLine("First 3 characters: ");

            // 3. Now print out the first three and the last three characters
            // Output: Adaace
            string lastThree = name.Substring(name.Length-3);
            Console.WriteLine("Last 3 characters: " + lastThree);

            // 4. What about the last word?
            // Output: Lovelace
            if (name.Contains("Lovelace")){
                return name;
            }

            // Console.WriteLine("Last Word: ");

            // 5. Does the string contain inside of it "Love"?
            // Output: true
            bool containsLove = name.Contains("Love");
            Console.WriteLine("Contains \"Love\"" + containsLove);

            // 6. Where does the string "lace" show up in name?
            // Output: 8
              
            Console.WriteLine("Index of \"lace\": " + name.IndexOf("Lace"));







            // 7. How many 'a's OR 'A's are in name?
            int output = 0;
            for (int i = 0; i < name.Length; i++)
            {
                if (name[i] == 'a' || name[i] == 'A')
                {
                    output += 1;
                }

            }
            Console.WriteLine("number of A's = " + output);


            // 8. Replace "Ada" with "Ada, Countess of Lovelace"

            name = name.Replace("Ada", "Ada, Countess of Lovelace");
            Console.WriteLine(name);

            // 9. Set name equal to null.
            name = null;
            // 10. If name is equal to null or "", print out "All Done".
            if (name == null || name == (""))
            {
                Console.WriteLine("All done");
            }
        }
    }
}
﻿using System;

namespace StringsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "Ada LovelAcE";

            // Strings are actually arrays of characters (char).
            // Those characters can be accessed using [] notation.

            // 1. Write code that prints out the first and last characters
            //      of name.
            // Output: A
            // Output: e
            Console.WriteLine("First and Last Character. ");
            Console.WriteLine(name[0]);
            Console.WriteLine(name[name.Length - 1]);
            // every other character
            for(int i = 0; i < name.Length; i+=2)
            {
                Console.Write(name[i] + " ");

            }
            Console.WriteLine();

            // 2. How do we write code that prints out the first three characters
            // Output: Ada
            string three = name.Substring(0, 3);
            Console.WriteLine($"First 3 characters: {three}");

            // 3. Now print out the first three and the last three characters
            // Output: Adaace
            string lastThree = name.Substring(name.Length - 3);
            Console.WriteLine($"Last 3 characters: {three}{lastThree}");

            // 4. What about the last word?
            // Output: Lovelace
            int spaceIndex = name.IndexOf(' ');
            string lastWord = name.Substring(spaceIndex+1);
            Console.WriteLine("Last Word: " + lastWord);

            string[] words = name.Split(' ');
            Console.WriteLine("Last Word 2: " + words[words.Length-1]);

            // 5. Does the string contain inside of it "Love"?
            // Output: true
            bool containsLove = name.Contains("Love");
            Console.WriteLine("Contains \"Love\" " + containsLove);

            // 6. Where does the string "lace" show up in name?
            // Output: 8
            string adaLower = name.ToLower();
            Console.WriteLine("Index of \"lace\": " + adaLower.IndexOf("lace"));
            Console.WriteLine("Index of \"lace\": " + name.ToLower().IndexOf("lace"));
            // *** Come back to this

            // 7. How many 'a's OR 'A's are in name?
            // Output: 3
            // come back to this one too
            int numberOfAs = 0;
            for(int i = 0; i < name.Length; i++)
            {
                if(name[i] == 'a' || name[i] == 'A')
                {
                    numberOfAs += 1;
                }
            }
            Console.WriteLine("Number of \"a's\": " + numberOfAs);

            numberOfAs = 0;
            for(int i = 0; i < name.Length; i++)
            {
                if(name[i].ToString().ToLower() == "a")
                {
                    numberOfAs += 1;
                }
            }
            Console.WriteLine("Number of \"a's\" tolower: " + numberOfAs);

            // 8. Replace "Ada" with "Ada, Countess of Lovelace"
            name = name.Replace("Ada", "Ada, Countess of Lovelace");
            Console.WriteLine(name);

            // 9. Set name equal to null.
            name = null;
            // 10. If name is equal to null or "", print out "All Done".
            if(name == null || name.Equals(""))
            {
                Console.WriteLine("All done");
            }
            //Console.ReadLine();
            if(name == null || name == "")
            {
                Console.WriteLine("All done");
            }
            if(string.IsNullOrEmpty(name))
            {
                Console.WriteLine("Yes it is null or empty");
            }
            //Console.ReadLine();
        }
    }
}
﻿using System;

namespace StackVHeap
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] superheroes = new string[4];
            superheroes[0] = "Iron Man";
            superheroes[1] = "Batman";
            superheroes[2] = "Thor";
            superheroes[3] = "Wonder Woman";

            Console.WriteLine("Superhero at index 2 " + superheroes[2]);
            Console.WriteLine("Press enter to continue...");
            //Console.ReadLine();
            Console.Clear();

            string[] superheroCopy = superheroes;
            superheroCopy[2] = "Dr. Strange";
            Console.WriteLine("Superhero at index 2 " + superheroCopy[2]);
            Console.WriteLine("Superhero at index 2 " + superheroes[2]);

            int number = 1;
            int numberCopy = number;
            numberCopy = 2;

            Console.WriteLine("number: " + number);
            Console.WriteLine("numberCopy: " + numberCopy);

            string hello = "Hello World!";
            Console.WriteLine(hello[3]);

            for(int i = 0; i < hello.Length;i++)
            {
                Console.WriteLine(hello[i]);
            }
            int indexOfW = hello.IndexOf('W');
            Console.WriteLine(indexOfW);
            hello = "Hello my honey, hello my baby";
            string helloUP = hello.ToUpper();
            Console.WriteLine(hello);
            Console.WriteLine(helloUP);
            string hello2 = "Hello my honey, hello my baby";
            Console.WriteLine("String comparison " + (hello == hello2));

            int[] arrayOne = { 1, 2, 3, 4, 5 };
            int[] arrayTwo = { 1, 2, 3, 4, 5 };
            Console.WriteLine("Array comparison " + (arrayOne == arrayTwo));

        }
    }
}

﻿namespace BankTellerExercise.Classes
{
    public class SavingsAccount : BankAccount
    {
        public SavingsAccount(string accountHolderName, string accountNumber) : base(accountHolderName, accountNumber) { }
        public SavingsAccount(string accountHolderName, string accountNumber, decimal balance) : base(accountHolderName, accountNumber, balance) { }


        /*   public virtual decimal Withdraw(decimal amountToWithdraw)
        {
            Balance -= amountToWithdraw;
            return Balance;
        }
        */
        public override decimal Withdraw(decimal amountToWithdraw)
        {
            decimal withdrawalPlusFees = amountToWithdraw + 2.00M;

            if (Balance - amountToWithdraw < 150 && Balance - withdrawalPlusFees >= 0)
            {
                return base.Withdraw(amountToWithdraw + 2M);
                
            }
            else if (Balance - withdrawalPlusFees < 0)
            {
                return Balance;
            }
            else
            {
                return base.Withdraw(amountToWithdraw);
            }
        }

    }
}

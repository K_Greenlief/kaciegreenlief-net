﻿namespace BankTellerExercise.Classes
{
    public class CheckingAccount : BankAccount
    {
        public CheckingAccount(string accountHolderName, string accountNumber) : base(accountHolderName, accountNumber) { }
        public CheckingAccount(string accountHolderName, string accountNumber, decimal balance) : base(accountHolderName, accountNumber, balance) { }

        public override decimal Withdraw(decimal amountToWithdraw)
        {
            if(Balance - amountToWithdraw > -100M) 
            { 
                base.Withdraw(amountToWithdraw);
                if(Balance < 0M)
                {
                    base.Withdraw(10M);
                } 
            }
            return Balance;
        }
    }
}

/*
public override decimal Withdraw(decimal amountToWithdraw)
{
    decimal newBalance = Balance - amountToWithdraw;

    if (newBalance < 0.00M && newBalance <= -100.00M)
    {
        return Balance;
    }
    else if (newBalance < 0.00M && newBalance > -100.00M)
    {
        return Balance = (newBalance - 10.00M);
    }
    else
    {
        return Balance -= amountToWithdraw;
    }
  ^^^^ *****why doesn't this work when I try to return balance? (this code works, 
but only if I change Balance to 'protected' instead of 'private', and I'm still not entirely 
sure why one is better than the other. )****   */ 
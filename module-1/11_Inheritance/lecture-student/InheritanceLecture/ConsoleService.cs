﻿using InheritanceLecture.Auctioneering;
using System;
using System.Collections.Generic;
using System.Text;

namespace InheritanceLecture
{
    public class ConsoleService
    {
        public void Start()
        {
            // Create a header
            Console.WriteLine("Welcome to .NET Auction Site");
            Console.WriteLine();
            Console.WriteLine("Starting a general auction");
            Auction generalAuction = new Auction("Spider-Man #1");

            // No need to save the object in this context, just pass it to the method
            generalAuction.PlaceBid(new Bid("Medvitz", 10));
            generalAuction.PlaceBid(new Bid("Henry Edwards", 25));
            generalAuction.PlaceBid(new Bid("Bill Reeves", 15));
            generalAuction.PlaceBid(new Bid("Mimi Malone", 50));

            // Create a header
            Console.WriteLine("Welcome to .NET Reserve Auction Site");
            Console.WriteLine();
            Console.WriteLine("Starting a reserve auction");
            ReserveAuction reserveAuction = new ReserveAuction(5,"Used computer");
            Console.WriteLine("You are bidding on a " + reserveAuction.Item);
            reserveAuction.PlaceBid(new Bid("O'Brien", 75));
            reserveAuction.PlaceBid(new Bid("Walt", 85));
            reserveAuction.PlaceBid(new Bid("Kim", 55));
            reserveAuction.PlaceBid(new Bid("Medvitz", 25));

            Console.WriteLine();
            // Create a header
            Console.WriteLine("Welcome to .NET Buy Out Auction Site");
            Console.WriteLine();
            Console.WriteLine("Starting a buy out auction");
            BuyOutAuction buyOutAuction = new BuyOutAuction(1000);
            buyOutAuction.PlaceBid(new Bid("Medvitz", 100));
            buyOutAuction.PlaceBid(new Bid("Impelliciari", 150));
            buyOutAuction.PlaceBid(new Bid("Goodrow", 100));
            buyOutAuction.PlaceBid(new Bid("O'Brien", 1500));
            buyOutAuction.PlaceBid(new Bid("Edwards", 2000));

        }
    }
}

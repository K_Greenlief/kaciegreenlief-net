﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InheritanceLecture.Auctioneering
{
    public class ReserveAuction : Auction
    {
        private decimal ReservePrice { get; }
        public ReserveAuction(decimal reservePrice, string itemName) : base(itemName)
        {
            ReservePrice = reservePrice;
        }

        public override bool PlaceBid(Bid offeredBid)
        {
            bool isCurrentHighBid = false;
            // Only going to capture bids that meet or beat the reservePrice
            if (offeredBid.BidAmount >= ReservePrice)
            {
                Console.WriteLine("Reserve has been met!");
                // Store is int the CurrentBid and add it to the list
                isCurrentHighBid = base.PlaceBid(offeredBid);
            }
            else
            {
                Console.WriteLine("You're bid is very small, it didn't meet the reserve.");
            }
            return isCurrentHighBid;
        }
    }
}

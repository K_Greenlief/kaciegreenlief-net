﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InheritanceLecture.Auctioneering
{
    public class BuyOutAuction : Auction
    {
        private decimal buyOutPrice { get; set; }
        public BuyOutAuction(decimal buyOutPrice) : base("Golf clubs")
        {
            this.buyOutPrice = buyOutPrice;
        }

        public override bool PlaceBid(Bid offeredBid)
        {
            // buy out logic
            if(!HasEnded)
            {
                if (offeredBid.BidAmount >= buyOutPrice)
                {
                    // let's be nice, and not gouge our customers
                    Bid buyoutBid = new Bid(offeredBid.Bidder, buyOutPrice);
                    base.PlaceBid(buyoutBid);
                    Console.WriteLine("Buyout met by " + offeredBid.Bidder);
                    EndAuction();
                }
                else
                {
                    base.PlaceBid(offeredBid);
                }
            }
            else
            {
                Console.WriteLine("The auction is over.");
            }
            return false;
        }
    }
}

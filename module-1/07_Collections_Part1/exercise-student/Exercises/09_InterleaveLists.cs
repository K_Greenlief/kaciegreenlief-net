﻿using System;
using System.Collections.Generic;

namespace Exercises
{
    public partial class Exercises
    {
        /*
        Given two lists of Integers, interleave them beginning with the first element in the first list followed
        by the first element of the second. Continue interleaving the elements until all elements have been interwoven.
        Return the new list. If the lists are of unequal lengths, simply attach the remaining elements of the longer
        list to the new list before returning it.
        // if 1 < 2
        // if 2 < 1
        //if 2 == 1
        InterleaveLists( [1, 2, 3], [4, 5, 6] )  ->  [1, 4, 2, 5, 3, 6]
        */
        public List<int> InterleaveLists(List<int> listOne, List<int> listTwo)
        {
            List<int> newList = new List<int>();

            if (listOne.Count >= listTwo.Count)
            {
                for (int i = 0; i < listOne.Count; i++)
                {
                    if (i < listTwo.Count)
                    {
                        newList.Add(listOne[i]);
                        newList.Add(listTwo[i]);
                    }
                    else 
                    {
                        newList.Add(listOne[i]);
                    }
                }
                
            }
        
            else if (listOne.Count<listTwo.Count)
            {
                for (int j = 0; j < listTwo.Count; j++)
                {
                    if (j < listOne.Count)
                    {
                        newList.Add(listOne[j]);
                        newList.Add(listTwo[j]);
                    }
                    else
                    {
                        newList.Add(listTwo[j]);
                    }
                }
            }
            return newList;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CollectionsLectureNotes
{
    class Program
    {
        static void Main(string[] args)
        {

            // QUEUE <T>
            //
            // Queues are a special type of data structure that follow First-In First-Out (FIFO).
            // With Queues, we Enqueue (add) and Dequeue (remove) items.

            // Create a queue of strings
            Queue<string> tasks = new Queue<string>();

            // Add four tasks to the queue that you do every day
            tasks.Enqueue("Brush teeth");
            tasks.Enqueue("Drink coffee");
            tasks.Enqueue("Pet Eddie");
            tasks.Enqueue("Read the comics");
            /////////////////////
            // PROCESSING ITEMS IN A QUEUE
            /////////////////////

            // What is the next item?
            string nextItem = tasks.Peek();
            Console.WriteLine(nextItem);
            // Print each task to the console in the order they were entered (FIFO)
            while(tasks.Count > 0)
            {
                Console.WriteLine("My morning routine includes " + tasks.Dequeue());
            }
            Console.WriteLine(tasks.TryPeek(out nextItem));
            // STACK <T>
            //
            // Stacks are another type of data structure that follow Last-In First-Out (LIFO).
            // With Stacks, we Push (add) and Pop (remove) items. 

            // Create a stack of strings
            Stack<string> breakfastItems = new Stack<string>();
            // Add to the stack four things you might eat for breakfast
            breakfastItems.Push("Eggs");
            breakfastItems.Push("Pop-tarts");
            breakfastItems.Push("Mimosas");
            breakfastItems.Push("Lucky Charms");
          

            ////////////////////
            // POPPING THE STACK
            ////////////////////
            
            // Print each breakfast item to the console in reverse order (LIFO)
            while(breakfastItems.Count > 0)
            {
                Console.WriteLine(breakfastItems.Count);
                Console.WriteLine(breakfastItems.Pop());
            }

        }
    }
}

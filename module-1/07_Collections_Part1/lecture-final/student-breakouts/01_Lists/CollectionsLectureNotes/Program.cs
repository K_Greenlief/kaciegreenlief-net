﻿using System;
using System.Collections.Generic;

namespace CollectionsLectureNotes
{
    class Program
    {
        static void Main(string[] args)
        {
            // LIST<T>
            //
            // Lists allow us to hold collections of data. They are declared with a type of data that they hold
            // only allowing items of that type to go inside of them.
            //
            // The syntax used for declaring a new list of type T is
            //      List<T> list = new List<T>();
            //
            //

            // Create two lists of integers
            List<int> listOne;
            listOne = new List<int>();
            List<int> listTwo = new List<int>();
            // Create list of strings
            List<string> namesList = new List<string>();
            // Write these variables to the console
            Console.WriteLine(listOne);
            Console.WriteLine(listTwo);
            Console.WriteLine(namesList);
            int foo = 8;
            Console.WriteLine(foo);
            int[] bar = new int[4];
            Console.WriteLine(bar);
            // Discuss: What did you see on the console? Is that what you expected?
            // For reference types, Console.WriteLine prints out the datatype not the values

            /////////////////


            //////////////////
            // OBJECT EQUALITY
            //////////////////

            // Check if the first list you created is equal to the second list
            // If they are equal, write "They are the same" to the console.
            // If they are not equal, write "They are not the same" to the console.
            // Discuss: Why did you get that result?
            if(listOne == listTwo)
            {
                Console.WriteLine("They are the same");
            }
            else
            {
                Console.WriteLine("They are different");
            }
            // Assign the first integer list to the second integer list
            listOne = listTwo;
            if (listOne == listTwo)
            {
                Console.WriteLine("They are the same, this time.");
            }
            else
            {
                Console.WriteLine("They are still different");
            }

            // Check if the first list you created is equal to the second list
            // If they are equal, write "They are the same" to the console.
            // If they are not equal, write "They are not the same" to the console.
            // Discuss: Why did you get that result?

            // the equals sign copies the stack value to the new variable. In this case
            // that's the memory address

            /////////////////
            // ADDING ITEMS
            /////////////////

            // Add three numbers to one of the integer lists
            listOne.Add(42);
            listOne.Add(8);
            listOne.Add(50);

            // Add four words to the list of strings
            string[] words = { "Hello", "World", "Mars", "Universe" };
            namesList.AddRange(words);
            string[] namesAfter = namesList.ToArray();

            //////////////////
            // ACCESSING BY INDEX
            //////////////////
            // Use a for loop to access each element by its index
            // Write each element to the console. 
            // Do this for both the integer list and the string list.
            for(int i = 0; i < listOne.Count;i++)
            {
                Console.WriteLine(listOne[i]);
            }
            for(int i = 0; i < namesList.Count; i++)
            {
                Console.Write("With a for loop on namesList: " + namesList[i]);
            }
            foreach(string name in namesList)
            {
                Console.WriteLine("From namesList: " + name);
            }

            foreach(string word in words)
            {
                Console.WriteLine("From words array: " + word);
            }
        }
    }
}

﻿using System;

namespace FileIOPart1
{
    class Program
    {
        static void Main(string[] args)
        {
            FileFunctions fileFunctions = new FileFunctions();
            fileFunctions.UsingTheDirctoryClass();
            fileFunctions.UsingTheFileClass();
            fileFunctions.ReadTheInputFile();
            Console.WriteLine(fileFunctions.SumAllNumbers());
        }
    }
}

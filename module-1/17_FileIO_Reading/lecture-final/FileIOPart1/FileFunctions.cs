﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileIOPart1
{
    public class FileFunctions
    {
        public void UsingTheDirctoryClass()
        {
            // Getting the current directory our program is running in
            // not always the same directory our .cs code-file is in
            string currentDirectory = Directory.GetCurrentDirectory();

            // see if a directory exists
            bool directoryExists = Directory.Exists("C:\\testdirectory");
            if (directoryExists)
            {
                Directory.CreateDirectory("C:\\testdirectory\\June1");
            }
            // @ means no escape characters
            Directory.CreateDirectory(@"c:\test2\june1\1137");
            string[] subDirectories = Directory.GetDirectories(@"c:\testdirectory");
            //Directory.Delete(@"c:\testdirectory");
            Directory.Delete("C:\\test2\\june1\\1137");
        }

        public void UsingTheFileClass()
        {
            const string FilePath = @"C:\TestDirectory\FileName.txt";

            // Check to see if a file exists
            bool fileExists = File.Exists(FilePath);
            if (fileExists)
            {
                // To copy a file we need the source and destination
                File.Copy(FilePath, @"C:\TestDirectory\DestinationFileName.txt");

                // To delete a file we can use the file path
                File.Delete(FilePath);
            }
        }
        // Let's read that file
        public void ReadTheInputFile()
        {
            // find the input file
            string directory = Directory.GetCurrentDirectory();
            string filename = "Input.txt";
            // create the full path
            string fullPath = Path.Combine(directory, filename);

            // DANGER WILL ROBINSON
            // this works just fine, as long as you remember to clean up.
            // using is nicer :-)
            try
            {
                StreamReader sr = new StreamReader(fullPath);
                // now, let's read the file, one line at a time.
                // check the endofstream property for when we are done.
                while (!sr.EndOfStream)
                {
                    // Read a line from the stream
                    string line = sr.ReadLine();
                    // Write it to the screen
                    Console.WriteLine(line);
                }
                sr.Close();
            }
            catch (IOException io)
            {
                Console.WriteLine(io.Message);
            }
            catch (Exception)
            {
                Console.WriteLine("oops");
            }
        }

        public int SumAllNumbers()
        {
            // Get the file
            string directory = Directory.GetCurrentDirectory();
            string filename = "numbers.csv";
            string fullPath = Path.Combine(directory, filename);
            int sum = 0;
            try
            {
                using (StreamReader sr = new StreamReader(fullPath))
                {
                    // read until we are done
                    while (!sr.EndOfStream)
                    {
                        // read a line
                        string lineFromCsv = sr.ReadLine();
                        // splint into individual values
                        string[] array = lineFromCsv.Split(',');
                        // loop through the array
                        foreach (string number in array)
                        {
                            // parse from string to int
                            // Here, if we have a problem reading a bit of data
                            // Ignore it, and continue with the next data item
                            try
                            {
                                sum += int.Parse(number);
                            }
                            catch (Exception)
                            { }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return sum;
        }
    }
}

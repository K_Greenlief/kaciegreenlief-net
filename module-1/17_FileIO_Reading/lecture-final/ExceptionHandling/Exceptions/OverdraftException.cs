﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExceptionHandling.Exceptions
{
    public class OverdraftException : Exception
    {
        public decimal OverdraftAmount { get; } = 0.0M;
        public OverdraftException(string message, decimal overDraftAmount) : base(message)
        {
            OverdraftAmount = overDraftAmount;
        }
    }
}

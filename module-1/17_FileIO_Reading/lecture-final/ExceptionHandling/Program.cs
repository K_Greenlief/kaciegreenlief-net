﻿using ExceptionHandling.Exceptions;
using System;

namespace ExceptionHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("The following cities: ");
            string[] cities = new string[3];
            cities[0] = "Pittsburgh";
            cities[1] = "Philadelphia";
            cities[2] = "New Alexandria";

            try
            {
                for (int i = 0; i < cities.Length; i++)
                {
                    Console.Write(" " + cities[i] + ",");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Ooops, something seems to have gone wrong.");
            }
            Console.WriteLine("are all in Pennsylvania");

            // errors bubble
            try
            {
                Console.WriteLine("Enter a number");
                string input = Console.ReadLine();
                int number = int.Parse(input);
                DoSomethingDangerous();

            }
            catch (IndexOutOfRangeException i)
            {
                Console.WriteLine("Ooops, went too far");
            }
            catch (FormatException fe)
            {
                Console.WriteLine(fe.Message);
                Console.WriteLine("Please enter a numerical value.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Caughtcha!");
            }

            try
            {
                double hotelCost = CalculateHotelRoomCharges(1, 2);
                hotelCost = CalculateHotelRoomCharges(2, -2);
            }
            catch (ArgumentOutOfRangeException ae)
            {
                Console.WriteLine(ae.ParamName);
                Console.WriteLine(ae.Message);
            }
            catch (Exception)
            {
                Console.WriteLine("Something went wrong");
            }

            // let's use our own exception
            try
            {
                decimal myBalance = Withdraw(3000M);
                Console.WriteLine("The final balance is " + myBalance);
                myBalance = Withdraw(7000M);
                Console.WriteLine("The final balance is " + myBalance);
            }
            catch (OverdraftException oe)
            {
                Console.WriteLine(oe.Message);
                Console.WriteLine("You are overdrawn by " + oe.OverdraftAmount);
            }
            catch (Exception)
            {
                Console.WriteLine("Something went wrong");
            }
            // optional. Executes whether there was an exception or not.
            finally
            {
                Console.WriteLine("Thank you for banking with Flybynight Bank");
            }
        }

        private static void DoSomethingDangerous()
        {
            int[] numbers = new int[5];
            for (int i = 0; i < 10; i++)
            {
                numbers[i] = i;
            }
            Console.WriteLine("Look ma, no exceptions!");
        }

        private static double CalculateHotelRoomCharges(int nights, int numberOfGuest)
        {
            if (nights < 1)
            {
                throw new ArgumentOutOfRangeException("nights", nights, "Minimum number of nights is 1");
            }
            if (numberOfGuest < 1)
            {
                throw new ArgumentOutOfRangeException("numberOfGuest", numberOfGuest, "Minimum number of guests is 1");
            }

            double dailyRate = 85 + (nights * numberOfGuest);
            return dailyRate;
        }

        private static decimal Withdraw(decimal amount)
        {
            const decimal StartingBalance = 5000M;
            decimal finalBalance = StartingBalance - amount;
            if (finalBalance < 0)
            {
                throw new OverdraftException("You're account is overdrawn.", finalBalance);
            }
            return finalBalance;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WordSearch
{
    public class Program
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("What is the fully qualified name of the file that should be searched?");
            string filePath = $"{Console.ReadLine()}";
            Console.WriteLine("What is the search word you are looking for?"); 
            string searchString = $"{Console.ReadLine()}";
            Console.WriteLine("Should the search be case sensitive? (y/n)");
            string yesOrNo = Console.ReadLine();
            char.Parse(yesOrNo);
            
            

  
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    int lineCount = 0;

                    while (!sr.EndOfStream)
                    {
                        lineCount++;

                        string aliceLine = sr.ReadLine();
                        if (aliceLine.ToLower().Contains(searchString) && (yesOrNo == "n"))
                        { 
                            
                            Console.WriteLine(lineCount + " " + aliceLine);
                        }
                        else if (aliceLine.Contains(searchString) && (yesOrNo == "y"))
                        {
                            Console.WriteLine(lineCount + " " + aliceLine);
                        }


                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}

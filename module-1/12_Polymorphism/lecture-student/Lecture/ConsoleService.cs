﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture
{
    public class ConsoleService
    {
        public void LetsSing()
        {
            //
            // OLD MACDONALD
            //
            string sound = "Moo";
            string name = "Cow";

            Console.WriteLine("Old MacDonald had a farm, ee ay ee ay oh!");
            Console.WriteLine("And on his farm he had a " + name + ", ee ay ee ay oh!");
            Console.WriteLine("With a " + sound + " " + sound + " here");
            Console.WriteLine("And a " + sound + " " + sound + " there");
            Console.WriteLine("Here a " + sound + " there a " + sound + " everywhere a " + sound + " " + sound);
            Console.WriteLine();

        }
    }
}

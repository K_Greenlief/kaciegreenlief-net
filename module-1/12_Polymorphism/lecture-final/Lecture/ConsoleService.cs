﻿using Lecture.Farming;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture
{
    public class ConsoleService
    {
        public void LetsSing()
        {
            //
            // OLD MACDONALD
            //
            // with individual classes
            Cow bessie = new Cow();
            Console.WriteLine("Old MacDonald had a farm, ee ay ee ay oh!");
            Console.WriteLine("And on his farm he had a " + bessie.Name + ", ee ay ee ay oh!");
            Console.WriteLine("With a " + bessie.Sound + " " + bessie.Sound + " here");
            Console.WriteLine("And a " + bessie.Sound + " " + bessie.Sound + " there");
            Console.WriteLine("Here a " + bessie.Sound + " there a " + bessie.Sound + " everywhere a " + bessie.Sound + " " + bessie.Sound);
            Console.WriteLine();
            Duck donald = new Duck();
            Console.WriteLine("Old MacDonald had a farm, ee ay ee ay oh!");
            Console.WriteLine("And on his farm he had a " + donald.Name + ", ee ay ee ay oh!");
            Console.WriteLine("With a " + donald.Sound + " " + donald.Sound + " here");
            Console.WriteLine("And a " + donald.Sound + " " + donald.Sound + " there");
            Console.WriteLine("Here a " + donald.Sound + " there a " + donald.Sound + " everywhere a " + donald.Sound + " " + donald.Sound);
            Console.WriteLine();

            List<FarmAnimal> animals = new List<FarmAnimal>();
            animals.Add(bessie);
            animals.Add(donald);
            Pig wilbur = new Pig();
            wilbur.Name = "Pig";
            wilbur.Sound = "Oink";
            animals.Add(wilbur);
            Console.WriteLine(wilbur.Talk());
            Console.WriteLine("Now with feeling...I mean polymoriphism");
            Console.WriteLine();
            foreach(FarmAnimal farmAnimal in animals)
            {
                // figure out if I have a pig
                if(farmAnimal.GetType() == typeof(Pig))
                {
                    Console.WriteLine(((Pig)farmAnimal).Talk());
                }
                Console.WriteLine("Old MacDonald had a farm, ee ay ee ay oh!");
                Console.WriteLine("And on his farm he had a " + farmAnimal.Name + ", ee ay ee ay oh!");
                Console.WriteLine("With a " + farmAnimal.MakeSoundTwice() + " here");
                Console.WriteLine("And a " + farmAnimal.MakeSoundTwice() + " there");
                Console.WriteLine("Here a " + farmAnimal.MakeSoundOnce() + " there a " + farmAnimal.MakeSoundOnce() + " everywhere a " + farmAnimal.MakeSoundTwice());
                Console.WriteLine();

            }

            // Overriding from System.Object
            Console.WriteLine(donald);

            // now with interfaces
            Console.WriteLine();
            Console.WriteLine("Now with feeling...I mean Interfaces");
            Console.WriteLine();

            List<ISingable> singables = new List<ISingable>();
            Tractor deere = new Tractor("Vroom");
            deere.Name = "John";
            singables.Add(deere);
            singables.AddRange(animals);

            foreach(ISingable item in singables)
            {
                Console.WriteLine("Old MacDonald had a farm, ee ay ee ay oh!");
                Console.WriteLine("And on his farm he had a " + item.Name + ", ee ay ee ay oh!");
                Console.WriteLine("With a " + item.MakeSoundTwice() + " here");
                Console.WriteLine("And a " + item.MakeSoundTwice() + " there");
                Console.WriteLine("Here a " + item.MakeSoundOnce() + " there a " + item.MakeSoundOnce() + " everywhere a " + item.MakeSoundTwice());
                Console.WriteLine();

            }

            // Let's make some money
            Apple redDelicious = new Apple("Red Delicious");
            redDelicious.SetPrice(2M);
            Apple honeyCrisp = new Apple("Honey Crisp");
            honeyCrisp.SetPrice(5M);
            List<ISellable> itemsToSell = new List<ISellable>()
            {
                redDelicious,honeyCrisp
            };
            itemsToSell.Add(donald);
            itemsToSell.Add(deere);
            foreach(ISellable moneyMaker in itemsToSell)
            {
                Console.WriteLine(moneyMaker.PriceListing());
            }

            // since everything inherits from System.Object, I can put everything
            List<System.Object> obj = new List<System.Object>();
            obj.Add(bessie);
            obj.Add(wilbur);
            obj.Add(deere);
            obj.Add(redDelicious);
        }
    }
}

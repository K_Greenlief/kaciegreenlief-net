﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture.Farming
{
    public interface ISingable
    {
        // define what I need for a singable object
        // The bare minimum
        string Name { get; }
        string Sound { get; }
        string MakeSoundOnce();
        string MakeSoundTwice();
    }
}

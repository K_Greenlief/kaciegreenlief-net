﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture.Farming
{
    public class Cow : FarmAnimal
    {
        public Cow()
        {
            Name = "Cow";
            Sound = "Moo";
        }
    }
}

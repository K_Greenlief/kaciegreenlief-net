﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankTellerExercise
{
    public interface IAccountable
    {
        public int Balance { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankTellerExercise
{
    public class BankCustomer
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        private List<IAccountable> accounts { get; set; } = new List<IAccountable>();
        public bool IsVip
        {
            get
            {
                int sum = 0;
                foreach (IAccountable item in accounts)
                {
                    sum += item.Balance;
                }
                if (sum >= 25000)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void AddAccount(IAccountable newAccount)
        {
            accounts.Add(newAccount);
        }

        public IAccountable[] GetAccounts() 
        {
            return accounts.ToArray();
        }

    }
}

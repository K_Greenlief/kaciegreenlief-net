﻿using Exercises.Classes;
using System;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            HomeworkAssignment homework = new HomeworkAssignment(100, "");

            Employee employee = new Employee();
            employee.RaiseSalary(0.0);
           


            FruitTree fruitTree = new FruitTree();
            fruitTree.PickFruit(0);

            Airplane airplane = new Airplane();
            airplane.ReserveSeats();

            Elevator elevator = new Elevator();
            elevator.OpenDoor();
            elevator.CloseDoor();
            elevator.GoUp(0);
            elevator.GoDown(0);


            Television tv = new Television();
            tv.TurnOn();
            tv.TurnOff();
            tv.ChangeChannel(5);


        }
    }
}

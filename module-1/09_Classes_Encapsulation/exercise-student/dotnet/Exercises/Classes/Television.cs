﻿namespace Exercises.Classes
{
    public class Television
    {
        public bool IsOn { get; private set; }
        public int CurrentChannel { get; private set; } = 3;
        public int CurrentVolume { get; private set; }

        public Television()
        {
            CurrentVolume = 2;
            //set vol to default of 2 ^^
        }

        /// <summary>
        /// turn tv off
        /// </summary>
        public void TurnOff()
        {
            IsOn = false;
        }
        /// <summary>
        /// tv on, set channel 3, vol 2
        /// </summary>
        public void TurnOn()
        {
            IsOn = true;
            CurrentChannel = 3;
            CurrentVolume = 2;
        }
        /// <summary>
        /// channel changer
        /// </summary>
        /// <param name="newChannel"></param>
        public void ChangeChannel(int newChannel)
        {
            if (IsOn)
            {
                if (newChannel >= 3 && newChannel <= 18)
                {
                    CurrentChannel = newChannel;
                }
            }
        }
        /// <summary>
        /// increases chan by one if ON
        /// </summary>
        public void ChannelUp()
        {
            if (IsOn)
            {
                if (CurrentChannel == 18)
                {
                    CurrentChannel = 3;
                }
                else
                {
                    CurrentChannel++;
                }
            }
        }
        /// <summary>
        /// decreases channel by 1 if ON
        /// </summary>
        public void ChannelDown()
        {
            if (IsOn)
            {
                if (CurrentChannel == 3)
                {
                    CurrentChannel = 18;
                }
                else
                {
                    CurrentChannel--;
                }
            }
        }
        public void RaiseVolume()
        {
            if (IsOn && CurrentVolume != 10)
            {
                CurrentVolume++;
            }

        }
        public void LowerVolume()
        {
            if (IsOn && CurrentVolume != 0)
            {
                    CurrentVolume--;
            }

        }

    }
}

﻿using System;

namespace Exercises.Classes
{
    public class Airplane
    {
        public string PlaneNumber { get; private set; }
        public int TotalFirstClassSeats { get; private set; }
        public int BookedFirstClassSeats { get; private set; }
        public int AvailableFirstClassSeats
        {
            get
            {
                return this.TotalFirstClassSeats - this.BookedFirstClassSeats;
            }
        }
        public int TotalCoachSeats { get; private set; }
        public int BookedCoachSeats { get; private set; }
        public int AvailableCoachSeats
        {
            get
            {
                return this.TotalCoachSeats - this.BookedCoachSeats;
            }
        }

        public Airplane(string planeNumber, int totalFirstClassSeats, int totalCoachSeats)
        {
            this.PlaneNumber = planeNumber;
            this.TotalFirstClassSeats = totalFirstClassSeats;
            this.TotalCoachSeats = totalCoachSeats;
        }

        public Airplane()
        {
            PlaneNumber = "";
        }

        public bool ReserveSeats(bool forFirstClass = false, int totalNumberOfSeats = 2)
        {
            if (forFirstClass == true && (totalNumberOfSeats + this.BookedFirstClassSeats < this.TotalFirstClassSeats))
            {
                this.BookedFirstClassSeats += totalNumberOfSeats;
                return true;
            }
            if (forFirstClass == false && (totalNumberOfSeats + this.BookedCoachSeats < this.TotalCoachSeats))
            {
                this.BookedCoachSeats += totalNumberOfSeats;
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}

﻿namespace Exercises.Classes
{
    public class HomeworkAssignment
    {
        public int EarnedMarks { get; set; }
        public int PossibleMarks { get; private set; } = 100;
        public string SubmitterName { get; private set; }
        public string LetterGrade
        {
            get
            {
                int testScore = ((EarnedMarks * 100) / PossibleMarks);
                if (testScore >= 90)
                {
                    return "A";
                }
                else if (testScore >= 80 && testScore <= 89)
                {
                    return "B";
                }
                else if (testScore >= 70 && testScore <= 79)
                {
                    return "C";
                }
                else if (testScore >= 60 && testScore <= 69)
                {
                    return "D";
                }
                else
                {
                    return "F";
                }
            }
        }


        public HomeworkAssignment(int possibleMarks, string submitterName)
        {
            this.PossibleMarks = possibleMarks;
            this.SubmitterName = submitterName;
        }
    }
}

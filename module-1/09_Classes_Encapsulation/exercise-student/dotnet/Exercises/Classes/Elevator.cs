﻿namespace Exercises.Classes
{
    public class Elevator
    {
        public int CurrentLevel { get; private set; } = 1;
        public int NumberOfLevels { get; private set; }
        public bool DoorIsOpen { get; private set; }

        public Elevator(int numberOfLevels = 15)
        {
            this.NumberOfLevels = numberOfLevels;
        }

        public void OpenDoor()
        {
            if (!DoorIsOpen)
            {
                DoorIsOpen = true;
            }
        }
        public void CloseDoor()
        {
            if (DoorIsOpen)
            {
                DoorIsOpen = false;
            }
        }
        public void GoUp(int desiredfloor)
        {
            if (DoorIsOpen == false)
            {
                while (desiredfloor > this.CurrentLevel && this.CurrentLevel < NumberOfLevels)
                {
                    this.CurrentLevel++;
                }
                


            }

        }
        public void GoDown(int desiredfloor)
        {

            if (DoorIsOpen == false)
            {
                while (desiredfloor < this.CurrentLevel && this.CurrentLevel > 1)
                {
                    this.CurrentLevel--;
                }
                
            }
         
        }
    }
}

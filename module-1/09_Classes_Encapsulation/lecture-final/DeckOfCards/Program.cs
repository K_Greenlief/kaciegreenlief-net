﻿using DeckOfCards.Classes;
using System;

namespace DeckOfCards
{
    class Program
    {
        static void Main(string[] args)
        {
            Card firstCard = new Card("Clubs",33);
            Console.WriteLine($"Is the card face up? {firstCard.isFaceUp}");
            if(firstCard.isFaceUp)
            {
                Console.WriteLine($"The card is the {firstCard.FaceValue} of {firstCard.Suit}");
            } else
            {
                Console.WriteLine("Hey, no peeking");
            }
            firstCard.Flip();
            Console.WriteLine($"The card is the {firstCard.FaceValue} of {firstCard.Suit} which is {firstCard.Color}");
            Console.WriteLine($"Is the card face up now? {firstCard.isFaceUp}");
            Card secondCard = new Card("Hearts", 8, true);
            if (secondCard.isFaceUp)
            {
                Console.WriteLine($"The card is the {secondCard.FaceValue} of {secondCard.Suit}");
            }
            else
            {
                Console.WriteLine("Hey, no peeking");
            }
            secondCard.Flip(true);
            secondCard.Flip(true);

            Deck ourDeck = new Deck();
            for(int i = 0; i < ourDeck.NumberOfCardsInDeck; i++)
            {
                Card dealtCard = ourDeck.Draw();
                Console.WriteLine($"The card is the {dealtCard.FaceValue} of {dealtCard.Suit}");
            }

            ourDeck = new Deck();
            ourDeck.Shuffle();
            for(int i = 0; i < ourDeck.NumberOfCardsInDeck; i++)
            {
                Card dealtCard = ourDeck.Draw();
                Console.WriteLine($"The card is the {dealtCard.FaceValue} of {dealtCard.Suit}");
            }
        }
    }
}

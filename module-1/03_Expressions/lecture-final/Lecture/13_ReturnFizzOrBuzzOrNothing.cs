﻿namespace Lecture
{
    public partial class LectureExample
    {
        /*
         13. Write an if/else statement that returns
            "Fizz" if the parameter is 3,
            "Buzz" if the parameter is 5
            and an empty string "" for anything else.
            TOPIC: Conditional Logic
         */
        public string ReturnFizzOrBuzzOrNothing(int number)
        {
            // create an output variable
            string output = "";

            // check if the number is 3
            if(number == 3)
            {
                // if it is return Fizz
                output = "Fizz";
            }
            // then check if the number is 5
            else if (number == 5)
            {
                // if it is return Buzz
                output = "Buzz";
            }
            // if it's neither return empty string
            return output;
        }
    }
}

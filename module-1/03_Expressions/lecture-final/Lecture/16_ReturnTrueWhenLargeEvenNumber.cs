﻿namespace Lecture
{
    public partial class LectureExample
    {
        /*
        16. Return "Big Even Number" when number is even, larger than 100, and a multiple of 5.
            Return "Big Number" if the number is just larger than 100.
            Return empty string for everything else.
            TOPIC: Complex Expression
        */
        public string ReturnBigEvenNumber(int number)
        {
            // Declare output variable
            string output = "";
            // which is the most specific (smallest amount of numbers)
            // when number is even, larger than 100, and a multiple of 5
            if (number % 2 == 0 && number > 100 && number % 5 == 0)
            {
                // if that's that case, ouput Big Even Number
                output = "Big Even Number";
            }
            // if not check if over 100
            else if (number > 100)
            {
                output = "Big Number";
            }
            return output;
        }
    }
}

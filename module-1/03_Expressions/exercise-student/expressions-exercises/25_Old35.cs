﻿namespace Exercises
{
    public partial class Exercises
    {
        /*
         Return true if the given non-negative number is a multiple of 3 or 5, but not both.
        
         */
        public bool Old35(int n)
        {
            if (n % 3 == 0 && n % 5 == 0)
            {
                return false;
            }
            else if (n%3 == 0 || n%5 == 0)
            {
                return true;
            }
            return false;
        }
    }
}

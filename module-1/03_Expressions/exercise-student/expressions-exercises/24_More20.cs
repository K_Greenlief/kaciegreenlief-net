﻿namespace Exercises
{
    public partial class Exercises
    {
        /*
         Return true if the given non-negative number is 1 or 2 more than a multiple of 20.
  
         */
        public bool More20(int n)
        {
            if (n%20 == 2 || n%20 == 1)
            {
                return true;
            }
            return false;
        }
    }
}

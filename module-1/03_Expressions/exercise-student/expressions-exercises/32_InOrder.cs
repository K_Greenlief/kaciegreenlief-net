﻿namespace Exercises
{
    public partial class Exercises
    {
        /*
         Given three ints, a b c, return true if b is greater than a, and c is greater than b. However, with
         the exception that if "bOk" is true, b does not need to be greater than a.
      
         */
        public bool InOrder(int a, int b, int c, bool bOk)
        {
            if (bOk && c>b)
            {
                return true;
            }
            if (b>a && c>b)
            {
                return true;

            }
            return false;
        }
    }
}

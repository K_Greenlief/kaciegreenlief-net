﻿namespace Exercises
{
    public partial class Exercises
    {
        /*
         Given 3 int values, a b c, return their sum. However, if one of the values is 13 then it does not
         count towards the sum and values to its immediate right do not count. So for example, if b is 13, then both
         b and c do not count.

        if a is 13, return 0
        if a and b are 13, return 0
        if a b and c are 12 return 0

        if b is 13, return a
        if c is 13, return a and b


         */
        public int LuckySum(int a, int b, int c)
        {
            int sum = 0;
            if (a != 13)
            {
                sum = sum + a;
            }
            if (a!= 13 && b != 13)
            {
                sum = sum + b;
            }
            if (c != 13 && b !=13)
            {
                sum = sum + c;
            }
            return sum;
        }
    }
}

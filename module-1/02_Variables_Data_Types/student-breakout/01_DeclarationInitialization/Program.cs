﻿using System;

namespace DeclarationInitialization
{
    class Program
    {
        static void Main(string[] args)
        {
            /* VARIABLES & DATA TYPES */

            /*
		    1. Create a variable to hold an int and call it numberOfExercises.
			Then set it to 26.
		    */
            int numberOfExercises;
            numberOfExercises = 26;
            Console.WriteLine(numberOfExercises);

            /*
            2. Create a variable to hold a double and call it half.
                Set it to 0.5.
            */

            double half = 0.5;

            Console.WriteLine(half);

            /*
            3. Create a variable to hold a string and call it name.
                Set it to "TechElevator".
            */
            string name = "TechElevator";
            Console.WriteLine(name);

            /*
            4. Create a variable called seasonsOfFirefly and set it to 1.
            */
            int seasonsOfFirefly = 1;
            Console.WriteLine(seasonsOfFirefly);

            /*
            5. Create a variable called myFavoriteLanguage and set it to "C#".
            */
            string myFavoriteLanguage = "C#";
            Console.WriteLine(myFavoriteLanguage);

            /*
            6. Create a variable called pi and set it to 3.1416.
            */
            decimal pi = 3.1416M;
            Console.WriteLine(pi);

            /*
            7. Create and set a variable that holds your name. Write it out to the console.
            */

            string yourName = "Kacie and Stephen";
            Console.WriteLine(yourName);

            /*
            8. Create and set a variable that holds the number of buttons on your mouse. Write it out to the console.
            */

            int numberOfMouseButtons = 5;
            Console.WriteLine(numberOfMouseButtons);

            /*
            9. Create and set a variable that holds the percentage of battery left on
            your phone. Write it out to the console.
            */

            int percentBatteryCharge = 85;
            Console.WriteLine(percentBatteryCharge);
        }
    }
}

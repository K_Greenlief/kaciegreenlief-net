﻿using System;

namespace Expressions
{
    class Program
    {
        static void Main(string[] args)
        {
            /* EXPRESSIONS */

            /*
            10. Create an int variable that holds the difference between 121 and 27.
            */
            int difference;
            difference = 121 - 27;
            Console.WriteLine(difference);
            /*
            11. Create a double that holds the addition of 12.3 and 32.1.
            */
            double addition = 12.3 + 32.1;
            Console.WriteLine(addition);
            /*
            12. Create a string that holds your full name.
            */
            string myName = "Kacie Craig";
            Console.WriteLine(myName);
            /*
            13. Create a string that holds the word "Hello, " concatenated onto your
            name from above.
            */
            string greeting = " Hello";
            Console.WriteLine(myName + greeting);
            /*
            14. Add a " Esquire" onto the end of your full name and save it back to
            the same variable.
            */
            string title = myName + " Esquire";
            Console.WriteLine(title);
            /*
            15. Create a variable to hold "Saw" and add a 2 onto the end of it.
            */
            string movie = "Saw";
            int number = 2;
            Console.Write(movie + number);

            /*
            16. Add a 0 onto the end of the variable from exercise 16.
            */
            Console.WriteLine(movie + number + 0);
            /*
            17. What is 4.4 divided by 2.2?
            */
            decimal quotient = 4.4M / 2.2M;
            Console.WriteLine(quotient);
            /*
            18. What is 5.4 divided by 2?
          
            */
            double nextQuotient = 5.4 / 2;
            Console.WriteLine(nextQuotient);
        }
    }
}

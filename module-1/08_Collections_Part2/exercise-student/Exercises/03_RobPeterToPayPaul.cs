﻿using System.Collections.Generic;

namespace Exercises
{
    public partial class Exercises
    {
        /*
         * Modify and return the given Dictionary as follows: if "Peter" has more than 0 money, transfer half of it to "Paul",
         * but only if Paul has less than $10s.
         *
         * monetary amounts specified in cents: penny=1, nickel=5, ... $1=100, ... $10=1000, ...
         *
         * RobPeterToPayPaul({"Peter": 2000, "Paul": 99}) → {"Peter": 1000, "Paul": 1099}
         * RobPeterToPayPaul({"Peter": 2000, "Paul": 30000}) → {"Peter": 2000, "Paul": 30000}
         *  // Paul & peter are keys and money is value
         */
        public Dictionary<string, int> RobPeterToPayPaul(Dictionary<string, int> peterPaul)
        {
            int petersCash = peterPaul["Peter"];
            int paulsCash = peterPaul["Paul"];

            if (petersCash > 0 && paulsCash < 1000)
            {
                petersCash = (peterPaul["Peter"] / 2);
                paulsCash = peterPaul["Paul"] + petersCash;

                peterPaul["Peter"] = petersCash;
                peterPaul["Paul"] = paulsCash;
                return peterPaul;

            }
            else
            {
                return peterPaul;
            }
        }
    }
}

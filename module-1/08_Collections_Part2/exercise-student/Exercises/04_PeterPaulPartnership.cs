﻿using System.Collections.Generic;

namespace Exercises
{
    public partial class Exercises
    {
        /*
         * Modify and return the given Dictionary as follows:
         * if "Peter" has $50 or more, AND "Paul" has $100 or more,
         *    --  if peter >= 5000 && paul >= 10000
         *     
         * then create a new "PeterPaulPartnership" worth a combined contribution of a quarter of each partner's
         * current worth.
         *     -- PeterPaulPartnership = 0.25 * peter PLUS 0.25 * paul
         * PeterPaulPartnership({"Peter": 50000, "Paul": 100000}) → {"Peter": 37500, "Paul": 75000, "PeterPaulPartnership": 37500}
         * PeterPaulPartnership({"Peter": 3333, "Paul": 1234567890}) → {"Peter": 3333, "Paul": 1234567890}
         *
         */
        public Dictionary<string, int> PeterPaulPartnership(Dictionary<string, int> peterPaul)
        {
            int petersTotal = peterPaul["Peter"];  
            int paulsTotal = peterPaul["Paul"];


            int petersLeftovers = peterPaul["Peter"] - (petersTotal/4);
            int paulsLeftovers = peterPaul["Paul"] - (paulsTotal/4);

            if (peterPaul["Peter"] >= 5000 && peterPaul["Paul"] >= 10000)
            {

                int prophetsShare = petersTotal/4 + paulsTotal/4;
               
                peterPaul["PeterPaulPartnership"] = prophetsShare;

                peterPaul["Peter"] = petersLeftovers;
                peterPaul["Paul"] = paulsLeftovers;

            }
            return peterPaul;
        }
    }
}

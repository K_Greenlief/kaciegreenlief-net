﻿using System.Collections.Generic;

namespace Exercises
{
    public partial class Exercises
    {
        /* * IntCount([1, 99, 63, 1, 55, 77, 63, 99, 63, 44]) → {1: 2, 44: 1, 55: 1, 63: 3, 77: 1, 99:2}
         * IntCount([107, 33, 107, 33, 33, 33, 106, 107]) → {33: 4, 106: 1, 107: 3}
         * IntCount([]) → {}
         * Given an array of int values, return a Dictionary<int, int> with a key for each int, with the value the
         * number of times that int appears in the array.
         *
         *
         *      kay for each int = so gotta loop
         *      make counter -- if seen, up count. if not seen, don't up count
         */
        public Dictionary<int, int> IntCount(int[] ints)
        {
            Dictionary<int, int> IntCountDic = new Dictionary<int, int>();
            foreach (int key in ints)
            {
                if (IntCountDic.ContainsKey(key))
                {
                    IntCountDic[key] += 1;
                }
                else
                {
                    IntCountDic[key] = 1;
                }
            }
            return IntCountDic;
        }
    }
}

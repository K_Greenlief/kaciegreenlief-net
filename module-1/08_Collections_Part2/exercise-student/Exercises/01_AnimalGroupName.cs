﻿using System.Collections.Generic;

namespace Exercises
{
    public partial class Exercises
    {
        /* If the name of the animal is not found, null, or empty, return "unknown".
         * 
         * Given the name of an animal, return the name of a group of that animal
         * (e.g. "Elephant" -> "Herd", "Rhino" - "Crash").
         *
         * The animal name should be case insensitive so "elephant", "Elephant", and
         * "ELEPHANT" should all return "herd".
         *

         *
         * AnimalGroupName("giraffe") → "Tower"
         * AnimalGroupName("") -> "unknown"
         * AnimalGroupName("walrus") -> "unknown"
         * AnimalGroupName("Rhino") -> "Crash"
         * AnimalGroupName("rhino") -> "Crash"
         * AnimalGroupName("elephants") -> "unknown"
         *
         */
        public string AnimalGroupName(string animalName)
        {
            Dictionary<string, string> animalDictionary = new Dictionary<string, string>();

            animalDictionary["rhino"]= "Crash";
            animalDictionary["giraffe"] = "Tower";
            animalDictionary["elephant"] = "Herd";
            animalDictionary["lion"] = "Pride";
            animalDictionary["crow"] = "Murder";
            animalDictionary["pigeon"] = "Kit";
            animalDictionary["flamingo"] = "Pat";
            animalDictionary["deer"] = "Herd";
            animalDictionary["crocodile"] = "Float";


            if (animalName == null)
            {
                return "unknown";
            }
            else if (animalDictionary.ContainsKey(animalName.ToLower()))
            {
                return animalDictionary[animalName.ToLower()];
            }

            return "unknown";
        }
    }
}

﻿using System.Collections.Generic;

namespace Exercises
{
    public partial class Exercises
    {
        /*
         * Given two Dictionaries, Dictionary<string, int>, merge the two into a new Dictionary, Dictionary<string, int> where keys in Dictionary2,
         * and their int values, are added to the int values of matching keys in Dictionary1. Return the new Dictionary.
         *
         * Unmatched keys and their int values in Dictionary2 are simply added to Dictionary1.
         *
         * ConsolidateInventory({"SKU1": 100, "SKU2": 53, "SKU3": 44} {"SKU2":11, "SKU4": 5})
         * 	 → {"SKU1": 100, "SKU2": 64, "SKU3": 44, "SKU4": 5}
         * 	 
         * 	 compare value of dic 1 and 2 to see if they match
         * 	 if yes, add to dic1
         * 	 
         * 	if no, insert dic1
         *
         */
        public Dictionary<string, int> ConsolidateInventory(Dictionary<string, int> mainWarehouse,
                                                            Dictionary<string, int> remoteWarehouse)

            

        {
            foreach(string keys in remoteWarehouse.Keys)
            {
                if (mainWarehouse.ContainsKey(keys))
                {
                    mainWarehouse[keys] += remoteWarehouse[keys];
                }
                else
                {
                    mainWarehouse[keys] = remoteWarehouse[keys];
                }
            }
            return mainWarehouse;
        }
    }
}

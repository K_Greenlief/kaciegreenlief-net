﻿using System;
using System.Collections.Generic;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Dictionary<string, int> test = new Dictionary<string, int>();
            test.Add("a", 1);
            test.Add("b", 11);
            test.Add("c", 111);

            foreach (KeyValuePair<string, int> item in test)
            {
                Console.WriteLine(item);
            }
            

        }
    }
}

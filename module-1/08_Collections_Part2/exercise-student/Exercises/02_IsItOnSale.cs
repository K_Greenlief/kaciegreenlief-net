﻿using System.Collections.Generic;

namespace Exercises
{
    public partial class Exercises
    {
        /*
         * * The item number should be case insensitive so "kitchen4001", "Kitchen4001", and "KITCHEN4001"
         * should all return 0.20.
         * If the item number is empty or null, return 0.00.
         * Given an string item number (a.k.a. SKU), return the discount percentage if the item is on sale.
         * If the item is not on sale, return 0.00.
         *
         * The item number should be case insensitive so "kitchen4001", "Kitchen4001", and "KITCHEN4001"
         * should all return 0.20.
         *
         * IsItOnSale("kitchen4001") → 0.20
         * IsItOnSale("") → 0.00
         * IsItOnSale("GARAGE1070") → 0.15
         * IsItOnSale("dungeon9999") → 0.00
         *
         */
        public double IsItOnSale(string itemNumber)
        {
            Dictionary<string, double> pricesDictionary = new Dictionary<string, double>();
            pricesDictionary["KITCHEN4001"] = 0.20;
            pricesDictionary["GARAGE1070"] = 0.15;
            pricesDictionary["LIVINGROOM"] = 0.10;
            pricesDictionary["KITCHEN6073"] = 0.40; 
            pricesDictionary["BEDROOM3434"] = 0.60;
            pricesDictionary["BATH0073"] = 0.15;
            
            if(itemNumber == null)
            {
                return 0.00;
            }
            else if (pricesDictionary.ContainsKey(itemNumber.ToUpper()))
            {
                return pricesDictionary[itemNumber.ToUpper()];
            }


                return 0.00;
        }
    }
}

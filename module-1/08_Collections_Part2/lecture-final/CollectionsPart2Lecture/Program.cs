﻿using System;
using System.Collections.Generic;

namespace CollectionsPart2Lecture
{
    public class CollectionsPart2Lecture
    {
        static void Main(string[] args)
        {
            Console.WriteLine("####################");
            Console.WriteLine("       DICTIONARIES");
            Console.WriteLine("####################");
            Console.WriteLine();

            // declare a dictionary
            Dictionary<string, string> nameToZipCode;
            // initialize the dictionary
            nameToZipCode = new Dictionary<string, string>();
            // add items to my dictionary
            // this syntax will create the key if it doesn't exist
            // if the key does exist, it will overwrite the value
            nameToZipCode["Tom"] = "15229";
            nameToZipCode["Henry"] = "15108-1452";
            nameToZipCode["Mimi"] = "15237";
            nameToZipCode["Henry"] = "15233";

            // a different adding syntax
            // if the key already exists, the program will crash
            // let's protect ourselves and check for the key
            if (!nameToZipCode.ContainsKey("Eddie"))
            {
                nameToZipCode.Add("Eddie", "15229");
            }
            if (!nameToZipCode.ContainsKey("Henry"))
            {
                nameToZipCode.Add("Henry", "15489");
            }

            // get information out of our dictionary
            Console.WriteLine("Tom lives in zip code " + nameToZipCode["Tom"]);
            Console.WriteLine("Mimi lives in zip code " + nameToZipCode["Mimi"]);
            Console.WriteLine("Henry lives in zip code " + nameToZipCode["Henry"]);
            Console.WriteLine("Eddie lives in zip code " + nameToZipCode["Eddie"]);
            Console.WriteLine();
            Console.WriteLine("Using the keys collection");
            // looping through our dictionary to get keys method 1
            foreach (string name in nameToZipCode.Keys)
            {
                Console.WriteLine($"{name} lives in the zip code {nameToZipCode[name]}");
            }
            Console.WriteLine();
            Console.WriteLine("Using the values collection");

            // looping through our dictionary method 2
            // no access to the key
            foreach (string zip in nameToZipCode.Values)
            {
                Console.WriteLine($"Here's a zip code {zip}");
            }
            Console.WriteLine();
            Console.WriteLine("Using the key value pair collection");
            // looping through our dictionary method 3 (my favorite)
            foreach (KeyValuePair<string, string> kvp in nameToZipCode)
            {
                Console.WriteLine($"{kvp.Key} lives in {kvp.Value}");
            }
            Console.WriteLine();

            Dictionary<string, int> numbersToAdd = new Dictionary<string, int>();
            numbersToAdd["first"] = 42;
            numbersToAdd["next"] = 8;
            numbersToAdd["final"] = 10;
            // let's calculate a sum
            int sum = 0;
            foreach (int number in numbersToAdd.Values)
            {
                sum += number;
            }
            Console.WriteLine("The sum is " + sum);
            // let's calculate the sum with commentary.
            sum = 0;
            foreach (KeyValuePair<string, int> kvp in numbersToAdd)
            {
                Console.WriteLine($"The current sum is {sum}");
                Console.WriteLine($"We are going to add the {kvp.Key} number to the sum");
                sum += kvp.Value;
                Console.WriteLine($"The sum is now {sum}");
            }
            bool is15229 = nameToZipCode.ContainsValue("15229");
            nameToZipCode.Remove("Tom");
            nameToZipCode.Clear();

            Console.WriteLine("##########################");
            Console.WriteLine("HASHSET");
            Console.WriteLine("##########################");
            List<string> colors = new List<string>();
            colors.Add("Blue");
            colors.Add("Green");
            colors.Add("Grey");
            colors.Add("Yellow");
            colors.Add("Green");
            foreach (string color in colors)
            {
                Console.WriteLine($"From list {color}");
            }
            HashSet<string> colorsHash = new HashSet<string>();
            colorsHash.Add("Blue");
            colorsHash.Add("Green");
            colorsHash.Add("Grey");
            colorsHash.Add("Yellow");
            colorsHash.Add("Green");
            foreach (string color in colorsHash)
            {
                Console.WriteLine($"From hashset {color}");
            }

            // Let's find the duplicate colors in the colors list
            List<string> dupColors = new List<string>();
            HashSet<string> colorTemp = new HashSet<string>();
            foreach (string color in colors)
            {
                if (!colorTemp.Add(color))
                {
                    dupColors.Add(color);
                }
            }

        }
    }
}

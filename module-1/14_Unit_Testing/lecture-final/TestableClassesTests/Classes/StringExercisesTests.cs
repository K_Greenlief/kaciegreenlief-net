﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TestableClasses.Classes;

namespace TestableClassesTests.Classes
{
    [TestClass]
    public class StringExercisesTests
    {
        [TestMethod]
        public void MakeAbbaHappyPathTest()
        {
            // makeAbba("Hi", "Bye") → "HiByeByeHi"

            // Arrange -- my ingredients
            StringExercises stringExercises = new StringExercises();
            string expectOutput = "HiByeByeHi";
            string string1 = "Hi";
            string string2 = "Bye";
            string actual = "";

            // Act
            actual = stringExercises.MakeAbba(string1, string2);

            // Assert
            Assert.AreEqual(expectOutput, actual, "Hmmm, looks like we aren't doing abba");

            // makeAbba("Yo", "Alice") → "YoAliceAliceYo"
            string1 = "Yo";
            string2 = "Alice";
            expectOutput = "YoAliceAliceYo";
            // Act
            actual = stringExercises.MakeAbba(string1, string2);
            // Assert
            Assert.AreEqual(expectOutput, actual,"Hmmm, looks like we aren't doing abba");
        }

        [TestMethod]
        public void MakeAbbaEmptyStringTest()
        {
            // Arrange
            StringExercises stringExercises = new StringExercises();
            string thing1 = "";
            string thing2 = "";
            string actual = "";

            // Act
            actual = stringExercises.MakeAbba(thing1, thing2);

            // Assert
            Assert.IsTrue(string.IsNullOrEmpty(actual));
        }

        [TestMethod]
        public void MakeAbbaNullTest()
        {
            // Arrange
            StringExercises stringExercises = new StringExercises();
            string actual = "";

            // Act
            actual = stringExercises.MakeAbba(null, null);
            // Assert
            Assert.AreEqual("", actual);
            // Act
            actual = stringExercises.MakeAbba("Hi", null);
            // Assert
            Assert.AreEqual("", actual);
        }

        [TestMethod]
        public void FirstTwoHappyPath()
        {
            // Arrange
            StringExercises stringExercises = new StringExercises();
            string input = "Hello";
            string expected = "He";
            string actual = "";
            // Act
            actual = stringExercises.FirstTwo(input);
            // Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void FirstTwoNull()
        {
            // Arrange
            StringExercises stringExercises = new StringExercises();
            string input = null;
            string expected = "";
            string actual = "";
            // Act
            actual = stringExercises.FirstTwo(input);
            // Assert
            Assert.IsNull(actual);
        }
    }
}
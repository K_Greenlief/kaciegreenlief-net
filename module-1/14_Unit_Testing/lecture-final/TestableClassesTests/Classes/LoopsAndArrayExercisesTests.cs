﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TestableClasses.Classes;

namespace TestableClassesTests.Classes
{
    [TestClass]
    public class LoopsAndArrayExercisesTests
    {
        [TestMethod]
        public void MiddleWayHappyPath()
        {
            // Arrange
            LoopsAndArrayExercises loopsAndArrayExercises = new LoopsAndArrayExercises();
            int[] one = { 1, 2, 3 };
            int[] two = { 4, 5, 6 };
            int[] expectedResult = { 2,5 };
            // Act
            int[] actualResult = loopsAndArrayExercises.MiddleWay(one, two);
            // Assert
            // Can't use Assert class
            CollectionAssert.AreEqual(expectedResult, actualResult);
        }
    }
}

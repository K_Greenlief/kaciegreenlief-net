﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercises.Tests
{
    [TestClass]
    public class CigarPartyTests
    {
        [TestMethod]
        public void CigarPartyHappyTests()
        {
            //arrange -- cigars in range, is weekend (make sure it is inclusive)
            CigarParty cigarParty = new CigarParty();
            bool expectedResult = true;
            int cigarInput = 40;
            bool isItAWeekend = true;

            //act
            bool actual = cigarParty.HaveParty(cigarInput, isItAWeekend);

            //assert
            Assert.AreEqual(expectedResult, actual);

            //in range, not weekend
            expectedResult = true;
            cigarInput = 40;
            isItAWeekend = false;

            //act
            actual = cigarParty.HaveParty(cigarInput, isItAWeekend);

            //assert
            Assert.AreEqual(expectedResult, actual);
        }

        [TestMethod]
        public void CigarPartyAboveRangeTests()
        {
            //cigars big range, is weekend
            CigarParty cigarParty = new CigarParty();
            bool expectedResult = true;
            int cigarInput = 150;
            bool isItAWeekend = true;

            //act
            bool actual = cigarParty.HaveParty(cigarInput, isItAWeekend);

            //assert
            Assert.AreEqual(expectedResult, actual);
            
            // above range, no weekend 
            expectedResult = false;
            cigarInput = 65;
            isItAWeekend = false;

            //act
            actual = cigarParty.HaveParty(cigarInput, isItAWeekend);

            //assert
            Assert.AreEqual(expectedResult, actual);
        }
        [TestMethod]
        public void CigarPartyBelowRangeTests()
        {
            // cigars small range, is weekend
            CigarParty cigarParty = new CigarParty();
            bool expectedResult = false;
            int cigarInput = 30;
            bool isItAWeekend = true;

            //act
            bool actual = cigarParty.HaveParty(cigarInput, isItAWeekend);

            //assert
            Assert.AreEqual(expectedResult, actual);

            // small range, is not weekend

            expectedResult = false;
            cigarInput = 30;
            isItAWeekend = false;

            //act
            actual = cigarParty.HaveParty(cigarInput, isItAWeekend);

            //assert
            Assert.AreEqual(expectedResult, actual);
        }

    }
}

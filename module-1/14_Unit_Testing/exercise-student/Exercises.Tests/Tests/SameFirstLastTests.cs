﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Tests
{
    [TestClass]
    public class SameFirstLastTests
    {
        [TestMethod]
        public void SameFirstLastHappyTests()
        {
            SameFirstLast sameFirstLast = new SameFirstLast();
            int[] givenArray = new int[] { 1, 2, 3, 1 };
            bool expectedBool = true;

            bool actual = sameFirstLast.IsItTheSame(givenArray);

            Assert.AreEqual(actual, expectedBool);

            //

            int[] givenArray2 = new int[] { 1, 2, 3 };
            expectedBool = false;

            actual = sameFirstLast.IsItTheSame(givenArray2);

            Assert.AreEqual(actual, expectedBool);
        }

        [TestMethod]
        public void SameFirstLastExceptionTests()
        {
            SameFirstLast sameFirstLast = new SameFirstLast();
            int[] givenArray = new int[] { 2 };
            bool expectedBool = true;

            bool actual = sameFirstLast.IsItTheSame(givenArray);

            Assert.AreEqual(actual, expectedBool);

            //

            int[] givenArray2 = new int[] { 2, 4 };
            expectedBool = false;

            actual = sameFirstLast.IsItTheSame(givenArray2);

            Assert.AreEqual(actual, expectedBool);
        }
        [TestMethod]
        public void SameFirstLastEmptyTests()
        {
            SameFirstLast sameFirstLast = new SameFirstLast();
            int[] givenArray = new int[] {};
            bool expectedBool = false;

            bool actual = sameFirstLast.IsItTheSame(givenArray);

            Assert.AreEqual(actual, expectedBool);
        }
        [TestMethod]
        public void SameFirstLastNullTests()
        {
            SameFirstLast sameFirstLast = new SameFirstLast();
            int[] givenArray = null;
            bool expectedBool = false;

            bool actual = sameFirstLast.IsItTheSame(givenArray);

            Assert.AreEqual(actual, expectedBool);
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Exercises;

namespace Exercises.Tests
{
    [TestClass]
    public class AnimalGroupNameTests
    { /*    
         * Giraffe -> Tower
         * Elephant -> Herd
         * GetHerd("elephants") -> "unknown"*/
        [TestMethod]
        public void AnimalGroupHappyPath()
        {
            //arrange
            AnimalGroupName animalGroupName = new AnimalGroupName();
            string expectedOutput = "Tower";
            string testInput = "giraffe";
            string actual = "";

            //act
            actual = animalGroupName.GetHerd(testInput);

            //assert
            Assert.AreEqual(actual, expectedOutput);

            //

            expectedOutput = "Herd";
            testInput = "elephant";

            actual = animalGroupName.GetHerd(testInput);

            Assert.AreEqual(actual, expectedOutput);
        }
        [TestMethod]
        public void AnimalGroupEmptyTest()
        {
            //arrange
            AnimalGroupName animalGroupName = new AnimalGroupName();
            string expectedOutput = "unknown";
            string testInput = "";
            string actual = "";

            //act
            actual = animalGroupName.GetHerd(testInput);

            //assert
            Assert.AreEqual(expectedOutput, actual);
        }
        [TestMethod]
        public void AnimalGroupNullTest()
        {
            //arrange
            AnimalGroupName animalGroupName = new AnimalGroupName();
            string expectedOutput = "unknown";
            string testInput = null;
            string actual = "";

            //act
            actual = animalGroupName.GetHerd(testInput);

            //assert
            Assert.AreEqual(expectedOutput, actual);
        }
        [TestMethod]
        public void AnimalGroupCaseTest()
        {
            //arrange
            AnimalGroupName animalGroupName = new AnimalGroupName();
            string expectedOutput = "Herd";
            string testInput = "ElEpHaNt";

            //act
            string actual = animalGroupName.GetHerd(testInput);

            //assert
            Assert.AreEqual(expectedOutput, actual);
        }
        [TestMethod]
        public void AnimalGroupWeirdKeyTest()
        {
            //arrange
            AnimalGroupName animalGroupName = new AnimalGroupName();
            string expectedOutput = "unknown";
            string testInput = "koala";

            //act
            string actual = animalGroupName.GetHerd(testInput);

            //assert
            Assert.AreEqual(expectedOutput, actual);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Tests
{
    [TestClass]
    //  frontTimes("Chocolate", 2) → "ChoCho"
    //  frontTimes("Chocolate", 3) → "ChoChoCho"
    //  frontTimes("Abc", 3) → "AbcAbcAbc"
    public class FrontTimesTests
    {
        [TestMethod]
        public void FrontTimesHappyTest()
        {
            // Arrange
            FrontTimes frontTimes = new FrontTimes();
            string expectedString = "ChoChoCho";
            string originalString = "Chocolate";
            int timesToRepeat = 3;

            // Act
            string actual = frontTimes.GenerateString(originalString, timesToRepeat);
            // Assert
            Assert.AreEqual(expectedString, actual);
        }
        [TestMethod]
        public void FrontTimesShortStringTest()
        {
            // Arrange
            FrontTimes frontTimes = new FrontTimes();
            string expectedString = "MyMyMyMy";
            string originalString = "My";
            int timesToRepeat = 4;

            // Act
            string actual = frontTimes.GenerateString(originalString, timesToRepeat);
            // Assert
            Assert.AreEqual(expectedString, actual);
        }
        [TestMethod]
        public void FrontTimesNullorEmptyStringTest()
        {
            // Arrange
            FrontTimes frontTimes = new FrontTimes();
            string expectedString = "";
            string originalString = "";
            int timesToRepeat = 4;

            // Act
            string actual = frontTimes.GenerateString(originalString, timesToRepeat);
            // Assert
            Assert.AreEqual(expectedString, actual);
            //act
            actual = frontTimes.GenerateString(null, timesToRepeat);
            // Assert
            Assert.AreEqual(expectedString, actual);

        }
    }
}

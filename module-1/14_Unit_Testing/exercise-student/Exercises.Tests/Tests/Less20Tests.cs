﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Tests
{
    [TestClass]
    public class Less20Tests
    {
        [DataTestMethod]
        [DataRow(18, true)]
        [DataRow(19, true)]
        [DataRow(20, false)]
        [DataRow(38, true)]
        [DataRow(39, true)]
        [DataRow(40, false)]
        [DataRow(17, false)]
        public void Less20DataTests(int num, bool isMultiple)
        {
            //arrange
            Less20 less20 = new Less20();
            int inputNum = num;
            //act
            bool result = less20.IsLessThanMultipleOf20(inputNum);
            //assert
            Assert.AreEqual(result, isMultiple);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Tests
{
    [TestClass]
    public class NonStartTest
    {
        [TestMethod]

        public void NonStartHappyPathTests()
        {
            NonStart nonStart = new NonStart();
            string string1 = "Hello";
            string string2 = "There";
            string expectedAnswer = "ellohere";

            string userAnswer = nonStart.GetPartialString(string1, string2);

            Assert.AreEqual(expectedAnswer, userAnswer);

            //

            string1 = "Free";
            string2 = "Icecream";
            expectedAnswer = "reececream";

            userAnswer = nonStart.GetPartialString(string1, string2);

            Assert.AreEqual(expectedAnswer, userAnswer);
        }
        [TestMethod]
        public void NonStartShortStringTests()
        {
            NonStart nonStart = new NonStart();
            string string1 = "H";
            string string2 = "i";
            string expectedAnswer = "";

            string userAnswer = nonStart.GetPartialString(string1, string2);

            Assert.AreEqual(expectedAnswer, userAnswer);
        }
        [TestMethod]
        public void NonStartNullTests()
        {
            //The strings will be at least length 1.
            NonStart nonStart = new NonStart();
            string string1 = "";
            string string2 = "";

            string userAnswer = nonStart.GetPartialString(string1, string2);

            Assert.IsTrue(string.IsNullOrEmpty(userAnswer));
        }



    }
}

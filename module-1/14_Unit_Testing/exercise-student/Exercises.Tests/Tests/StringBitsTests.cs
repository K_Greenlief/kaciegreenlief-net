﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Tests
{
    [TestClass]
    public class StringBitsTests
    {

        [TestMethod]
        public void StringBitsHappyTests()
        {
            StringBits stringBits = new StringBits();
            string originalString = "KacieCraig";
            string expectedAnswer = "Kceri";

            string userActual = stringBits.GetBits(originalString);

            Assert.AreEqual(userActual, expectedAnswer);

            //

            originalString = "Hello";
            expectedAnswer = "Hlo";

            userActual = stringBits.GetBits(originalString);

            Assert.AreEqual(userActual, expectedAnswer);
        }
        [TestMethod]
        public void StringBitsNullorEmptyTest()
        {

            StringBits stringBits = new StringBits();
            string originalString = "";

            string userActual = stringBits.GetBits(originalString);

            Assert.IsTrue(string.IsNullOrEmpty(userActual));
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercises.Tests
{
    [TestClass]
    public class DateFashionTests
    {
        [TestMethod]
        public void DateFashionHappyTests()
        {
            //both look terrible
            DateFashion dateFashion = new DateFashion();
            int expectedToGetATable = 0;
            int yourLooks = 2;
            int yourDate = 3;

            //act
            int actual = dateFashion.GetATable(yourLooks, yourDate);

            //assert
            Assert.AreEqual(expectedToGetATable, actual);

            //both look fine, i guess
            expectedToGetATable = 1;
            yourLooks = 5;
            yourDate = 5;

            //act
            actual = dateFashion.GetATable(yourLooks, yourDate);

            //assert
            Assert.AreEqual(expectedToGetATable, actual);

            //both look fantabulous
            expectedToGetATable = 2;
            yourLooks = 8;
            yourDate = 8;

            //act
            actual = dateFashion.GetATable(yourLooks, yourDate);

            //assert
            Assert.AreEqual(expectedToGetATable, actual);

        }
        [TestMethod]
        public void DateFashionExceptionTests()
        {
            //one is a kardashian and the other is a hobo
            DateFashion dateFashion = new DateFashion();
            int expectedToGetATable = 0;
            int yourLooks = 10;
            int yourDate = 1;

            //act
            int actual = dateFashion.GetATable(yourLooks, yourDate);

            //assert
            Assert.AreEqual(expectedToGetATable, actual);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Tests
{
    [TestClass]
    public class Lucky13Tests
    {
        //GetLucky([0, 2, 4]) → true
        //GetLucky([1, 2, 3]) → false
        //GetLucky([1, 2, 4]) → false
        [TestMethod]
        public void Lucky13HappyPathTests()
        {
            Lucky13 lucky13 = new Lucky13();
            int[] luckyArray1 = new int[] {2, 5, 6, 9};
            bool expectedBool = true;

            bool actual = lucky13.GetLucky(luckyArray1);

            Assert.AreEqual(actual, expectedBool);

            //

            int[] luckyArray2 = new int[] { 4, 3, 5, 6 };
            expectedBool = false;

            actual = lucky13.GetLucky(luckyArray2);

            Assert.AreEqual(actual, expectedBool);
        }
        [TestMethod]
        public void Lucky13NotSoHappyPathTests()
        {
            Lucky13 lucky13 = new Lucky13();
            int[] luckyArray1 = new int[] { 2, 35, 6, 19, 467 };
            bool expectedBool = true;

            bool actual = lucky13.GetLucky(luckyArray1);

            Assert.AreEqual(actual, expectedBool);

            //

            int[] luckyArray2 = new int[] { 4, 8, 5, 1, 6 };
            expectedBool = false;

            actual = lucky13.GetLucky(luckyArray2);

            Assert.AreEqual(actual, expectedBool);
        }
    }
}

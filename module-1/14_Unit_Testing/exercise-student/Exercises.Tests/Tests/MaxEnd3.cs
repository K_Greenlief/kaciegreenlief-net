﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Tests
{
    [TestClass]
    public class MaxEnd3Tests
    {
        [TestMethod]

        public void MaxEnd3ArrayTests()
        {
            MaxEnd3 maxend3 = new MaxEnd3();
            int[] originalArray = new int[] { 2, 11, 42 };
            int[] expectedArray = new int[] { 42, 42, 42 };

            int[] userAnswer = maxend3.MakeArray(originalArray);

            CollectionAssert.AreEqual(userAnswer, expectedArray);

            //

            originalArray = new int[] { 12, 117, 4 };
            expectedArray = new int[] { 12, 12, 12 };

            userAnswer = maxend3.MakeArray(originalArray);

            CollectionAssert.AreEqual(userAnswer, expectedArray);

            //

            originalArray = new int[] { 44, 7, 17 };
            expectedArray = new int[] { 44, 44, 44 };

            userAnswer = maxend3.MakeArray(originalArray);

            CollectionAssert.AreEqual(userAnswer, expectedArray);
        }
        /// <summary>
        ///  Do I need this Null method, since the specs dictate that the length will always be three? 
        ///  It is a reference type, so I still wrote it
        /// </summary>
        [TestMethod]
        public void MaxEnd3NullTests()
        {
            MaxEnd3 maxend3 = new MaxEnd3();
            int[] expectedArray = {};

            int[] userAnswer = maxend3.MakeArray(null);

            CollectionAssert.AllItemsAreNotNull(userAnswer);
        }
       
    }
}

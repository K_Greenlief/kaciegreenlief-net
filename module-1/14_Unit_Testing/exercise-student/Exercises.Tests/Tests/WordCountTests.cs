﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises.Tests
{
    [TestClass]
    public class WordCountTests
    {
        /*  GetCount(["ba", "ba", "black", "sheep"]) → {"ba" : 2, "black": 1, "sheep": 1 }
          * GetCount(["a", "b", "a", "c", "b"]) → {"a": 2, "b": 2, "c": 1}
          * GetCount([]) → {}
          * GetCount(["c", "b", "a"]) → {"c": 1, "b": 1, "a": 1}*/
        [TestMethod]
        public void WordCountHappyTests()
        {
            WordCount wordCount = new WordCount();
            string[] givenArray = new string[] { "ba", "ba", "black", "sheep" };

           Dictionary<string, int> newDict = new Dictionary<string, int>()
            {
                {"ba", 2},
                {"black", 1},
                {"sheep", 1}
            };

            Dictionary<string, int> usersDict = wordCount.GetCount(givenArray); 

            CollectionAssert.AreEqual(usersDict, newDict);

            //
            string[] secondArray = new string[] { "a", "b", "a", "c", "b", "a" };
            Dictionary<string, int> newDict2 = new Dictionary<string, int>()
            {
                {"a", 3},
                {"b", 2},
                {"c", 1}
            };

            Dictionary<string, int> usersDict2 = wordCount.GetCount(secondArray);

            CollectionAssert.AreEqual(usersDict2, newDict2);

        }
        [TestMethod]
        public void WordCountEmptyTests()
        {
            WordCount wordCount = new WordCount();
            string[] givenArray = new string[] {};
            Dictionary<string, int> newDict = new Dictionary<string, int>(){};

            Dictionary<string, int> usersDict = wordCount.GetCount(givenArray);

            CollectionAssert.AreEqual(usersDict, newDict);

        }
        [TestMethod]
        public void WordCountNullTests()
        {
            WordCount wordCount = new WordCount();

            Dictionary<string, int> usersDict = wordCount.GetCount(null); 

            Assert.IsNotNull(usersDict);

        }
    }
}

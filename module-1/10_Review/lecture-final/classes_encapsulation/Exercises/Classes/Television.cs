﻿namespace Exercises.Classes
{
    public class Television
    {
        /// <summary>
        /// Tells me if the TV is on or off.
        /// </summary>
        public bool IsOn { get; private set; } 
        public int CurrentChannel { get; private set; } = 3;
        public int CurrentVolume { get; private set; }

        public Television()
        {
            // set the CurrentVolume to a default of 2
            CurrentVolume = 2;
        }
        /// <summary>
        /// Turns the TV off
        /// </summary>
        public void TurnOff()
        {
            IsOn = false;
        }
        /// <summary>
        /// Turns the TV on and sets the channel to 3 and the volume to 2
        /// </summary>
        public void TurnOn()
        {
            IsOn = true;
            CurrentChannel = 3;
            CurrentVolume = 2;
        }
        /// <summary>
        /// Changes the channel to the specified channel
        /// </summary>
        /// <param name="newChannel">The channel you want to watch</param>
        public void ChangeChannel(int newChannel)
        {
            if(IsOn)
            {
                if(newChannel >= 3 && newChannel <=18)
                {
                    CurrentChannel = newChannel;
                }
            }
        }
        /// <summary>
        /// Increases the Current Channel by one if the TV is on
        /// </summary>
        public void ChannelUp()
        {
            if(IsOn)
            {
                if(CurrentChannel == 18)
                {
                    CurrentChannel = 3;
                }
                else
                {
                    CurrentChannel++;
                }
            }
        }

        /// <summary>
        /// Decreases the Current Channel by one if the TV is on
        /// </summary>
        public void ChannelDown()
        {
            if(IsOn)
            {
                if(CurrentChannel == 3)
                {
                    CurrentChannel = 18;
                } else
                {
                    CurrentChannel--;
                }
            }
        }
        /// <summary>
        /// Increases volume to a maximum of 10
        /// </summary>
        public void RaiseVolume()
        {
            if(IsOn && CurrentVolume != 10)
            {
                CurrentVolume++;
            }
        }

        /// <summary>
        /// Decrease volume to a minimum of 0
        /// </summary>
        public void LowerVolume()
        {
            if(IsOn && CurrentVolume != 0)
            {
                CurrentVolume--;
            }
        }
    }
}

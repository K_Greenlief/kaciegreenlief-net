﻿using Exercises.Classes;
using System;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            Television tv = new Television();
            tv.TurnOn();
            tv.TurnOff();
            tv.ChangeChannel(8);
            Console.WriteLine("Hello World!");
        }
    }
}

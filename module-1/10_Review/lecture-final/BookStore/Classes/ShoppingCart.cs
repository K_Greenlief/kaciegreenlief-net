﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Classes
{
    public class ShoppingCart
    {
        public decimal Total
        {
            get
            {
                decimal sum = 0;
                foreach(Book item in BooksToBuy)
                {
                    sum += item.Price;
                }
                return sum;
            }
        }
        public List<Book> BooksToBuy { get; set; } = new List<Book>();
        public int AddBookToCart(Book bookToAdd)
        {
            BooksToBuy.Add(bookToAdd);
            return BooksToBuy.Count;
        }

        public string PrintReceipt()
        {
            string receipt = "\nYour Receipt\n";
            foreach(Book item in BooksToBuy)
            {
                receipt += item.BookInfo();
                receipt += "\n";
            }
            receipt += "\nTotal in Cart: " + Total.ToString("C2");
            return receipt;
        }
    }
}

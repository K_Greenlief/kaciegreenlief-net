﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Classes
{
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public decimal Price { get; set; }
        public string ISBN { get; set; }
        /// <summary>
        /// Instantiates a new book
        /// </summary>
        /// <param name="title">The Title</param>
        /// <param name="Author">The Author</param>
        /// <param name="Price">The Price</param>
        /// <param name="ISBN">The ISBN</param>
        public Book(string title, string Author,decimal Price,string ISBN)
        {
            Title = title;
            this.Author = Author;
            this.Price = Price;
            this.ISBN = ISBN;
        }

        public string BookInfo()
        {
            return $"Title is {Title}, Author is {Author}. It costs {Price:C2}";
        }
    }
}

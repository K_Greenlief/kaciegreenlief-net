﻿using BookStore.Classes;
using System;

namespace BookStore
{
    class Program
    {
        static void Main(string[] args)
        {
            Book taleOfTwoCities = new Book("Tale of Two Cities","Charles Dickens",9.99M, "978-1503219700");
            Book philosophersStone = new Book("Harry Potter and the Philosopers Stone", "J.K. Rowling", 17.95M, "978-1338596700");
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.AddBookToCart(taleOfTwoCities);
            shoppingCart.AddBookToCart(philosophersStone);
            Console.WriteLine(shoppingCart.BooksToBuy.Count);
            Console.WriteLine(shoppingCart.Total);
            Console.WriteLine(shoppingCart.PrintReceipt());
        }
    }
}

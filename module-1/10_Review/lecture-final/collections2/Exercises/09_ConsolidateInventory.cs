﻿using System.Collections.Generic;

namespace Exercises
{
    public partial class Exercises
    {
        /*
         * Given two Dictionaries, Dictionary<string, int>, merge the two into a new Dictionary, Dictionary<string, int> where keys in Dictionary2,
         * and their int values, are added to the int values of matching keys in Dictionary1. Return the new Dictionary.
         *
         * Unmatched keys and their int values in Dictionary2 are simply added to Dictionary1.
         *
         * ConsolidateInventory({"SKU1": 100, "SKU2": 53, "SKU3": 44} {"SKU2":11, "SKU4": 5})
         * 	 → {"SKU1": 100, "SKU2": 64, "SKU3": 44, "SKU4": 5}
         *
         */
        public Dictionary<string, int> ConsolidateInventory(Dictionary<string, int> mainWarehouse,
                                                            Dictionary<string, int> remoteWarehouse)
        {
            // make a copy
            Dictionary<string, int> output = new Dictionary<string, int>(mainWarehouse);
            // look at each key in remoteWarehouse
            foreach(KeyValuePair<string,int> kvp in remoteWarehouse)
            {
                string keyLocal = kvp.Key;
                int valueLocal = kvp.Value;

                // see if that key is in mainWarehouse
                if(output.ContainsKey(keyLocal))
                {
                    // if it is, add the values together
                    // put in output dictionary
                    output[keyLocal] += valueLocal;
                }
                else
                {
                    // if it isn't, create a new kvp from remoteWarehouse in output dictionary

                    output[keyLocal] = valueLocal;
                }
            }

            return output;
        }
    }
}

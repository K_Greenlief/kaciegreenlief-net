﻿using System;

namespace DiscountCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Print a welcome
            Console.WriteLine("Welcome to the Discount Calculator");
            Console.WriteLine();
            // Get the price
            // Prompt the user
            Console.Write("Enter a series of prices separated by space without the dollar sign: ");
            string prices = Console.ReadLine();
            // split the prices into an array
            char[] delimiters = { ' ', ',', '-', ':' };
            // Tom's favorite version of Split
            string[] priceArray = prices.Split(delimiters,StringSplitOptions.RemoveEmptyEntries);

            //decimal cost = decimal.Parse(Console.ReadLine());
            // Get the discount
            Console.Write("Enter a discount amount (w/o percent sign)");
            double discount = double.Parse(Console.ReadLine()) / 100;
            // Calculate the discounted price for each item in the array
            for (int i = 0; i < priceArray.Length; i++)
            {
                // Get the value in priceArray at index i, and convert to decimal
                decimal price = decimal.Parse(priceArray[i]);
                decimal priceAfterDiscount = price - (price * (decimal)discount);
                // Print the original price, the discount percentage, and the discounted price
                // let's line up the columns
                string column1 = $"Original price: {price:C2} ";
                string column2 = $"Sale Price: {priceAfterDiscount:C2} ";
                string column3 = $"Discount: {discount * 100}%";
                Console.WriteLine(column1.PadRight(30) + "| " + column2.PadRight(30) + "| " + column3.PadRight(30));
            }
        }
    }
}

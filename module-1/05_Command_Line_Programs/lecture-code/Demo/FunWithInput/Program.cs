﻿using System;

namespace FunWithInput
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Fun with Input!");

            // Ask the user for their name
            Console.WriteLine("Please type in your first name. Pretty please.");
            string firstName = Console.ReadLine();
            // Print Hello and their name         
            Console.WriteLine("Hello " + firstName);
            Console.WriteLine("Excellent, how about a last name?");
            string lastName = Console.ReadLine();
            Console.WriteLine("Your full name is " + firstName + " " + lastName);
            Console.WriteLine("Where do you live?");
            string location = Console.ReadLine();
            Console.WriteLine($"That's cool, I love {location}. It's awesome {firstName}");
            Console.WriteLine();
            Console.WriteLine("What's your mantra");
            string mantra = Console.ReadLine();
            Console.WriteLine("How many times should I write your mantra?");
            string timesInput = Console.ReadLine();
            int numTimes = int.Parse(timesInput);

            for(int i = 0; i < numTimes; i++)
            {
                Console.WriteLine(mantra);
            }

            Console.WriteLine("Give me a number");
            int numberGiven = int.Parse(Console.ReadLine());
            while(numberGiven < 20)
            {
                Console.WriteLine(numberGiven);
                numberGiven++;
            }
            int numberAsInt = 0;
            do
            {
                Console.Write("Enter a number");
                string number = Console.ReadLine();
                numberAsInt = int.Parse(number);
                Console.WriteLine("Is it positive? " + (numberAsInt > 0));
            } while (numberAsInt > 0);
        }
    }
}

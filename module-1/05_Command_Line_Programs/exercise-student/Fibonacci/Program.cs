﻿using System;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Fibonacci Sequencer!");
            Console.WriteLine();
            Console.Write("Please enter a number: ");
            string fibValue = Console.ReadLine();
            int goalValue = int.Parse(fibValue);
            int sum = 0;
            int firstValue = 0;
            int secondValue = 1;
            if (goalValue == 0)
            {
                 Console.WriteLine("0, 1");
            }
            if (goalValue == 1)
            {
                Console.WriteLine("0, 1, 1");
            }
            else
            {
                Console.Write("0, 1");
                while (sum < goalValue)

                {
                    sum = firstValue + secondValue;
                    if (sum == goalValue)
                    {
                        Console.Write(", " + sum);
                    }
                    else if (sum < goalValue)
                    {
                        Console.Write(", " + sum);
                        firstValue = secondValue;
                        secondValue = sum;

                    }
                }
            }





        }



    }
}


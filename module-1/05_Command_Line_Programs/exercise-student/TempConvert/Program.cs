﻿using System;

namespace TempConvert
{
    class Program
    {
        static void Main(string[] args)
        {
            //promt temp input
            Console.WriteLine("Welcome to the temperature converter");
            Console.WriteLine();
            Console.Write("Please enter the temperature: ");
            string tempInput = Console.ReadLine();

            char[] extraCharacters = { ' ', ',', '-', ':'};
            string[] tempsArray = tempInput.Split(extraCharacters, StringSplitOptions.RemoveEmptyEntries);


            //promt unit input
            Console.WriteLine("Thanks! Is the temperature in (C)elcius or (F)ahrenheit?");
            string tempUnit = Console.ReadLine();

            //convert and return
            for (int i=0; i<tempsArray.Length; i++) {

                if (tempUnit == "C" || tempUnit == "c")
                {
                    double initialCelcius = double.Parse(tempsArray[i]);
                    double celcuisConversion = (initialCelcius * 1.8 + 32);
                    Console.WriteLine(tempsArray[i] + "°C is " + celcuisConversion + "°F");

                }
                else if (tempUnit == "F" || tempUnit == "f")
                {
                    double initialFahrenheit = double.Parse(tempsArray[i]);
                    double FahrenheitConversion = ((initialFahrenheit - 32) / 1.8);
                    Console.WriteLine(tempsArray[i] + "°C is " + FahrenheitConversion + "°F");
                }


            }
        }

    }
}

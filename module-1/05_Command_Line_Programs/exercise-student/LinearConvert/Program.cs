﻿using System;

namespace LinearConvert
{
    class Program
    {
         static void Main(string[] args)
        {
            //promt  input
            Console.WriteLine("Welcome to the Linear Converter");
            Console.WriteLine();
            Console.Write("Please enter the length: ");
            string lengthInput = Console.ReadLine();

            char[] extraCharacters = { ' ', ',', '-', ':' };
            string[] lengthsArray = lengthInput.Split(extraCharacters, StringSplitOptions.RemoveEmptyEntries);


            //promt unit input
            Console.WriteLine("Is the length in (m)eters or (f)eet?");
            string lengthUnit = Console.ReadLine();

            //convert and return
            for (int i = 0; i < lengthsArray.Length; i++)
            {

                if (lengthUnit == "M" || lengthUnit == "m")
                {
                    double initialLength = double.Parse(lengthsArray[i]);
                    double feetConversion = (initialLength * 3.2808399);
                    Console.WriteLine(lengthsArray[i] + " meters is " + feetConversion + " feet.");

                }
                else if (lengthUnit == "F" || lengthUnit == "f")
                {
                    double initialLength = double.Parse(lengthsArray[i]);
                    double metersConversion = initialLength * 0.3048;
                    Console.WriteLine(lengthsArray[i] + " feet is " + metersConversion + " meters.");
                }


            }
        }
    }
}

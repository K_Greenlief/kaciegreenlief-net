let allItemsIncomplete = true;
const pageTitle = 'My Shopping List';
const groceries = [
    { id: 1, name: 'Oatmeal', completed: false },
    { id: 2, name: 'Milk', completed: false },
    { id: 3, name: 'Banana', completed: false },
    { id: 4, name: 'Strawberries', completed: false },
    { id: 5, name: 'Lunch Meat', completed: false },
    { id: 6, name: 'Bread', completed: false },
    { id: 7, name: 'Grapes', completed: false },
    { id: 8, name: 'Steak', completed: false },
    { id: 9, name: 'Salad', completed: false },
    { id: 10, name: 'Tea', completed: false }
];

/**
 * This function will get a reference to the title and set its text to the value
 * of the pageTitle variable that was set above.
 */
function setPageTitle() {
    const title = document.getElementById('title');
    title.innerText = pageTitle;
}

/**
 * This function will loop over the array of groceries that was set above and add them to the DOM.
 */
function displayGroceries() {
    const ul = document.querySelector('ul');
    groceries.forEach((item) => {
        const li = document.createElement('li');
        li.innerText = item.name;
        const checkCircle = document.createElement('i');
        checkCircle.setAttribute('class', 'far fa-check-circle');
        li.appendChild(checkCircle);
        ul.appendChild(li);
    });
}
/**
 * these functions add an event listener to each list item and icon  ...      
 * and change them when clicked depending on the class
 */

document.addEventListener('DOMContentLoaded', () => {

    setPageTitle();
    displayGroceries();


    const icons = document.querySelectorAll('li i');
    icons.forEach(item => {
        item.addEventListener('click', (event) => {
            if (!item.classList.contains('completed')) {
                item.classList.add('completed');
            } else {
                item.classList.remove('completed');
            }
        })
    });



    document.querySelectorAll('li').forEach((item) => {
        item.addEventListener('click', () => {

            if (!item.classList.contains('completed')) {
                item.classList.add('completed');
                const icon = item.querySelector('li i');
                icon.setAttribute('class', 'far fa-check-circle completed');
            }
        })
    })
    document.querySelectorAll('ul li').forEach((item) => {
        item.addEventListener('dblclick', () => {
            if (item.classList.contains('completed')) {
                item.classList.remove('completed');
                const icon = item.querySelector('li i');
                icon.setAttribute('class', 'far fa-check-circle');
            }
        })
    })


    const wholeList = document.querySelectorAll('ul li');
    const button = document.querySelector('a');

    button.addEventListener('click', event => {


        if (allItemsIncomplete) {

            wholeList.forEach(item => {
                item.classList.add('completed');
                const icon = item.querySelector('li i');
                icon.setAttribute('class', 'far fa-check-circle completed');
                allItemsIncomplete = false;
                button.text = ('MARK ALL INCOMPLETE');

            })
        } else {
            wholeList.forEach(item => {
                item.classList.remove('completed');
                const icon = item.querySelector('li i');
                icon.setAttribute('class', 'far fa-check-circle');
                allItemsIncomplete = true;
                button.text = ('MARK ALL COMPLETE');

            })

        }




    })

});
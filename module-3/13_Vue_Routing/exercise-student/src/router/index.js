import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import MyBooks from '@/views/MyBooks.vue';
import NewBook from '@/views/NewBook.vue';
import Details from '@/views/Details.vue';

Vue.use(VueRouter);

const routes = [{
        path: '/',
        name: "home-view",
        component: Home
    },
    {
        path: '/myBooks',
        name: "my-books",
        component: MyBooks
    },
    {
        path: '/addBook',
        name: "new-book",
        component: NewBook
    },
    {
        path: '/book/:isbn',
        name: "details",
        component: Details
    }
];

const router = new VueRouter({
    mode: 'history',
    // base: process.env.BASE_URL,
    routes
});

export default router;
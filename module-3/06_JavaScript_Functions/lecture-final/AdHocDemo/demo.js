function concatAll() { // No parameters defined, but we still might get some
    let result = '';
    for(let i = 0; i < arguments.length; i++) {
        if(arguments[i] === 5)
        {
            result = 0;
        } else if(result === undefined)
        {
            result = '';
        }
        result += arguments[i];
    }
    return result;
}

let numbers = [1,2,3,4];
let evenNumbers = numbers.filter((item) => {
    return item % 2 === 0;
});
console.log(evenNumbers);

let nameParts = ['bosco', 'p.', 'soultrain'];

let fullName = nameParts.reduce( (reducer, part) => {
    return reducer + ' ' + part.substring(0, 1).toLocaleUpperCase() + part.substring(1);
}, ''); // <--- The empty quotes is the value of the reducer for the first element

console.log(fullName.trim());


/**
 * All named functions will have the function keyword and
 * a name followed by parentheses.
 * 
 * @returns {number} 1
 */
function returnOne() {
  return 1;
}

/**
 * Functions can also take parameters. These are just variables that are filled
 * in by whoever is calling the function.
 *
 * Also, we don't *have* to return anything from the actual function.
 *
 * @param {any} value the value to print to the console
 */
function printToConsole(value) {
  console.log(value);
}

/**
 * Write a function called multiplyTogether that multiplies two numbers together. But 
 * what happens if we don't pass a value in? What happens if the value is not a number?
 *
 * @param {number} firstParameter the first parameter to multiply
 * @param {number} secondParameter the second parameter to multiply
 */
function multiplyTogether(firstParameter,secondParameter) {
  return firstParameter * secondParameter;
}
/**
 * This version makes sure that no parameters are ever missing. If
 * someone calls this function without parameters, we default the
 * values to 0. However, it is impossible in JavaScript to prevent
 * someone from calling this function with data that is not a number.
 * Call this function multiplyNoUndefined
 *
 * @param {number} [firstParameter=0] the first parameter to multiply
 * @param {number} [secondParameter=0] the second parameter to multiply
 */
function multiplyNoUndefined(firstParameter = 0, secondParameter = 0)
{
  return firstParameter * secondParameter;
}

 /** Order of parameters matters */
 function orderMatters(first,second,third)
 {
   console.log(first);
   console.log(second + third);
 }
/**
 * Functions can return earlier before the end of the function. This could be useful
 * in circumstances where you may not need to perform additional instructions or have to
 * handle a particular situation.
 * In this example, if the firstParameter is equal to 0, we return secondParameter times 2.
 * Observe what's printed to the console in both situations.
 * 
 * @param {number} firstParameter the first parameter
 * @param {number} secondParameter the second parameter
 */
function returnBeforeEnd(firstParameter, secondParameter) {
  console.log("This will always fire.");

  if (firstParameter == 0) {
    console.log("Returning secondParameter times two.");
    return secondParameter * 2;
  }

  //this only runs if firstParameter is NOT 0
  console.log("Returning firstParameter + secondParameter.");
  return firstParameter + secondParameter;
}

/**
 * Scope is defined as where a variable is available to be used.
 *
 * If a variable is declare inside of a block, it will only exist in
 * that block and any block underneath it. Once the block that the
 * variable was defined in ends, the variable disappears.
 */
function scopeTest() {
  // This variable will always be in scope in this function
  let inScopeInScopeTest = true;

  {
    // this variable lives inside this block and doesn't
    // exist outside of the block
    let scopedToBlock = inScopeInScopeTest;
  }

  // scopedToBlock doesn't exist here so an error will be thrown
  if (inScopeInScopeTest && scopedToBlock) {
    console.log("This won't print!");
  }
}

/**
 * 
 * @param {*} name 
 * @param {*} age 
 * @param {*} listOfQuirks 
 * @param {*} separator 
 * @returns 
 */
function createSentenceFromUser(name, age, listOfQuirks = [], separator = ', ') {
  let description = `${name} is currently ${age} years old. Their quirks are: `;
  return description + listOfQuirks.join(separator);
}

/**
 * Takes an array and, using the power of anonymous functions, generates
 * their sum.
 *
 * @param {number[]} numbersToSum numbers to add up
 * @returns {number} sum of all the numbers
 */
function sumAllNumbers(numbersToSum) {
  return numbersToSum.reduce((sum,numberFromArray) => {
    return sum += numberFromArray;
  },0);
}

/**
 * Numbers to sum, no reduce
 */
function sumWithoutReduce(numbersToSum)
{
  let ourSum = 0;
  numbersToSum.forEach((item) => {
    ourSum += item;
  })
  console.log(ourSum);
}

/** 
 * use a named-function
 */

function addUpNumbers(thing1, thing2)
{
  return thing1 + thing2;
}

function sumAllNumbersWithNamed(numbersToSum)
{
  return numbersToSum.reduce(addUpNumbers); 
}
/**
 * 
 * This is the substituted version of above
 */
function sumAllNumbersWithNamed(numbersToSum)
{
  return numbersToSum.reduce((thing1, thing2) =>
  {
    return thing1 + thing2;
  },0); 
}

/**
 * Takes an array and returns a new array of only numbers that are
 * multiples of 3
 *
 * @param {number[]} numbersToFilter numbers to filter through
 * @returns {number[]} a new array with only those numbers that are
 *   multiples of 3
 */
function allDivisibleByThree(numbersToFilter) {
  let resultArray = numbersToFilter.filter((passedInNumber) => {
    return passedInNumber % 3 === 0;
  })
  return resultArray;
}
/**
 * Fancy version: directly return the result from filter
 * Know that all values are truthy/falsy so when 0 return true (!false)
 */
function allDivisibleByThree(numbersToFilter) {
  return numbersToFilter.filter((passedInNumber) => {
    return !(passedInNumber % 3);
  })
}

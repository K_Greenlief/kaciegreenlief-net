const groceries = ['milk', 'sugar', 'eggs', 'chicken',
    'watermelon', 'cookies', 'coffee', 'cheese', 'bread', 'bacon'
];
const pageTitle = 'My Shopping List';

/**
 * This function will get a reference to the title and set its text to the value
 * of the pageTitle variable that was set above.
 */
function setPageTitle() {
    document.getElementById('title').innerText = pageTitle;

}

/**
 * This function will loop over the array of groceries that was set above and add them to the DOM.
 */
function displayGroceries() {
    const list = document.getElementById('groceries');

    groceries.forEach(item => {
        const listItem = document.createElement('li');
        listItem.classList.add(item);
        list.insertAdjacentElement('beforeend', listItem);
        list.insertAdjacentText('beforeend', item);
    });
}

/**
 * add the class completed to each one
 */
function markCompleted() {
    const listItems = document.querySelectorAll('li');
    listItems.forEach(item => {
        item.setAttribute('class', ' completed');
    })
}

setPageTitle();

displayGroceries();

// Don't worry too much about what is going on here, we will cover this when we discuss events.
document.addEventListener('DOMContentLoaded', () => {
    // When the DOM Content has loaded attach a click listener to the button
    const button = document.querySelector('.btn');
    button.addEventListener('click', markCompleted);
});
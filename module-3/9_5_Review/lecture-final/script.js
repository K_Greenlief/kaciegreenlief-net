document.addEventListener('DOMContentLoaded', event => {
    const listParent = document.getElementsByTagName('ul')[0];

    fetch('https://techelevator-pgh-teams.azurewebsites.net/api/techelevator/shoppinglist')
        .then(first => {
            return first.json();
        })
        .then(second => {
            second.forEach(element => {
                const newLI = document.createElement('li');
                newLI.innerText = element.name;
                listen(newLI);
                listParent.insertAdjacentElement('beforeend',newLI);
                //listParent.appendChild(newLI);
            });
            // this will work here because we have the elements in the DOM
            listenBroken();
            populateList(second);

        })
        .catch(err => {
            console.log(err);
        })

    // this doesn't work here but works above
    //listenBroken();
})

function listen(element) {
    element.addEventListener('click', event => {
        alert(element.innerText);
    })
}

function listenBroken() {
    const liElements = document.getElementsByTagName('li');
    Array.from(liElements).forEach(element => {
        element.addEventListener('click', event => {
            alert('You wont see me')
        })
    })
}

function populateList(theList) {
    if ('content' in document.createElement('template')) {
        const bodyReference = document.querySelector('body');
        theList.forEach(element => {
            if(element.completed)
            {
                const tmpl = document.getElementById('shoppingListTrue').content.cloneNode(true);
                tmpl.querySelector('h3').innerText = element.name;
                tmpl.querySelector('strong').innerText = element.completed;
                bodyReference.appendChild(tmpl);
    
            } else {
                const tmpl = document.getElementById('shoppingListFalse').content.cloneNode(true);
                tmpl.querySelector('h3').innerText = element.name;
                tmpl.querySelector('strong').innerText = element.completed;
                bodyReference.appendChild(tmpl);
    
            }

        })
    }
}
// let's create a variable to hold our reviews
let reviews = [];

document.addEventListener('DOMContentLoaded', (event) => {
    // be careful if another button is added above this on in the html
    const loadButton = document.querySelector('button');
    loadButton.addEventListener('click', (event) => {
        fetchReviews();
    })
})

/**
 * This function when invoked will look at an array of reviews
 * and add it to the page by cloning the #review-template
 */
function displayReviews() {
    console.log("Display Reviews...");


    if ('content' in document.createElement('template')) {
        // query the document for .reviews and assign it to a variable called container
        const container = document.querySelector(".reviews");
        // loop over the reviews array
        reviews.forEach((review) => {
            // get the template; find all the elements and add the data from our review to each element
            const tmpl = document.getElementById('review-template').content.cloneNode(true);
            tmpl.querySelector('img').setAttribute("src", review.avatar);
            tmpl.querySelector('.username').innerText = review.username;
            tmpl.querySelector('h2').innerText = review.title;
            tmpl.querySelector('.published-date').innerText = review.publishedOn;
            tmpl.querySelector('.user-review').innerText = review.review;
            container.appendChild(tmpl);
        });
    } else {
        console.error('Your browser does not support templates');
    }
}

function fetchReviews() {
    fetch('https://techelevator-pgh-teams.azurewebsites.net/api/techelevator')
        .then((firstPromise) => {
            return firstPromise.json();
        })
        .then((secondPromise) => {
            reviews = secondPromise;
            console.table(reviews);
            displayReviews();

        })
        .catch((error) => {
            console.log(error);
        })
        //alert('Getting reviews');

}
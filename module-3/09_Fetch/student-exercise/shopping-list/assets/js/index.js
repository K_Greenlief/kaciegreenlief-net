let groceryList = [];
isCompleted = false;
isListLoaded = false;
let parentList;

document.addEventListener('DOMContentLoaded', () => {

    const loadingButton = document.querySelector('a');
    loadingButton.addEventListener('click', event => {

        if (isListLoaded == false) {
            fetchGroceries();
            isListLoaded = true;
        }

    })
})

function fetchGroceries() {

    fetch("https://techelevator-pgh-teams.azurewebsites.net/api/techelevator/shoppinglist")
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            groceryList = data;
            displayGroceries();
            markTask();
        })
        .catch((error) => {
            console.log(error);
        })
}

function displayGroceries() {
    if ('content' in document.createElement('template')) {
        parentList = document.querySelector('ul');

        groceryList.forEach(item => {
            const elementTempl = document.getElementById('shopping-list-item-template').content.cloneNode(true);
            elementTempl.querySelector('li').insertAdjacentHTML('afterbegin', item.name);

            parentList.appendChild(elementTempl);

        });



    } else {
        console.error('Your browser does not support templates');
    }
}

function markTask() {

    const all = document.querySelectorAll('li');

    all.forEach((item) => {
        item.addEventListener('click', (event) => {

            if (!item.classList.contains('completed')) {
                item.classList.add('completed');
                const icon = item.querySelector('li i');
                icon.setAttribute('class', 'far fa-check-circle completed');


            } else {
                item.classList.remove('completed');
                const icon = item.querySelector('li i');
                icon.setAttribute('class', 'far fa-check-circle');
            }
        })
    });
}